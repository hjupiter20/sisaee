function validarFormulario(pForm){
	
    jQuery.validator.messages.required = 'Este campo es obligatorio.';
    jQuery.validator.messages.number = 'Este campo debe ser num&eacute;rico.';
    jQuery.validator.messages.email = 'La direcci&oacute;n de correo es incorrecta.';
	jQuery.validator.messages.digits = 'Este campo debe ser n&uacute;mero entero.'
    var validado=$("#"+pForm).valid();
	if(validado){
          //alert('El formulario es correcto.');
		return true;
       }
	else{
		return false;
	}		
}
function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46-32";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
}

function bloquearCopiar()
{
	$('*').bind("cut copy paste",function(e) {
      e.preventDefault();
    });
}

