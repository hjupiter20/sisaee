function validarNumerosNegativos(e){
    if( e.keyCode == 109 || e.keyCode == 107 || e.keyCode == 189 || e.keyCode == 187){
        return false;
    }
}

function IsNumeric(valor) 
{ 
	var log=valor.length; var sw="S"; 
	for (x=0; x<log; x++) 
	{ v1=valor.substr(x,1); 
	v2 = parseInt(v1); 
	//Compruebo si es un valor numérico 
	if (isNaN(v2)) { sw= "N";} 
	} 
	if (sw=="S") {return true;} else {return false; } 
} 
var primerslap=false; 
var segundoslap=false;
function formateafecha(fecha,oEvent,name,obj) 
{ 
	var long = fecha.length; 
	var dia; 
	var mes; 
	var ano; 
	iKeyCode=oEvent.keyCode;
	if ((long>=2) && (primerslap==false)) { dia=fecha.substr(0,2); 
	if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); primerslap=true; } 
	else { fecha=""; primerslap=false;} 
	} 
	else 
	{ dia=fecha.substr(0,1); 
	if (IsNumeric(dia)==false) 
	{fecha="";} 
	if ((long<=2) && (primerslap=true)) {fecha=fecha.substr(0,1); primerslap=false; } 
	} 
	if ((long>=5) && (segundoslap==false)) 
	{ mes=fecha.substr(3,2); 
	if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); segundoslap=true; } 
	else { fecha=fecha.substr(0,3);; segundoslap=false;} 
	} 
	else { if ((long<=5) && (segundoslap=true)) { fecha=fecha.substr(0,4); segundoslap=false; } } 
	if (long>=7) 
	{ ano=fecha.substr(6,4); 
	if (IsNumeric(ano)==false) { fecha=fecha.substr(0,6); } 
	else { if (long==10){ if ((ano==0) || (ano<1900) || (ano>2100)) { fecha=fecha.substr(0,6); } } } 
	} 
	
	if (long>=10) 
	{ 
	fecha=fecha.substr(0,10); 
	dia=fecha.substr(0,2); 
	mes=fecha.substr(3,2); 
	ano=fecha.substr(6,4); 
	// Año no viciesto y es febrero y el dia es mayor a 28 
		if ( (ano%4 != 0) && (mes ==02) && (dia > 28) ) 
		{ fecha=fecha.substr(0,2)+"/"; } 
	} 

	return (fecha); 	
} 
function validarid(wtexto) {
	try {
		var numero = document.getElementById(wtexto).value;
		
		var suma = 0;
		var residuo = 0;
		var pri = false;
		var pub = false;
		var nat = false;
		var numeroProvincias = 24;
		var modulo = 11;

		/* Verifico que el campo no contenga letras */
		var ok = 1;
		for (i = 0; i < numero.length && ok == 1; i++) {
			var n = parseInt(numero.charAt(i));
			if (isNaN(n)) ok = 0;
		}
		if (ok == 0) {
			vMensaje='No puede ingresar caracteres en el n&uacute;mero';
			$('#divCedula').focus();
			$('#divCedula').removeClass();
			$('#divCedula').addClass('form-group has-error');
			$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(300).delay(3500).hide(200);
			return false;
		}

		if (numero.length < 10) {
			vMensaje='El n&uacute;mero ingresado no es v&aacute;lido';
			$('#divCedula').focus();
			$('#divCedula').removeClass();
			$('#divCedula').addClass('form-group has-error');
			$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(300).delay(3500).hide(200);
			return false;
		}

		/* Los primeros dos digitos corresponden al codigo de la provincia */
		provincia = numero.substr(0, 2);
		if (provincia < 1 || provincia > numeroProvincias) {			
			vMensaje='El c&oacute;digo de la provincia (dos primeros d&iacute;gitos) es inv&aacute;lido';
			$('#divCedula').focus();
			$('#divCedula').removeClass();
			$('#divCedula').addClass('form-group has-error');
			$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(300).delay(3500).hide(200);
			return false;
		}
		/* Aqui almacenamos los digitos de la cedula en variables. */
		d1 = numero.substr(0, 1);
		d2 = numero.substr(1, 1);
		d3 = numero.substr(2, 1);
		d4 = numero.substr(3, 1);
		d5 = numero.substr(4, 1);
		d6 = numero.substr(5, 1);
		d7 = numero.substr(6, 1);
		d8 = numero.substr(7, 1);
		d9 = numero.substr(8, 1);
		d10 = numero.substr(9, 1);

		/* El tercer digito es: */
		/* 9 para sociedades privadas y extranjeros   */
		/* 6 para sociedades publicas */
		/* menor que 6 (0,1,2,3,4,5) para personas naturales */

		if (d3 == 7 || d3 == 8) {
			vMensaje='El tercer d&iacute;gito ingresado es inv&aacute;lido';
			$('#divCedula').focus();
			$('#divCedula').removeClass();
			$('#divCedula').addClass('form-group has-error');
			$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(300).delay(3500).hide(200);
			return false;
		}
		/* Solo para personas naturales (modulo 10) */
		if (d3 < 6) {
			nat = true;
			p1 = d1 * 2;
			if (p1 >= 10) p1 -= 9;
			p2 = d2 * 1;
			if (p2 >= 10) p2 -= 9;
			p3 = d3 * 2;
			if (p3 >= 10) p3 -= 9;
			p4 = d4 * 1;
			if (p4 >= 10) p4 -= 9;
			p5 = d5 * 2;
			if (p5 >= 10) p5 -= 9;
			p6 = d6 * 1;
			if (p6 >= 10) p6 -= 9;
			p7 = d7 * 2;
			if (p7 >= 10) p7 -= 9;
			p8 = d8 * 1;
			if (p8 >= 10) p8 -= 9;
			p9 = d9 * 2;
			if (p9 >= 10) p9 -= 9;
			modulo = 10;
		}
		/* Solo para sociedades publicas (modulo 11) */
		/* Aqui el digito verficador esta en la posicion 9, en las otras 2 en la pos. 10 */
		else if (d3 == 6) {
			pub = true;
			p1 = d1 * 3;
			p2 = d2 * 2;
			p3 = d3 * 7;
			p4 = d4 * 6;
			p5 = d5 * 5;
			p6 = d6 * 4;
			p7 = d7 * 3;
			p8 = d8 * 2;
			p9 = 0;
		}
		/* Solo para entidades privadas (modulo 11) */
		else if (d3 == 9) {
			pri = true;
			p1 = d1 * 4;
			p2 = d2 * 3;
			p3 = d3 * 2;
			p4 = d4 * 7;
			p5 = d5 * 6;
			p6 = d6 * 5;
			p7 = d7 * 4;
			p8 = d8 * 3;
			p9 = d9 * 2;
		}
		suma = p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
		residuo = suma % modulo;

		/* Si residuo=0, dig.ver.=0, caso contrario 10 - residuo*/
		digitoVerificador = residuo == 0 ? 0 : modulo - residuo;

		/* ahora comparamos el elemento de la posicion 10 con el dig. ver.*/
		if (pub == true) {
			if (digitoVerificador != d9) {
				vMensaje='El ruc de la empresa del sector p&uacute;blico es incorrecto.';
				$('#divCedula').focus();
				$('#divCedula').removeClass();
				$('#divCedula').addClass('form-group has-error');
				$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(300).delay(3500).hide(200);
				return false;
			}
			/* El ruc de las empresas del sector publico terminan con 0001*/
			if (numero.substr(9, 4) != '0001') {
				vMensaje='El ruc de la empresa del sector p&uacute;blico debe terminar con 0001.';
				$('#divCedula').focus();
				$('#divCedula').removeClass();
				$('#divCedula').addClass('form-group has-error');
				$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(300).delay(3500).hide(200);
				return false;
			}
		} else if (pri == true) {
			if (digitoVerificador != d10) {
				vMensaje='El ruc de la empresa del sector privado es incorrecto.';
				$('#divCedula').focus();
				$('#divCedula').removeClass();
				$('#divCedula').addClass('form-group has-error');
				$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(300).delay(3500).hide(200);
				return false;
			}
			if (numero.substr(10, 3) != '001') {
				vMensaje='El ruc de la empresa del sector privado debe terminar con 001.';
				$('#divCedula').focus();
				$('#divCedula').removeClass();
				$('#divCedula').addClass('form-group has-error');
				$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(300).delay(3500).hide(200);
				return false;
			}
		} else if (nat == true) {
			if (digitoVerificador != d10) {
				vMensaje='El n\u00FAmero de c&eacute;dula de la persona natural es incorrecto.';
				$('#divCedula').focus();
				$('#divCedula').removeClass();
				$('#divCedula').addClass('form-group has-error');
				$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(300).delay(3500).hide(200);
				return false;
			}
			if (numero.length > 10 && numero.substr(10, 3) != '001') {
				vMensaje='El ruc de la persona natural debe terminar con 001.';
				$('#divCedula').focus();
				$('#divCedula').removeClass();
				$('#divCedula').addClass('form-group has-error');
				$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(300).delay(3500).hide(200);
				return false;
			}
		}
		$('#divCedula').removeClass();
		$('#divCedula').addClass('form-group has-success');	
		return true;
	} catch (expeption) {
		alert(expeption.name);
	}
}  

function validarid2(pNumCedula) {
	try {
		var numero = pNumCedula;		
		var suma = 0;
		var residuo = 0;
		var pri = false;
		var pub = false;
		var nat = false;
		var numeroProvincias = 24;
		var modulo = 11;
		/* Verifico que el campo no contenga letras */
		var ok = 1;
		for (i = 0; i < numero.length && ok == 1; i++) {
			var n = parseInt(numero.charAt(i));
			if (isNaN(n)) ok = 0;
		}
		if (ok == 0) {			
			return false;
		}
		if (numero.length < 10) {
			return false;
		}		
		provincia = numero.substr(0, 2);
		if (provincia < 1 || provincia > numeroProvincias) {
			return false;
		}
		/* Aqui almacenamos los digitos de la cedula en variables. */
		d1 = numero.substr(0, 1);
		d2 = numero.substr(1, 1);
		d3 = numero.substr(2, 1);
		d4 = numero.substr(3, 1);
		d5 = numero.substr(4, 1);
		d6 = numero.substr(5, 1);
		d7 = numero.substr(6, 1);
		d8 = numero.substr(7, 1);
		d9 = numero.substr(8, 1);
		d10 = numero.substr(9, 1);
		
		if (d3 == 7 || d3 == 8) {
			return false;
		}
		/* Solo para personas naturales (modulo 10) */
		if (d3 < 6) {
			nat = true;
			p1 = d1 * 2;
			if (p1 >= 10) p1 -= 9;
			p2 = d2 * 1;
			if (p2 >= 10) p2 -= 9;
			p3 = d3 * 2;
			if (p3 >= 10) p3 -= 9;
			p4 = d4 * 1;
			if (p4 >= 10) p4 -= 9;
			p5 = d5 * 2;
			if (p5 >= 10) p5 -= 9;
			p6 = d6 * 1;
			if (p6 >= 10) p6 -= 9;
			p7 = d7 * 2;
			if (p7 >= 10) p7 -= 9;
			p8 = d8 * 1;
			if (p8 >= 10) p8 -= 9;
			p9 = d9 * 2;
			if (p9 >= 10) p9 -= 9;
			modulo = 10;
		}
		else if (d3 == 6) {
			pub = true;
			p1 = d1 * 3;
			p2 = d2 * 2;
			p3 = d3 * 7;
			p4 = d4 * 6;
			p5 = d5 * 5;
			p6 = d6 * 4;
			p7 = d7 * 3;
			p8 = d8 * 2;
			p9 = 0;
		}		
		else if (d3 == 9) {
			pri = true;
			p1 = d1 * 4;
			p2 = d2 * 3;
			p3 = d3 * 2;
			p4 = d4 * 7;
			p5 = d5 * 6;
			p6 = d6 * 5;
			p7 = d7 * 4;
			p8 = d8 * 3;
			p9 = d9 * 2;
		}
		suma = p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
		residuo = suma % modulo;
		/* Si residuo=0, dig.ver.=0, caso contrario 10 - residuo*/
		digitoVerificador = residuo == 0 ? 0 : modulo - residuo;
		/* ahora comparamos el elemento de la posicion 10 con el dig. ver.*/
		if (pub == true) {
			if (digitoVerificador != d9) {
				return false;
			}
			/* El ruc de las empresas del sector publico terminan con 0001*/
			if (numero.substr(9, 4) != '0001') {
				return false;
			}
		} else if (pri == true) {
			if (digitoVerificador != d10) {
				return false;
			}
			if (numero.substr(10, 3) != '001') {
				return false;
			}
		} else if (nat == true) {
			if (digitoVerificador != d10) {
				return false;
			}
			if (numero.length > 10 && numero.substr(10, 3) != '001') {
				return false;
			}
		}		
		return true;
	} catch (expeption) {
		alert(expeption.name);
	}
}  