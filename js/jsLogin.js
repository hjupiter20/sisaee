function lTrim(sStr)
{
 while (sStr.charAt(0) == " ")

  sStr = sStr.substr(1, sStr.length - 1);

 return sStr;
}
function rTrim(sStr)
{
 while (sStr.charAt(sStr.length - 1) == " ")

  sStr = sStr.substr(0, sStr.length - 1);

 return sStr;
}
function allTrim(sStr)
{
 return rTrim(lTrim(sStr));
}
function jsLogin()
{
	var pUser=allTrim(document.getElementById('txtUsuario').value);
	var pPasswd=md5(allTrim(document.getElementById('txtPasswd').value));	
	var url = '../php/login.php';
	var pAccion='LOGIN';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pAccion='+pAccion+'&pUser='+pUser+'&pPasswd='+pPasswd,
		success:function(data)
		{
			var array = eval(data);			
			switch(array[1])
				{
					case '1':
						$('#divMensaje').addClass('alert alert-success').html(array[0]).show(300).delay(3500).hide(200);
						window.location='../php/';
					break
					case '2':
					
						$('#divPasswd').removeClass();
						$('#divPasswd').addClass('form-group has-error');
						$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(3500).hide(200);
					break
					case '3':
						$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(3500).hide(200);
						window.location='../php/cambiar_password.php';
					break
					case '4':
						$('#divPasswd').removeClass();
						$('#divPasswd').addClass('form-group has-error');
						$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(3500).hide(200);
					break
					case '5':
						$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(3500).hide(200);
					break
					case '6':
						$('#divMensaje').addClass('alert alert-success').html(array[0]).show(300).delay(2500).hide(200);	
						window.location='../php/cambiar_password.php';
					break
				}
		}
	});	
}
function jsValidaPasswdAnt()
{	
	var pPasswdAnterior=md5(allTrim($('#txtPasswdAnterior').val()));
	
	//alert(pPasswdAnterior);
	var url = '../php/login.php';
	var pAccion='VALIDA_PASSWD_ANT';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pAccion='+pAccion+'&pPasswdAnterior='+pPasswdAnterior,
		success:function(data)
		{
			var array = eval(data);
			if(array[1]==1)
				{
					$('#divPasswdAnterior').removeClass();
					$('#divPasswdAnterior').addClass('form-group has-success');	
					//window.location='../php/';
				}
			else if(array[1]==3)
				{
					$('#divPasswdAnterior').removeClass();
					$('#divPasswdAnterior').addClass('form-group has-error');
					$('#txtPasswdAnterior').focus();
					$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(3500).hide(200);
				}
				else
				{
					$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(3500).hide(200);
					$('#divPasswdAnterior').removeClass();
					$('#divPasswdAnterior').addClass('form-group has-error');
					$('#txtPasswdAnterior').focus();
				}		
		}
	});	
}
function testPassword(passwd)
{
	var intScore   = 0
	var strVerdict = "weak"
	var strLog     = ""	
	// PASSWORD LENGTH
	if (passwd.length<=5)                         // length 4 or less
	{
		intScore = (intScore+5)
		strLog   = strLog + "5 points for length (" + passwd.length + ")\n"
	}
	else if (passwd.length>5 && passwd.length<=8) // length between 5 and 7
	{
		intScore = (intScore+7)
		strLog   = strLog + "7 points for length (" + passwd.length + ")\n"
	}
	else if (passwd.length>8 && passwd.length<=16)// length between 8 and 15
	{
		intScore = (intScore+10)
		strLog   = strLog + "10 points for length (" + passwd.length + ")\n"
	}
	else if (passwd.length>16 && passwd.length<=25)                    // length 16 or more
	{
		intScore = (intScore+14)
		strLog   = strLog + "14 point for length (" + passwd.length + ")\n"
	}
	else if (passwd.length>25)                    // length 16 or more
	{
		intScore = (intScore+18)
		strLog   = strLog + "18 point for length (" + passwd.length + ")\n"
	}	
	// LETTERS (Not exactly implemented as dictacted above because of my limited understanding of Regex)
	if (passwd.match(/[a-z]/))                              // [verified] at least one lower case letter
	{
		intScore = (intScore+2)
		strLog   = strLog + "2 point for at least one lower case char\n"
	}
	
	if (passwd.match(/[A-Z]/))                              // [verified] at least one upper case letter
	{
		intScore = (intScore+4)
		strLog   = strLog + "4 points for at least one upper case char\n"
	}	
	// NUMBERS
	if (passwd.match(/\d+/))                                 // [verified] at least one number
	{
		intScore = (intScore+4)
		strLog   = strLog + "4 points for at least one number\n"
	}
	
	if (passwd.match(/(.*[0-9].*[0-9].*[0-9])/))             // [verified] at least three numbers
	{
		intScore = (intScore+7)
		strLog   = strLog + "7 points for at least three numbers\n"
	}	
	// SPECIAL CHAR
	if (passwd.match(/.[!,@,#,$,%,^,&,*,?,_,~]/))            // [verified] at least one special character
	{
		intScore = (intScore+7)
		strLog   = strLog + "7 points for at least one special char\n"
	}	
	// [verified] at least two special characters
	if (passwd.match(/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/))
	{
		intScore = (intScore+9)
		strLog   = strLog + "9 points for at least two special chars\n"
	}	
	// COMBOS
	if (passwd.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))        // [verified] both upper and lower case
	{
		intScore = (intScore+3)
		strLog   = strLog + "3 combo points for upper and lower letters\n"
	}

	if (passwd.match(/([a-zA-Z])/) && passwd.match(/([0-9])/)) // [verified] both letters and numbers
	{
		intScore = (intScore+3)
		strLog   = strLog + "3 combo points for letters and numbers\n"
	}
								// [verified] letters, numbers, and special characters
	if (passwd.match(/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/))
	{
		intScore = (intScore+7)
		strLog   = strLog + "7 combo points for letters, numbers and special chars\n"
	}
	if(intScore <= 20)
	{
		strVerdict = "MUY DEBIL";
		$('#divNuevoPasswd').removeClass();
		$('#divNuevoPasswd').addClass('form-group has-error');
		$('#divNivelPasswd').removeClass();
		$('#divNivelPasswd').addClass('alert alert-danger');	
	}
	else if (intScore > 20 && intScore <= 34)
	{
	   	strVerdict = "DEBIL"
		$('#divNuevoPasswd').removeClass();
		$('#divNuevoPasswd').addClass('form-group has-error');
		$('#divNivelPasswd').removeClass();
		$('#divNivelPasswd').addClass('alert alert-danger');
	}
	else if (intScore > 34 && intScore <= 40)
	{
		strVerdict = "REGULAR"
		$('#divNuevoPasswd').removeClass();
		$('#divNuevoPasswd').addClass('form-group has-warning');
		$('#divNivelPasswd').removeClass();
		$('#divNivelPasswd').addClass('alert alert-warning');
	}
	else if (intScore > 40 && intScore <= 55)
	{
	   	strVerdict = "BUENA"
		$('#divNuevoPasswd').removeClass();
		$('#divNuevoPasswd').addClass('form-group has-success');
		$('#divNivelPasswd').removeClass();
		$('#divNivelPasswd').addClass('alert alert-success');
	}
	else
	{
	   	strVerdict = "MUY BUENA"
		$('#divNuevoPasswd').removeClass();
		$('#divNuevoPasswd').addClass('form-group has-success');
		$('#divNivelPasswd').removeClass();
		$('#divNivelPasswd').addClass('alert alert-success');
	}	
	document.getElementById('hddVeredicto').value=(intScore);
	document.getElementById('verdict').innerHTML=strVerdict;
	
}
function js_confirma_password(pPass, oEvent)
{
	iKeyCode=oEvent.keyCode;	
	if (iKeyCode==13)
	{
		if(document.getElementById('txtNuevoPasswd').value!=pPass)
		{
			//Sexy.error("<b>Las Contraseñas no coinciden</b>")
			$('#divMensaje').addClass('alert alert-danger').html("<b>Las Contraseñas no coinciden</b>").show(200).delay(2500).hide(200);
			document.getElementById('txtNuevoPasswd').value="";	
			$('#divRepetirNuevoPasswd').removeClass();
			$('#divRepetirNuevoPasswd').addClass('form-group has-error');
		}
		else
		{
			$('#divRepetirNuevoPasswd').removeClass();
			$('#divRepetirNuevoPasswd').addClass('form-group has-success');
			return true;
		}		
	}
	else
	{
		return false;
	}	
}
function js_confirma_password_blur(pPass)
{	
	if(document.getElementById('txtNuevoPasswd').value!=pPass)
	{
		//Sexy.error("<b>Las Contraseñas no coinciden</b>")
		$('#divMensaje').addClass('alert alert-danger').html("<b>Las Contraseñas no coinciden</b>").show(200).delay(2500).hide(200);
		document.getElementById('txtRepetirNuevoPasswd').value="";	
		$('#divRepetirNuevoPasswd').removeClass();
		$('#divRepetirNuevoPasswd').addClass('form-group has-error');
	}
	else
	{
		$('#divRepetirNuevoPasswd').removeClass();
		$('#divRepetirNuevoPasswd').addClass('form-group has-success');
		return true;
	}
	return false;	
}

function jsValidarCamposPasswd()
{	
	vpassOld=$('#txtPasswdAnterior').val();
	vPass=$('#txtNuevoPasswd').val();
	vConfirmPass=$('#txtRepetirNuevoPasswd').val();
	veredicto=$('#hddVeredicto').val();
	
	
	vMensaje="<span style='color:red'>Los Siguientes Campos son requeridos: </span><br>";
	if (vpassOld=="" || vPass=="" || vConfirmPass=="")
	{
		if(vpassOld=="")
		{
			vMensaje=vMensaje+"<span style='color:red'>Contraseña Anterior<span><br>";
			$('#divPasswdAnterior').removeClass();
			$('#divPasswdAnterior').addClass('form-group has-error');
		}
		
		if(vPass=="")
		{
			vMensaje=vMensaje+"<span style='color:red'>Nueva Contraseña<span><br>";
			$('#divNuevoPasswd').removeClass();
			$('#divNuevoPasswd').addClass('form-group has-error');
		}
		
		if(vConfirmPass=="")
		{
			vMensaje=vMensaje+"<span style='color:red'>Repetir Nueva Contraseña<span><br>";
			$('#divRepetirNuevoPasswd').removeClass();
			$('#divRepetirNuevoPasswd').addClass('form-group has-error');
		}
		//Sexy.alert(vMensaje);
		$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(200).delay(3000).hide(200);
		return false;
	}
	
	if(veredicto>34)
	{
		jsCambiarPasswd();
		return true;
	}
	else
	{
		//Sexy.alert("<b>La Contraseña es muy debil, ingrese otra</b>");
		vMensaje="La Contraseña es muy debil, ingrese otra"
		$('#divMensaje').addClass('alert alert-danger').html(vMensaje).show(200).delay(4000).hide(200);
		$('#divPasswdAnterior').removeClass();
		$('#divNuevoPasswd').removeClass();
		$('#divRepetirNuevoPasswd').removeClass();
		$('#divPasswdAnterior').addClass('form-group');
		$('#divNuevoPasswd').addClass('form-group');
		$('#divRepetirNuevoPasswd').addClass('form-group');
		$('#frmCambiarPasswd')[0].reset();
		
		return false;
	}
}

function jsValidaPasswdNew()
{	
	
	var pPasswdNew=md5(allTrim($('#txtNuevoPasswd').val()));
	//alert(pPasswdAnterior);
	var url = '../php/login.php';
	var pAccion='VALIDA_PASSWD_NEW';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pAccion='+pAccion+'&pPasswdNew='+pPasswdNew,
		success:function(data){
		var array = eval(data);
			if(array[1]==1)
				{
					$('#divNuevoPasswd').removeClass();
					$('#divNuevoPasswd').addClass('form-group has-success');	
					//window.location='../php/';
				}
			else
				{
					$('#divNuevoPasswd').removeClass();
					$('#divNuevoPasswd').addClass('form-group has-error');
					$('#divNuevoPasswd').focus();
					$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(3500).hide(200);
				}
				
			}
	});	
}
function jsCambiarPasswd()
{		
	var pPasswdNew=md5(allTrim($('#txtNuevoPasswd').val()));
	//alert(pPasswdAnterior);
	var url = '../php/login.php';
	var pAccion='CAMBIAR_PASSWD';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pAccion='+pAccion+'&pPasswdNew='+pPasswdNew,
		success:function(data)
		{
			var array = eval(data);
			if(array[1]==1)
				{					
					$('#divMensaje').addClass('alert alert-success').html(array[0]).show(300).delay(3500).hide(200);
					window.location='../pages/login.html';
				}
			else
				{					
					$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(3500).hide(200);
				}
		}
			
	});	
}
