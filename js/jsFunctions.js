$(document).ready(function(){
	
	$('#alertas_gestion_documentos').on('click', function(){
			var url = '../php/gestion_documentos.php';
			var pAccion='LISTA_RECIBIDOS';
			$.ajax({
				type:'POST',
				url:url,
				data:'pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
				$('#page-wrapper').html(array[0]);
				}
			});		
			return false;		
	});	
	
});  

function jsOpcionesMenu(pNombreArchivo,pAccion)//	FUNCION QUE PERMITE CARGAR LAS OPCIONES DEL MENU
{
    //alert(pNombreArchivo+" "+pAccion);
	var url = "../php/"+pNombreArchivo;
	var vAccion="";
	var pTipoLista="";
	if (pAccion.substr(0,1)=='M' || pAccion.substr(0,1)=='C' ) {
		vAccion="LISTA";
		pTipoLista=pAccion.substr(0,1);
	}
	else
	{
		vAccion=pAccion;
		pTipoLista="N";
	}
	$.ajax({
		type:'POST',
		url:url,
		data:'pAccion='+vAccion+'&pTipoLista='+pTipoLista+'&pNombreArchivo='+pNombreArchivo,
		success:function(data){
			var array = eval(data);
			$('#page-wrapper').html(array[0]);
		}
	});
	return false;
}

function jsRefrescarForm(pUrl)
{
    window.setTimeout(function() {
        var url = "../php/"+pUrl;
        var pAccion='NUEVO';
        var pTipoLista='M';
        $.ajax({
            type:'POST',
            url:url,
            data:'pAccion='+pAccion+'&pTipoLista='+pTipoLista+'&pNombreArchivo='+pUrl,
            success:function(data){
                var array = eval(data);
                $('#page-wrapper').html(array[0]);
                }
            });
    },800);

}

/*INICIO FUNCIONES CRUD GENERALES*/
function jsGuardar(pNombreArchivo, pNombreFrm)
{
	var url = '../php/'+pNombreArchivo;
	var pAccion='GUARDAR';
	var validado = validarFormulario(pNombreFrm);
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,
				data:$('#'+pNombreFrm).serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#'+pNombreFrm)[0].reset();
							$('#divMensaje').addClass('alert alert-success').html(array[0]).show(200).delay(2500).hide(200);
                            jsRefrescarForm(pNombreArchivo);

						}
					else
						{
							$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(200).delay(2500).hide(200);
						}
					}
			});
		}
	return false;
}

function jsVerPantallaModificar(pId, pNombreArchivo, pNombreModal)
{
	//var url = '../php/menu_sistema.php';
	var url = '../php/'+pNombreArchivo;
	var pAccion='VER_PANTALLA_MODIFICAR';
	$.ajax({
		type:'POST',
		url:url,
		data:'pId='+pId+'&pAccion='+pAccion+'&pNombreArchivo='+pNombreArchivo,
		success:function(data){
				var array = eval(data);
				$('#'+pNombreModal).html(array[0]);
		}
	});
}

function jsActualizar(pNombreArchivo, pNombreFrm)
{
	var url = '../php/'+pNombreArchivo;
	var pAccion='ACTUALIZAR';
	var validado = validarFormulario(pNombreFrm);
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,
				data:$('#'+pNombreFrm).serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#divMensajeModal').addClass('alert alert-success').html(array[0]).show(200).delay(2000).hide(150);
						}
					else
						{
							$('#divMensajeModal').addClass('alert alert-danger').html(array[0]).show(200).delay(2000).hide(150);
						}
					}
			});
		}
	return false;
}

function jsAnularActivar(pIdCodigo,pUrl)
{
	var url = '../php/'+pUrl;
	var pAccion='ANULA_ACTIVA';
	$.ajax({
		type:'POST',
		url:url,
		data:'pAccion='+pAccion+'&pIdCodigo='+pIdCodigo,
		success:function(data){
		var array = eval(data);
			if(array[1]==1)
				{
					$('#divMensaje').addClass('alert alert-success').html(array[0]).show(300).delay(2500).hide(200);
				}
			else
				{
					$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(2500).hide(200);
				}
			}
	});
	jsRefrescarForm(pUrl);
}
/*FIN FUNCIONES CRUD GENERALES*/

/*INICIO FUNCIONES PERSONALIZADAS*/

function jsRefrescarForm2(pUrl,pId)
{
    window.setTimeout(function() {
        var url = "../php/"+pUrl;
    var pAccion='NUEVO';
    $.ajax({
        type:'POST',
        url:url,
        data:'pAccion='+pAccion+'&pNombreArchivo='+pUrl+'&pId='+pId,
        success:function(data){
            var array = eval(data);
            $('#page-wrapper').html(array[0]);
            }
        });
    },800);
}
function jsRefrescarForm3(pUrl,pIdEdificio,pIdNivel)
{
    window.setTimeout(function() {
        var url = "../php/"+pUrl;
    var pAccion='NUEVO';
    $.ajax({
        type:'POST',
        url:url,
        data:'pAccion='+pAccion+'&pNombreArchivo='+pUrl+'&pIdEdificio='+pIdEdificio+'&pIdNivel='+pIdNivel,
        success:function(data){
            var array = eval(data);
            //$('#'+pNombreModal).modal('hide');
            $('#page-wrapper').html(array[0]);
            }
        });
    },800);
}

function jsRefrescarForm4(pUrl, pIdEdificio, pIdNivel, pIdAreaNivel)
{
    window.setTimeout(function() {
        var url = "../php/"+pUrl;
        var pAccion='NUEVO';
        jsAbrirAreaNivelElemento(pUrl,pAccion, pIdEdificio, pIdNivel, pIdAreaNivel)
    },800);
}
function jsAbrirNivel(pNombreArchivo,pAccion, pId)//	FUNCION QUE PERMITE CARGAR LAS OPCIONES DEL NIVEL
{
	var url = "../php/"+pNombreArchivo;
	$.ajax({
		type:'POST',
		url:url,
		data:'pAccion='+pAccion+'&pNombreArchivo='+pNombreArchivo+'&pId='+pId,
		success:function(data){
			var array = eval(data);
			$('#page-wrapper').html(array[0]);
		}
	});
	return false;
}
function jsAbrirAreaNivel(pNombreArchivo,pAccion, pIdEdificio, pIdNivel)//	FUNCION QUE PERMITE CARGAR LAS OPCIONES DEL NIVEL Y ELEMENTOS
{
	var url = "../php/"+pNombreArchivo;
	$.ajax({
		type:'POST',
		url:url,
		data:'pAccion='+pAccion+'&pNombreArchivo='+pNombreArchivo+'&pIdEdificio='+pIdEdificio+'&pIdNivel='+pIdNivel,
		success:function(data){
			var array = eval(data);
			$('#page-wrapper').html(array[0]);
		}
	});
	return false;
}
function jsAbrirAreaNivelElemento(pNombreArchivo,pAccion, pIdEdificio, pIdNivel, pIdAreaNivel)//	FUNCION QUE PERMITE CARGAR LAS OPCIONES DEL NIVEL Y ELEMENTOS
{
	var url = "../php/"+pNombreArchivo;
	$.ajax({
		type:'POST',
		url:url,
		data:'pAccion='+pAccion+'&pNombreArchivo='+pNombreArchivo+'&pIdEdificio='+pIdEdificio+'&pIdNivel='+pIdNivel+'&pIdAreaNivel='+pIdAreaNivel,
		success:function(data){
			var array = eval(data);
			$('#page-wrapper').html(array[0]);
		}
	});
	return false;
}
function jsAbrirAreaElemento(pNombreArchivo,pAccion, pIdEdificio, pIdNivel)//	FUNCION QUE PERMITE CARGAR LAS OPCIONES DEL NIVEL Y ELEMENTOS
{
	var url = "../php/"+pNombreArchivo;
	$.ajax({
		type:'POST',
		url:url,
		data:'pAccion='+pAccion+'&pNombreArchivo='+pNombreArchivo+'&pIdEdificio='+pIdEdificio+'&pIdNivel='+pIdNivel,
		success:function(data){
			var array = eval(data);
			$('#page-wrapper').html(array[0]);
		}
	});
	return false;
}
function jsAnularActivarEdificio(pIdNivel,pUrl,pIdEdificio)
{
	var url = '../php/'+pUrl;
	var pAccion='ANULA_ACTIVA';
	$.ajax({
		type:'POST',
		url:url,
		data:'pAccion='+pAccion+'&pIdEdificio='+pIdEdificio+'&pIdCodigo='+pIdNivel,
		success:function(data){
		var array = eval(data);
			if(array[1]==1)
				{
					$('#divMensaje').addClass('alert alert-success').html(array[0]).show(300).delay(2500).hide(200);
				}
			else
				{
					$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(2500).hide(200);
				}
			}
	});

    jsRefrescarForm2(pUrl,pIdEdificio);
}
function jsAnularActivar3(pIdCodigo, pUrl, pIdEdificio, pIdNivel)
{
	var url = '../php/'+pUrl;
	var pAccion='ANULA_ACTIVA';
	$.ajax({
		type:'POST',
		url:url,
		data:'pAccion='+pAccion+'&pIdEdificio='+pIdEdificio+'&pIdNivel='+pIdNivel+"&pIdCodigo="+pIdCodigo,
		success:function(data){
		var array = eval(data);
			if(array[1]==1)
				{
					$('#divMensaje').addClass('alert alert-success').html(array[0]).show(300).delay(2500).hide(200);
				}
			else
				{
					$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(2500).hide(200);
				}
			}
	});

    jsRefrescarForm3(pUrl,pIdEdificio,pIdNivel)
}
function jsAnularActivar4(pIdCodigo, pUrl, pIdEdificio, pIdNivel, pIdAreaNivel)
{
	var url = '../php/'+pUrl;
	var pAccion='ANULA_ACTIVA';
	$.ajax({
		type:'POST',
		url:url,
		data:'pAccion='+pAccion+'&pIdEdificio='+pIdEdificio+'&pIdNivel='+pIdNivel+"&pIdCodigo="+pIdCodigo,
		success:function(data){
		var array = eval(data);
			if(array[1]==1)
				{
					$('#divMensaje').addClass('alert alert-success').html(array[0]).show(300).delay(2500).hide(200);
				}
			else
				{
					$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(300).delay(2500).hide(200);
				}
			}
	});

    jsRefrescarForm4(pUrl, pIdEdificio, pIdNivel, pIdAreaNivel)
}
function jsVerPantallaModificar2(pId, pNombreArchivo, pNombreModal,pIdEdificio)
{
	//var url = '../php/menu_sistema.php';
	var url = '../php/'+pNombreArchivo;
	var pAccion='VER_PANTALLA_MODIFICAR';
	$.ajax({
		type:'POST',
		url:url,
		data:'pId='+pId+'&pAccion='+pAccion+'&pNombreArchivo='+pNombreArchivo+'&pIdEdificio='+pIdEdificio,
		success:function(data){
				var array = eval(data);
				$('#'+pNombreModal).html(array[0]);
		}
	});
}
function jsVerPantallaModificar3(pId, pNombreArchivo, pNombreModal, pIdEdificio, pIdNivel, pIdAreaNivel)
{
	//var url = '../php/menu_sistema.php';
	var url = '../php/'+pNombreArchivo;
	var pAccion='VER_PANTALLA_MODIFICAR';
	$.ajax({
		type:'POST',
		url:url,
		data:'pId='+pId+'&pAccion='+pAccion+'&pNombreArchivo='+pNombreArchivo+'&pIdEdificio='+pIdEdificio+'&pIdNivel='+pIdNivel+'&pIdAreaNivel='+pIdAreaNivel,
		success:function(data){
				var array = eval(data);
				$('#'+pNombreModal).html(array[0]);
		}
	});
}

function jsGuardar2(pNombreArchivo, pNombreFrm, pId)
{
	var url = '../php/'+pNombreArchivo;
	var pAccion='GUARDAR';
	var validado = validarFormulario(pNombreFrm);
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,
				data:$('#'+pNombreFrm).serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#'+pNombreFrm)[0].reset();
							$('#divMensaje').addClass('alert alert-success').html(array[0]).show(200).delay(2500).hide(200);
                            jsRefrescarForm2(pNombreArchivo, pId);

						}
					else
						{
							$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(200).delay(2500).hide(200);
						}
					}
			});
		}
	return false;
}

function jsActualizar2(pNombreArchivo, pNombreFrm)
{
	var url = '../php/'+pNombreArchivo;
	var pAccion='ACTUALIZAR';
	var validado = validarFormulario(pNombreFrm);
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,
				data:$('#'+pNombreFrm).serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#divMensajeModal').addClass('alert alert-success').html(array[0]).show(200).delay(2000).hide(150);

						}
					else
						{
							$('#divMensajeModal').addClass('alert alert-danger').html(array[0]).show(200).delay(2000).hide(150);
						}
					}
			});
		}
	return false;
}
function jsGuardar3(pNombreArchivo, pNombreFrm, pIdEdificio, pIdNivel)
{
	var url = '../php/'+pNombreArchivo;
	var pAccion='GUARDAR';
	var validado = validarFormulario(pNombreFrm);
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,
				data:$('#'+pNombreFrm).serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#'+pNombreFrm)[0].reset();
							$('#divMensaje').addClass('alert alert-success').html(array[0]).show(200).delay(2500).hide(200);
                            jsRefrescarForm3(pNombreArchivo,pIdEdificio,pIdNivel)

						}
					else
						{
							$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(200).delay(2500).hide(200);
						}
					}
			});
		}
	return false;
}
function jsGuardar4(pNombreArchivo, pNombreFrm, pIdEdificio, pIdNivel, pIdAreaNivel)
{
	var url = '../php/'+pNombreArchivo;
	var pAccion='GUARDAR';
	var validado = validarFormulario(pNombreFrm);
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,
				data:$('#'+pNombreFrm).serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#'+pNombreFrm)[0].reset();
							$('#divMensaje').addClass('alert alert-success').html(array[0]).show(200).delay(2500).hide(200);
                            jsRefrescarForm4(pNombreArchivo, pIdEdificio, pIdNivel, pIdAreaNivel);

						}
					else
						{
							$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(200).delay(2500).hide(200);
						}
					}
			});
		}
	return false;
}

function jsComboNiveles(pNombreArchivo, pSelected, pId)//	FUNCION QUE PERMITE CARGAR LAS OPCIONES DEL NIVEL
{
	var url = "../php/"+pNombreArchivo;
    var pAccion="COMBO"
	$.ajax({
		type:'POST',
		url:url,
		data:'pAccion='+pAccion+'&pNombreArchivo='+pNombreArchivo+'&pSelected='+pSelected+'&pId='+pId,
		success:function(data){
			var array = eval(data);
			$('#divCbNiveles').html(array[0]);
		}
	});
	return false;
}
function jsVerReporte(pNombreArchivo)//	FUNCION QUE PERMITE CARGAR LAS OPCIONES DEL NIVEL
{
	var url = "../php/"+pNombreArchivo;
    var pAccion="VER_REPORTE"
    var pIdEdificio=$('#cbEdificio').val();
    var pIdNivel=$('#cbNiveles').val();
	$.ajax({
		type:'POST',
		url:url,
		data:'pAccion='+pAccion+'&pNombreArchivo='+pNombreArchivo+'&pIdEdificio='+pIdEdificio+'&pIdNivel='+pIdNivel,
		success:function(data){
			var array = eval(data);
			$('#divReporte').html(array[0]);
		}
	});
	return false;
}
/*FIN FUNCIONES PERSONALIZADAS*/

/*INICIO FUNCIONES SEGURIDADES*/
function jsGuardarSeguridades()
{
	var url = '../php/seguridades.php';
	var pAccion='GUARDAR';
	var validado = validarFormulario('frmSeguridad');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,
				data:$('#frmSeguridad').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#frmSeguridad')[0].reset();
							$('#divMensaje').addClass('alert alert-success').html(array[0]).show(200).delay(2500).hide(200);
						}
					else
						{
							$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(200).delay(2500).hide(200);
						}
					}
			});
		}
	return false;

}
function jsActualizarSeguridades()
{
	var url = '../php/seguridades.php';
	var pAccion='ACTUALIZAR';
	var validado = validarFormulario('frmSeguridad');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,				
				data:$('#frmSeguridad').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{					
							$('#divMensajeModal').addClass('alert alert-success').html(array[0]).show(200).delay(2000).hide(150);	
						}
					else
						{
							$('#divMensajeModal').addClass('alert alert-danger').html(array[0]).show(200).delay(2000).hide(150);	
						}
					}
			});	
		}
	return false;
		
}
function jsConsultarParametroSeguridad(pIdParametroSeguridad)
{
	var url = '../php/seguridades.php';
	var pAccion='CONSULTA';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pIdParametroSeguridad='+pIdParametroSeguridad+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModConsultaSeguridad').html(array[0]);
		}
	});		
}
function jsModificarParametroSeguridad(pIdParametroSeguridad)
{
	var url = '../php/seguridades.php';
	var pAccion='MODIFICA';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pIdParametroSeguridad='+pIdParametroSeguridad+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModModificaSeguridad').html(array[0]);
		}
	});		
}
/*FIN FUNCIONES SEGURIDADES*/
/*INICIO FUNCIONES PERFILES*/
function jsGuardarPerfil()
{
	var url = '../php/perfiles.php';
	var pAccion='GUARDAR';
	var validado = validarFormulario('frmPerfil');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,				
				data:$('#frmPerfil').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#frmPerfil')[0].reset();
							$('#divMensaje').addClass('alert alert-success').html(array[0]).show(200).delay(2500).hide(200);	
						}
					else
						{
							$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(200).delay(2500).hide(200);	
						}
					}
			});	
		}
	return false;
		
}
function jsActualizarPerfil()
{
	var url = '../php/perfiles.php';
	var pAccion='ACTUALIZAR';
	var validado = validarFormulario('frmPerfil');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,				
				data:$('#frmPerfil').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{					
							$('#divMensajeModal').addClass('alert alert-success').html(array[0]).show(200).delay(2000).hide(150);	
						}
					else
						{
							$('#divMensajeModal').addClass('alert alert-danger').html(array[0]).show(200).delay(2000).hide(150);	
						}
					}
			});		
		}
	return false;
}
function jsConsultarPerfilUsuario(pIdPerfil)
{
	var url = '../php/perfiles.php';
	var pAccion='CONSULTA';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pIdPerfil='+pIdPerfil+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModConsultaPerfil').html(array[0]);
		}
	});		
}
function jsModificarPerfilUsuario(pIdPerfil)
{
	var url = '../php/perfiles.php';
	var pAccion='MODIFICA';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pIdPerfil='+pIdPerfil+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModModificarPerfil').html(array[0]);
		}
	});		
}

/*FIN FUNCIONES PERFILES*/
/*INICIO FUNCIONES MENU SISTEMA*/
function jsGuardarMenuSistema()
{
	var url = '../php/menu_sistema.php';
	var pAccion='GUARDAR';
	var validado = validarFormulario('frmMenuSistema');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,				
				data:$('#frmMenuSistema').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#frmMenuSistema')[0].reset();
							$('#divMensaje').addClass('alert alert-success').html(array[0]).show(200).delay(2500).hide(200);	
						}
					else
						{
							$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(200).delay(2500).hide(200);	
						}
					}
			});	
		}
	return false;
		
}
function jsActualizarMenuSistema()
{
	var url = '../php/menu_sistema.php';
	var pAccion='ACTUALIZAR';
	var validado = validarFormulario('frmMenuSistema');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,				
				data:$('#frmMenuSistema').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{					
							$('#divMensajeModal').addClass('alert alert-success').html(array[0]).show(200).delay(2000).hide(150);	
						}
					else
						{
							$('#divMensajeModal').addClass('alert alert-danger').html(array[0]).show(200).delay(2000).hide(150);	
						}
					}
			});		
		}
	return false;
}
function jsConsultarMenuSistema(pIdMenu)
{
	var url = '../php/menu_sistema.php';
	var pAccion='CONSULTA';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pIdMenu='+pIdMenu+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModConsultaMenuSistema').html(array[0]);
		}
	});		
}
function jsModificarMenuSistema(pIdMenu)
{
	var url = '../php/menu_sistema.php';
	var pAccion='MODIFICA';
	$.ajax({
		type:'POST',
		url:url,
		data:'pIdMenu='+pIdMenu+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModModificarMenuSistema').html(array[0]);
		}
	});
}

/*FIN FUNCIONES MENU SISTEMA*/
/*INICIO FUNCIONES OPCIONES*/
function jsGuardarOpcionSistema()
{
	var url = '../php/opciones_sistema.php';
	var pAccion='GUARDAR';
	var validado = validarFormulario('frmOpcionesSistema');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,				
				data:$('#frmOpcionesSistema').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#frmOpcionesSistema')[0].reset();
							$('#divMensaje').addClass('alert alert-success').html(array[0]).show(200).delay(2500).hide(200);	
						}
					else
						{
							$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(200).delay(2500).hide(200);	
						}
					}
			});		
		}
	return false;	
	
}
function jsConsultarOpciones(pIdOpciones)
{
	var url = '../php/opciones_sistema.php';
	var pAccion='CONSULTA';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pIdOpciones='+pIdOpciones+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModConsultaOpciones').html(array[0]);
		}
	});		
}
function jsModificarOpciones(pIdOpciones)
{
	var url = '../php/opciones_sistema.php';
	var pAccion='MODIFICA';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pIdOpciones='+pIdOpciones+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModModificarOpciones').html(array[0]);
		}
	});		
}
function jsActualizarOpciones()
{
	var url = '../php/opciones_sistema.php';
	var pAccion='ACTUALIZAR';
	var validado = validarFormulario('frmOpcionesSistema');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,				
				data:$('#frmOpcionesSistema').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{					
							$('#divMensajeModal').addClass('alert alert-success').html(array[0]).show(200).delay(2000).hide(150);	
						}
					else
						{
							$('#divMensajeModal').addClass('alert alert-danger').html(array[0]).show(200).delay(2000).hide(150);	
						}
					}
			});		
		}
	return false;
}
/*FIN FUNCIONES OPCIONES*/

/*INICIO FUNCIONES ACCESOS*/
function jsGuardarAccesoSistema()
{
	var url = '../php/accesos_sistema.php';
	var pAccion='GUARDAR';
	var validado = validarFormulario('frmAccesoSistema');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,				
				data:$('#frmAccesoSistema').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#frmAccesoSistema')[0].reset();
							$('#divMensaje').addClass('alert alert-success').html(array[0]).show(200).delay(2500).hide(200);	
						}
					else
						{
							$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(200).delay(2500).hide(200);	
						}
					}
			});	
			setTimeout("var url = '../php/accesos_sistema.php';var pAccion='NUEVO';$.ajax({type:'POST',url:url,data:'pAccion='+pAccion,success:function(data){var array = eval(data);$('#page-wrapper').html(array[0]);}});",800);	
		}
		
	return false;	
}
function jsModificarAccesoSistema(pIdAcceso)
{
	var url = '../php/accesos_sistema.php';
	var pAccion='MODIFICA';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pIdAcceso='+pIdAcceso+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModModificarAccesos').html(array[0]);
		}
	});		
}
function jsActualizarAccesoSistema()
{
	var url = '../php/accesos_sistema.php';
	var pAccion='ACTUALIZAR';
	var validado = validarFormulario('frmAccesoSistema');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,				
				data:$('#frmAccesoSistema').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{					
							$('#divMensajeModal').addClass('alert alert-success').html(array[0]).show(200).delay(2000).hide(150);	
						}
					else
						{
							$('#divMensajeModal').addClass('alert alert-danger').html(array[0]).show(200).delay(2000).hide(150);	
						}
					}
			});		
		}
	return false;
}

/*FIN FUNCIONES ACCESOS*/

/*INICIO FUNCIONES USUARIOS*/
function jsGuardarUsuario()
{
	var url = '../php/usuarios.php';
	var pAccion='GUARDAR';
	var validado = validarFormulario('frmNuevoUsuario');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,				
				data:$('#frmNuevoUsuario').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{
							$('#frmNuevoUsuario')[0].reset();
							$('#divMensaje').addClass('alert alert-success').html(array[0]).show(300).delay(2500).hide(200);	
						}
					else
						{
							$('#divMensaje').addClass('alert alert-danger').html(array[0]).show(200).delay(2500).hide(200);	
						}
					}
			});	
		}
	return false;		
}
function jsActualizarUsuario()
{
	var url = '../php/usuarios.php';
	var pAccion='ACTUALIZAR';
	var validado = validarFormulario('frmModificaUsuario');
    if(validado)
		{
			$.ajax({
				type:'POST',
				url:url,				
				data:$('#frmModificaUsuario').serialize()+'&pAccion='+pAccion,
				success:function(data){
				var array = eval(data);
					if(array[1]==1)
						{					
							$('#divMensajeModal').addClass('alert alert-success').html(array[0]).show(300).delay(2500).hide(200);	
						}
					else
						{
							$('#divMensajeModal').addClass('alert alert-danger').html(array[0]).show(200).delay(2500).hide(200);	
						}
					}
			});		
		}
	return false;
	
}
function jsModificarUsuario(pIdCodigo)
{
	var url = '../php/usuarios.php';
	var pAccion='MODIFICA';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pIdCodigo='+pIdCodigo+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModModificarUsuario').html(array[0]);
		}
	});		
}
function jsConsultarUsuario(pIdCodigo)
{
	var url = '../php/usuarios.php';
	var pAccion='CONSULTA';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pIdCodigo='+pIdCodigo+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModConsultaUsuario').html(array[0]);
		}
	});		
}
function jsDatosUsuario()
{
	var url = '../php/usuarios.php';
	var pAccion='DATOS_USUARIO';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#page-wrapper').html(array[0]);
		}
	});		
}

function js_crear_nombre_usuario(pForm)
{
	try
	{
		var vNombre1=allTrim(document.getElementById('txtNombre1').value);
		var vApellido1=allTrim(document.getElementById('txtApellido1').value);
		var vApellido2=String(allTrim(document.getElementById('txtApellido2').value)).substr(0,1);
		var vNombreUsuario=String(vNombre1+"."+vApellido1+"."+vApellido2).toLowerCase();
		//document.getElementById('txtNombreUsuario').value=vNombreUsuario;
		//alert(vNombreUsuario);
		var url = '../php/usuarios.php';
		var pAccion='VALIDA_USUARIO';
		var vForm='#'+pForm;
		
		if(vNombre1=='' || vApellido1=='' || vApellido2=='')
			{
				return false;
			}
		$.ajax({
			type:'POST',
			url:url,				
			data:'&pAccion='+pAccion+'&txtNombreUsuario='+vNombreUsuario,
			success:function(data){
			var array = eval(data);
				//$('#divMensaje').html(array[0]);
			if(array[0]==0)
				{
					document.getElementById('txtNombreUsuario').value=vNombreUsuario;	
				}
			else
				{
					document.getElementById('txtNombreUsuario').value=vNombreUsuario+array[0];	
				}
			}
		});
		
	}
	catch(er)
	{
		alert('Se produjo un error. Referencia: '+er.message + ' Tipo de Error: '+er.error)
	}
}
/*FIN FUNCIONES USUARIOS*/

/*INICIO FUNCIONES AUDITORIA*/
function jsConsultarAuditoria(pId)
{
	var url = '../php/consultar_pista_auditoria.php';
	var pAccion='CONSULTA';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pId='+pId+'&pAccion='+pAccion,
		success:function(data){
				var array = eval(data);
				$('#ModConsultaAuditoria').html(array[0]);
		}
	});		
}
/*FIN FUNCIONES AUDITORIA*/
/*INICIO FUNCIONES GENERALES*/
function jsExportarExcel()
{
    var pIdEdificio=$("#cbEdificio").val();
    var pIdNivel=$("#cbNiveles").val();

	var vruta="exportar.php?pIdEdificio="+pIdEdificio+"&pIdNivel="+pIdNivel;
	//alert(vruta);
	window.open(vruta, 'ingwin', 'width='+screen.availWidth+', height='+screen.availHeight+',dependent=yes, location=no top=250, left=200, scrollbars=yes,resizable=yes,directories=yes');return false;
}

function jsExportarExcelEvaluacion()
{
    var pIdEdificio=$("#cbEdificio").val();
    var pIdNivel=$("#cbNiveles").val();

	var vruta="exportar_evaluacion.php?pIdEdificio="+pIdEdificio+"&pIdNivel="+pIdNivel;
	//alert(vruta);
	window.open(vruta, 'ingwin', 'width='+screen.availWidth+', height='+screen.availHeight+',dependent=yes, location=no top=250, left=200, scrollbars=yes,resizable=yes,directories=yes');return false;
}
function lTrim(sStr)
{
 while (sStr.charAt(0) == " ")

  sStr = sStr.substr(1, sStr.length - 1);

 return sStr;
}
function rTrim(sStr)
{
 while (sStr.charAt(sStr.length - 1) == " ")

  sStr = sStr.substr(0, sStr.length - 1);

 return sStr;
}
function allTrim(sStr)
{
 return rTrim(lTrim(sStr));
}
function jsRefrescarLista(pUrl)
{
	setTimeout("var url = '../php/"+pUrl+"'; var pAccion='LISTA'; var pTipoLista='M';$.ajax({type:'POST',		url:url,data:'pAccion='+pAccion+'&pTipoLista='+pTipoLista,success:function(data){var array = eval(data);				$('#page-wrapper').html(array[0]);}});",800);
}

function js_solo_mayusculas(value, id)
{
	try
	{
		document.getElementById(id).value=document.getElementById(id).value.toUpperCase();
	}
	catch(er)
	{
		alert('Se produjo un error. Referencia: '+er.message + ' Tipo de Error: '+er.error)
	}
}
function js_solo_minusculas(value, id)
{
	try
	{
		document.getElementById(id).value=document.getElementById(id).value.toLowerCase();
	}
	catch(er)
	{
		alert('Se produjo un error. Referencia: '+er.message + ' Tipo de Error: '+er.error)
	}
}
function js_upload()
{//Funcion encargada de enviar el archivo via AJAX
	var validado = validarFormulario('frmGestionDocumentos');
    if(validado)
		{
			$("#divMensaje").text('Cargando...');
			var inputFileImage = document.getElementById("fileToUpload");
			var file = inputFileImage.files[0];
			var data = new FormData();
			var pAnio=document.getElementById('cbAnios').value;
			var pCarrera=document.getElementById('cbCarreras').value;
			var pUnidadOrganizativa=document.getElementById('cbUnidadOrganizativa').value;
			var pPeriodo=document.getElementById('cbPeriodos').value;
			var pMes=document.getElementById('cbMeses').value;
			var pResumenDoc=document.getElementById('txResumenDoc').value;
			var pTipoDoc=document.getElementById('cbTipoDocumento').value;
			document.cookie = "pAnio="+pAnio;
			document.cookie = "pCarrera="+pCarrera;
			document.cookie = "pUnidadOrganizativa="+pUnidadOrganizativa;
			document.cookie = "pPeriodo="+pPeriodo;
			document.cookie = "pMes="+pMes;
			document.cookie = "pResumenDoc="+pResumenDoc;
			document.cookie = "pTipoDoc="+pTipoDoc;
			data.append('fileToUpload',file);

			$.ajax({
				url: "../php/guardar_documento.php",        // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: data, 			  // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{			
					$("#divMensaje").html(data);
					window.setTimeout(function() {
					$(".alert-dismissible").fadeTo(500, 0).slideUp(500, function(){
					$(this).remove();
					});	}, 5000);
					$('#frmGestionDocumentos')[0].reset();
				}
			});	
		}
	return false;
}

function jsLogout()
{	
	var url = '../php/login.php';
	var pAccion='LOGOUT';
	$.ajax({
		type:'POST',
		url:url,				
		data:'pAccion='+pAccion,
		success:function(data){
		var array = eval(data);
		//$('#divMensaje').addClass('alert alert-success').html(array[0]).show(300).delay(2500).hide(200);	
		window.location='../index.html';
			}
	});	
}
function jsCambiarPasswd()
{	
	window.location='../php/cambiar_password.php';
	
}

/*FIN FUNCIONES GENERALES*/

