<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pAccion               =$_POST["pAccion"];
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'FILTROS':
        fCargarFormulario();
    break;
    case 'LISTA':
        fGeneraLista();
    break;
    case 'CONSULTA':
        $_pId     =$_POST['pId'];
        fConsultarAuditoria($_pId);
    break;
}


function fCargarFormulario()
{
    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Nuevo Men&uacute; del Sistema
                        </div>
                        <div class="panel-body">
                         <form role="form" id="frmPistaAuditoria" >
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label>Nombre del Men&uacute;</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            <input class="form-control" id="txtNombreMenu" name="txtNombreMenu">
                                            <p class="help-block with-errors">Ingrese el nombre del nuevo men&uacute;.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="btnGuardarMenuSistema" onClick="jsGuardarMenuSistema();">Guardar</button>
                           <button type="button" class="btn btn-primary">Limpiar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    //$_form="ENTRO";
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}
function fGeneraLista()
{
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="select a.id, a.fecha, a.hora, a.ip, a.usuario, a.base, a.comando, substr(a.sql,0,80) as sql
                from tbl_auditoria_sistema a
                order by a.fecha DESC, a.hora DESC
                limit 100000";
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">';
        $_form.='           Consultar Pistas Auditoria';
        $_form.='       </div>
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="ver_auditoria">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Fecha</th>
                                        <th>Hora</th>
                                        <th>IP</th>
                                        <th>Usuario</th>
                                        <th>SQL</th>
                                        <th>Comando</th>
                                        <th>Base de Datos</th>
                                        <th>&nbsp;</th>
                                        ';
        $_form.='                   </tr>
                                </thead>
                                <tbody>';
        foreach ($_vlv_Resultado as $_data)
        {
            if($_data["fecha"])
                $vldFecha=date('d/m/Y',strtotime($_data["fecha"]));
            ELSE
                $vldFecha="";
            $_form.='                   <tr class="odd gradeA">
                                            <td class="center">'.$_data["id"].'</td>
                                            <td class="center">'.$vldFecha.'</td>
                                            <td class="center">'.$_data["hora"].'</td>
                                            <td class="center">'.$_data["ip"].'</td>
                                            <td class="center">'.$_data["usuario"].'</td>
                                            <td class="center">'.$_data["sql"].' <a href="#"  data-toggle="modal" data-target="#ModConsultaAuditoria" onClick="jsConsultarAuditoria('.$_data["id"].')">M&aacute;s ...</a></td>
                                            <td class="center">'.$_data["comando"].'</td>
                                            <td class="center">'.$_data["base"].'</td>';
            $_form.='                       <td align="center">
                                                <button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModConsultaAuditoria"
                                                onClick="jsConsultarAuditoria('.$_data["id"].')">
                                                    Detalle
                                                </button>
                                            </td>';
            $_form.='                   </tr>';
        }
        $_form.='
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <script>
            $(document).ready(function() {
                $("#ver_auditoria").DataTable({
                    responsive: true,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                        "zeroRecords": "No se encontraron registros",
                        "info": "P&aacutegina _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros",
                        "sSearch":         "Buscar:",
                        "oPaginate": {
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    },
                    "order": [[ 1, "desc" ]]
                });
            });
            </script>
            <div class="modal fade" id="ModConsultaAuditoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';

        $array = array(0 => $_form);

        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fConsultarAuditoria($_pId) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="select a.id, a.fecha, a.hora, a.ip, a.usuario, a.base, a.comando, a.sql
                from tbl_auditoria_sistema a
                where a.id=".$_pId;
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        foreach ($_vlv_Resultado as $_data)
        {
            if($_data["fecha"])
                $vldFecha=date('d/m/Y',strtotime($_data["fecha"]));
            ELSE
                $vldFecha="";
            $_form='<!-- Modal -->
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <div class="col-lg-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            Consulta Pista Auditoria - ID: '.$_data["id"].'
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>Fecha del Registro</label>
                                                            <input class="form-control" id="txtNombreMenu" name="txtNombreMenu" value="'.$vldFecha.'" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Ip</label>
                                                            <input class="form-control" id="txtIp" name="txtIp" value="'.$_data["ip"].'" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Comando Ejecutado</label>
                                                            <input class="form-control" id="txtComando" name="txtComando" value="'.$_data["comando"].'" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>Hora del Registro</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                            <input class="form-control" id="txtHora" name="txtHora" value="'.$_data["hora"].'" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Usuario</label>
                                                            <input class="form-control" id="txtUsuario" name="txtUsuario" value="'.$_data["usuario"].'" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Nombre de la Base</label>
                                                            <input class="form-control" id="txtBase" name="txtBase"  value="'.$_data["base"].'" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label>Sentencia SQL</label>
                                                        <textarea class="form-control" rows="2" id="txSql" name="txSql" readonly>'.$_data["sql"].'</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.row (nested) -->
                                        </div>
                                    </div>
                                    <!-- /.panel -->
                                </div>
                            </div><!-- /.modal-body -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->

                <!-- /.modal -->';
        }

        $array = array(0 => $_form);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}


?>