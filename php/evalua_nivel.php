<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pAccion               =$_POST["pAccion"];
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$pNombreTabla="tbl_nivel";
$pNombreDataTable="ver_nivel";
$pNombreModal="ModModificarNivel";
$pNombreHeader="Nivel(es) de Edificio";
$pPlaceHolder="Ingrese la descripci&oacute;n";
$pPlaceHolder1="Ingrese el Largo";
$pPlaceHolder2="Ingrese el Alto";
$pPlaceHolder3="Ingrese el Ancho";

$p_funcion="SP_GUARDAR_NIVEL";
$p_funcionAnulaActiva="SP_ANULAR_ACTIVAR_NIVEL";

/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $_pId               =$_POST["pId"];
        //echo $_pId;
        fCargarFormulario($_pId);
    break;
    case 'CONSULTAR':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $_pId               =$_POST["pId"];
        //echo $_pId;
        fConsultarNiveles($_pId);
    break;

    case 'GUARDAR':
        $_pIdEdificio       =$_POST['hddIdCodigoEdificio'];
        $_pAncho            =$_POST['txtAncho'];
        $_pAlto             =$_POST['txtAlto'];
        $_pTipoNivel        =$_POST['cbTipoNivel'];
        $_pFondo            =$_POST['txtFondo'];
        $_pTipoEstructura   =$_POST['cbTipoEstructura'];
        fGuardar($_pTipoNivel, $_pAncho, $_pAlto, $_pFondo, $_pTipoEstructura, $_pIdEdificio);
    break;
    case 'ACTUALIZAR':
        $_pId               =$_POST['hddId'];
        $_pIdEdificio       =$_POST['hddIdCodigoEdificio'];
        $_pAncho            =$_POST['txtAncho'];
        $_pAlto             =$_POST['txtAlto'];
        $_pTipoNivel        =$_POST['cbTipoNivel'];
        $_pFondo            =$_POST['txtFondo'];
        $_pTipoEstructura   =$_POST['cbTipoEstructura'];
        fActualizar($_pId, $_pTipoNivel, $_pAncho, $_pAlto, $_pFondo, $_pTipoEstructura, $_pIdEdificio);
    break;
    case 'LISTA':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        fGeneraLista();
    break;
    case 'VER_PANTALLA_MODIFICAR':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $_pId               =$_POST['pId'];
        $_pIdEdificio       =$_POST['pIdEdificio'];
        fVerPantallaModificar($_pId,$_pIdEdificio);
    break;
    case 'ANULA_ACTIVA':
        $_pIdOpcion         =$_POST['pIdCodigo'];
        fAnularActivar($_pIdOpcion);
    break;
}

function fGuardar($_pTipoNivel, $_pAncho, $_pAlto, $_pFondo, $_pTipoEstructura,$_pIdEdificio)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $_pId="null";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pTipoNivel.",".$_pAncho.",".$_pAlto.",".$_pFondo.",". $_pTipoEstructura.",".$_pIdEdificio.",".$_pId;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

function fActualizar($_pId, $_pTipoNivel, $_pAncho, $_pAlto, $_pFondo, $_pTipoEstructura,$_pIdEdificio)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pTipoNivel.",".$_pAncho.",".$_pAlto.",".$_pFondo.",". $_pTipoEstructura.",".$_pIdEdificio.",".$_pId;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario($pId)
{
    global $_pNombreArchivo, $pNombreTabla,$pNombreDataTable,$pNombreModal, $pNombreFrm,$pNombreHeader,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3;
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $pNombreFrm="frmNuevo";
    $pNombreBtnGuardar="btnNivelEdificio";

    /*CREAR COMBO TIPO NIVEL*/
    $pNombreCombo='cbTipoNivel';
    $pCamposId='a.id_tipo_nivel';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_nivel a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbTipoNivel=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    /*CREAR COMBO TIPO ESTRUCTURA*/
    $pNombreCombo='cbTipoEstructura';
    $pCamposId='a.id_tipo_estructura';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_estructura a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbTipoEstructura=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_edificio, a.id_nivel, a.ancho, a.alto, a.fondo, b.detalle as tipo_nivel, c.detalle as tipo_estructura
            FROM ".$pNombreTabla." a
            inner join tbl_tipo_nivel b on a.id_tipo_nivel=b.id_tipo_nivel
            inner join tbl_tipo_estructura c on a.id_tipo_estructura=c.id_tipo_estructura
            where a.id_edificio=".$pId;

    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    $_form.='
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="'.$pNombreDataTable.'">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Id.</th>
                                    <th>Tipo Nivel</th>
                                    <th>Tipo Estructura</th>
                                    <th>Ancho</th>
                                    <th>Alto</th>
                                    <th>Fondo</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>';

   foreach ($_vlv_Resultado as $_data)
    {
        $_form.='           <tr class="odd gradeA">
                                <td class="center">'.$_data["estado"].'</td>
                                <td class="center">'.$_data["id_nivel"].'</td>
                                <td class="center">'.$_data["tipo_nivel"].'</td>
                                <td class="center">'.$_data["tipo_estructura"].'</td>
                                <td class="center">'.$_data["ancho"].'</td>
                                <td class="center">'.$_data["alto"].'</td>
                                <td class="center">'.$_data["fondo"].'</td>
                            ';
        if($_data["estado"]=='A')
            {
                $_form.='       <td align="center">&nbsp;</td>';
                $_form.='       <td align="center">&nbsp;</td>';
                $_form.='       <td align="center">&nbsp;</td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Registro" onClick="jsAnularActivarEdificio('.$_data["id_nivel"].',\''.$_pNombreArchivo.'\',\''.$_data["id_edificio"].'\')"><i class="fa fa-check"></i></button></td>';

            }
            else
            {
               // $_form.='       <td align="center"><button type="button" class="btn btn-outline btn-warning" data-toggle="modal" data-target="#'.$pNombreModal.'" onClick="jsVerPantallaModificar('.$_data["id_edificio"].',\''.$_pNombreArchivo.'\',\''.$pNombreModal.'\')">Asignar &Aacute;rea/Elementos</button></td>';
                $_form.="       <td align='center'><button type='button' class='btn btn-outline btn-warning' onClick=\"jsAbrirAreaNivel('area_nivel.php','NUEVO',".$_data["id_edificio"].",".$_data["id_nivel"].")\">Evaluar &Aacute;rea</button></td>";
                $_form.="       <td align='center'><button type='button' class='btn btn-outline btn-primary' onClick=\"jsAbrirAreaNivel('evalua_elemento.php','NUEVO',".$_data["id_edificio"].",".$_data["id_nivel"].")\">Evaluar Elemento</button></td>";
                $_form.='       <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#'.$pNombreModal.'" onClick="jsVerPantallaModificar2('.$_data["id_nivel"].',\''.$_pNombreArchivo.'\',\''.$pNombreModal.'\',\''.$_data["id_edificio"].'\')">Modificar</button></td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Registro" onClick="jsAnularActivarEdificio('.$_data["id_nivel"].',\''.$_pNombreArchivo.'\',\''.$_data["id_edificio"].'\')"><i class="fa fa-times"></i></button></td>';
            }
        $_form.='           </tr>';
    }
    $_form.='               </tbody>
                        </table>
                    </div> <!-- /.panel-body -->
                </div>  <!-- /.panel -->
            </div> <!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <script>
        $(document).ready(function() {
            $("#'.$pNombreDataTable.'").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                },
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 0, 1 ]
                } ]
            });
        });
        </script>
        ';
    $_form.='<div class="modal fade" id="'.$pNombreModal.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             </div>';
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}
function fConsultarNiveles($pId)
{
    global $_pNombreArchivo, $pNombreTabla,$pNombreDataTable,$pNombreModal, $pNombreFrm,$pNombreHeader,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3;
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $pNombreFrm="frmNuevo";
    $pNombreBtnGuardar="btnNivelEdificio";


    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_edificio, a.id_nivel, a.ancho, a.alto, a.fondo, b.detalle as tipo_nivel, c.detalle as tipo_estructura
            FROM ".$pNombreTabla." a
            inner join tbl_tipo_nivel b on a.id_tipo_nivel=b.id_tipo_nivel
            inner join tbl_tipo_estructura c on a.id_tipo_estructura=c.id_tipo_estructura
            where a.id_edificio=".$pId;

    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    $_form='
        <div class="row">
            <div class="col-lg-12">
                <div id="divMensaje">&nbsp;</div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                            '.$pNombreHeader.' No '.$pId.'
                        </div>
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="'.$pNombreDataTable.'">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Id.</th>
                                    <th>Tipo Nivel</th>
                                    <th>Tipo Estructura</th>
                                    <th>Ancho</th>
                                    <th>Altura</th>
                                    <th>Largo</th>

                                </tr>
                            </thead>
                            <tbody>';

   foreach ($_vlv_Resultado as $_data)
    {
        $_form.='           <tr class="odd gradeA">
                                <td class="center">'.$_data["estado"].'</td>
                                <td class="center">'.$_data["id_nivel"].'</td>
                                <td class="center">'.$_data["tipo_nivel"].'</td>
                                <td class="center">'.$_data["tipo_estructura"].'</td>
                                <td class="center">'.$_data["ancho"].'</td>
                                <td class="center">'.$_data["alto"].'</td>
                                <td class="center">'.$_data["fondo"].'</td>
                            ';

        $_form.='           </tr>';
    }
    $_form.='               </tbody>
                        </table>
                    </div> <!-- /.panel-body -->
                </div>  <!-- /.panel -->
            </div> <!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <script>
        $(document).ready(function() {
            $("#'.$pNombreDataTable.'").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                },
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 0, 1 ]
                } ]
            });
        });
        </script>
        ';
    $_form.='<div class="modal fade" id="'.$pNombreModal.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             </div>';
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}
function fGeneraLista()
{
    global $_pNombreArchivo;
    $_operacionesbd=new operacionesbd();
    $jsondata = array();
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_edificio, a.direccion, a.predio, a.propietario, a.codigo_obra
            FROM tbl_edificio a";
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    $_form='
    <div class="row">
        <div class="col-lg-12">
            <div id="divMensaje">&nbsp;</div>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Lista de Edificios
                </div>
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="ver_rEdificio">
                        <thead>
                            <tr>
                                <th>Estado</th>
                                <th>Id. Registro</th>
                                <th>Direcci&oacute;n</th>
                                <th>Predio</th>
                                <th>Propietario</th>
                                <th>C&oacute;digo Obra</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>';

   foreach ($_vlv_Resultado as $_data)
    {
        $_form.='           <tr class="odd gradeA">
                                <td class="center">'.$_data["estado"].'</td>
                                <td class="center">'.$_data["id_edificio"].'</td>
                                <td class="center">'.$_data["direccion"].'</td>
                                <td class="center">'.$_data["predio"].'</td>
                                <td class="center">'.$_data["propietario"].'</td>
                                <td class="center">'.$_data["codigo_obra"].'</td>
                            ';
        $_form.="       <td align='center'><button type='button' class='btn btn-outline btn-success'
        onClick=\"jsAbrirNivel('".$_pNombreArchivo."','NUEVO',".$_data["id_edificio"].")\">Evaluar Nivel</button></td>";
        $_form.="       <td align='center'><button type='button' class='btn btn-outline btn-warning'
        onClick=\"jsAbrirNivel('".$_pNombreArchivo."','CONSULTAR',".$_data["id_edificio"].")\">Consultar Nivel</button></td>";


        $_form.='           </tr>';
    }
    $_form.='               </tbody>
                        </table>
                    </div> <!-- /.panel-body -->
                </div>  <!-- /.panel -->
            </div> <!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <script>
        $(document).ready(function() {
            $("#ver_rEdificio").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                },
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 0, 1 ]
                } ]
            });
        });
        </script>
        ';

        $array = array(0 => $_form);

        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fVerPantallaModificar($_pIdOpcion, $pIdEdificio)
{
    global $_pNombreArchivo, $pNombreTabla,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3, $pNombreBtnGuardar, $pNombreModal;
    $pNombreFrm="frmActualizar";
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $jsondata = array();
    $vlnNum=0;

    $p_SQL="SELECT a.estado, a.id_edificio, a.id_tipo_nivel, a.id_nivel, a.ancho, a.alto, a.fondo, b.detalle as tipo_nivel,
            c.detalle as tipo_estructura, a.id_tipo_estructura
            FROM ".$pNombreTabla." a
            inner join tbl_tipo_nivel b on a.id_tipo_nivel=b.id_tipo_nivel
            inner join tbl_tipo_estructura c on a.id_tipo_estructura=c.id_tipo_estructura
            where a.id_edificio=".$pIdEdificio.' and a.id_nivel='.$_pIdOpcion;
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    /*CREAR COMBO TIPO ESTRUCTURA*/
    $pNombreCombo1='cbTipoEstructura';
    $pCamposId1='a.id_tipo_estructura';
    $pCamposDetalle1='a.detalle';
    $pTabla1='tbl_tipo_estructura a';
    $pInner1='';
    $pWhere1=' where a.estado is null';
    $pOrder1='';
    $pGroupBy1='';
    $pRequerido1='required';

    /*CREAR COMBO TIPO NIVEL*/
    $pNombreCombo='cbTipoNivel';
    $pCamposId='a.id_tipo_nivel';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_nivel a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';


    foreach ($_vlv_Resultado as $_data)
    {
        $pSelected=$_data["id_tipo_nivel"];
        $cbTipoNivel=$_appcomponentes->f_crear_combo_bloqueado($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

        $pSelected1=$_data["id_tipo_estructura"];
        $cbTipoEstructura=$_appcomponentes->f_crear_combo($pNombreCombo1,$pCamposId1,$pCamposDetalle1,$pTabla1,$pInner1,$pWhere1,$pOrder1,$pGroupBy1,$pSelected1,$pRequerido1);
        $_form='
        <!-- Modal -->
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarForm2(\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\');">&times;</button>
                        <div id="divMensajeModal"></div>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Modificar Registro - ID Edificio: '.$pIdEdificio.' / ID Nivel: '.$_pIdOpcion.'
                                </div>
                                <div class="panel-body">
                                <form role="form" id="'.$pNombreFrm.'" >
                                    <input type="hidden" id="hddId" name="hddId" value="'.$_pIdOpcion.'">
                                    <input type="hidden" id="hddIdCodigoEdificio" name="hddIdCodigoEdificio" value="'.$pIdEdificio.'">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Tipo de nivel</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                '.$cbTipoNivel.'
                                                <p class="help-block"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Ancho (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" id="txtAncho" name="txtAncho"
                                                placeholder="'.$pPlaceHolder1.'" value="'.$_data["ancho"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Altura entre niveles (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" id="txtAlto" name="txtAlto"
                                                placeholder="'.$pPlaceHolder1.'" value="'.$_data["alto"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Largo (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" id="txtFondo" name="txtFondo"
                                                placeholder="'.$pPlaceHolder1.'" value="'.$_data["fondo"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Tipo de estructura</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                '.$cbTipoEstructura.'
                                                <p class="help-block"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </form>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div><!-- /.modal-body -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="'.$pNombreBtnGuardar.'" onClick="jsActualizar2(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onClick="jsRefrescarForm2(\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\');">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        <!-- /.modal -->';
    }

    $array = array(0 => $_form);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}

function fAnularActivar($_pIdOpcion)
{
    global $_pUsuario, $_pIp, $p_funcionAnulaActiva;
    $_operacionesbd=new operacionesbd();

    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdOpcion;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcionAnulaActiva,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>