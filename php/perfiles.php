<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$_pAccion               =$_POST["pAccion"];
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        fCargarFormulario();
    break;
    case 'GUARDAR':
        $_pNombrePerfil =$_POST['txtNombrePerfil'];
        fGuardarPerfil($_pNombrePerfil);
    break;
    case 'ACTUALIZAR':
        $_pNombrePerfil =$_POST['txtNombrePerfil'];
        $_pIdPerfil     =$_POST['hddIdPerfilUsuario'];
        fActualizarPerfil($_pNombrePerfil,$_pIdPerfil);
    break;
    case 'LISTA':
        $_pTipoLista    =$_POST['pTipoLista'];
        fGeneraLista($_pTipoLista);
    break;
    case 'CONSULTA':
        $_pIdPerfil     =$_POST['pIdPerfil'];
        fConsultarPerfil($_pIdPerfil);
    break;
    case 'MODIFICA':
        $_pIdPerfil     =$_POST['pIdPerfil'];
        fModificarPerfil($_pIdPerfil);
    break;
    case 'ANULA_ACTIVA':
        $_pIdPerfil           =$_POST['pIdCodigo'];
        fAnularActivarPerfiles($_pIdPerfil);
    break;
}

function fGuardarPerfil($_pNombrePerfil)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    $_pIdPerfil="null";
    $p_funcion="SP_GUARDAR_PERFIL";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."','".strtoupper($_pNombrePerfil)."',".$_pIdPerfil;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fActualizarPerfil($_pNombrePerfil,$_pIdPerfil)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    //$_pIdPerfil="null";
    $p_funcion="SP_GUARDAR_PERFIL";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."','".strtoupper($_pNombrePerfil)."',".$_pIdPerfil;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Nuevo Perfil
                        </div>
                        <div class="panel-body">
                         <form role="form" data-toggle="validator" id="frmPerfil" >
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Nombre de Perfil</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtNombrePerfil" name="txtNombrePerfil">

                                        <p class="help-block with-errors">Ingrese el nombre del perfil a crear.</p>
                                    </div>

                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="btnGuardarPerfil" onClick="jsGuardarPerfil();">Guardar</button>
                           <button type="button" class="btn btn-primary">Limpiar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    //$_form="ENTRO";
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}
function fGeneraLista($pTipo) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_perfil_usuario, a.nombre_perfil
                FROM tbl_perfil_usuario a";
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        $_form='<div class="row">
                <div class="col-lg-8">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">';
        if($pTipo=='C')//CONSULTAR
            $_form.='       Consulta Pefiles';
        else
            $_form.='       Modificar Pefiles';

        $_form.='       </div>
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="ver_perfiles">
                                <thead>
                                    <tr>
                                        <th>Estado</th>
                                        <th>Id. Perfil</th>
                                        <th>Nombre Perfil</th>
                                        <th>&nbsp;</th>';
        if($pTipo=='M')//CONSULTAR
            $_form.='                   <th>&nbsp;</th>';

        $_form.='                   </tr>
                                </thead>
                                <tbody>';
        foreach ($_vlv_Resultado as $_data)
        {
        $_form.='                   <tr class="odd gradeA">
                                        <td class="center">'.$_data["estado"].'</td>
                                        <td class="center">'.$_data["id_perfil_usuario"].'</td>
                                        <td class="center">'.$_data["nombre_perfil"].'</td>
                                        ';
        if($pTipo=='C')//CONSULTAR
            {
                if($_data["estado"]=='A')
                    $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-danger" data-toggle="modal" data-target="#ModConsultaPerfil" onClick="jsConsultarPerfilUsuario('.$_data["id_perfil_usuario"].')">Ver</button></td>';
                else
                    $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModConsultaPerfil" onClick="jsConsultarPerfilUsuario('.$_data["id_perfil_usuario"].')">Ver</button></td>';
            }
            else//MODIFICAR
            {
                if($_data["estado"]=='A')
                {
                    $_form.='           <td align="center">&nbsp;</td>';
                    $_form.='           <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Perfil de Usuario" onClick="jsAnularActivar('.$_data["id_perfil_usuario"].',\'perfiles.php\')"><i class="fa fa-check"></i></button></td>';

                }
                else
                {
                    $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModModificarPerfil" onClick="jsModificarPerfilUsuario('.$_data["id_perfil_usuario"].')">Modificar</button></td>';
                    $_form.='           <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Perfil de Usuario" onClick="jsAnularActivar('.$_data["id_perfil_usuario"].',\'perfiles.php\')"><i class="fa fa-times"></i></button></td>';
                }
            }
        $_form.='
                                    </tr>';
        }
        $_form.='
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->

                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <script>
            $(document).ready(function() {
                $("#ver_perfiles").DataTable({
                    responsive: true,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                        "zeroRecords": "No se encontraron registros",
                        "info": "P&aacutegina _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros",
                        "sSearch":         "Buscar:",
                        "oPaginate": {
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    }
                });
            });
            </script>
            ';
        if($pTipo=='C')//CONSULTAR
        {
            $_form.='<div class="modal fade" id="ModConsultaPerfil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';
        }
        else
        {
            $_form.='<div class="modal fade" id="ModModificarPerfil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';
        }
        $array = array(0 => $_form);

        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fConsultarPerfil($_pIdPerfil) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_perfil_usuario, a.nombre_perfil
                FROM tbl_perfil_usuario a
                where a.id_perfil_usuario=".$_pIdPerfil;
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        foreach ($_vlv_Resultado as $_data)
        {
            $_form='<!-- Modal -->
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-lg-12">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        Consulta Perfil Usuario - ID: '.$_data["id_perfil_usuario"].'
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label>Nombre de Perfil</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                                    <input class="form-control" id="txtNombrePerfil" name="txtNombrePerfil" value="'.$_data["nombre_perfil"].'" readonly>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- /.row (nested) -->
                                                    </div>
                                                </div>
                                                <!-- /.panel -->
                                            </div>
                                        </div><!-- /.modal-body -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->

                            <!-- /.modal -->';
        }

        $array = array(0 => $_form);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fModificarPerfil($_pIdPerfil) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_perfil_usuario, a.nombre_perfil
                FROM tbl_perfil_usuario a
                where a.id_perfil_usuario=".$_pIdPerfil;
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        foreach ($_vlv_Resultado as $_data)
        {
            $_form='<!-- Modal -->
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarLista(\'perfiles.php\');">&times;</button>
                                            <div id="divMensajeModal"></div>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-lg-12">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        Consulta Perfil Usuario - ID: '.$_data["id_perfil_usuario"].'
                                                    </div>
                                                    <div class="panel-body">
                                                     <form role="form" id="frmPerfil" >
                                                     <input type="hidden" id="hddIdPerfilUsuario" name="hddIdPerfilUsuario" value="'.$_data["id_perfil_usuario"].'">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label>Nombre de Perfil</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                                    <input class="form-control required" id="txtNombrePerfil" name="txtNombrePerfil" value="'.$_data["nombre_perfil"].'">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- /.row (nested) -->
                                                        </form>
                                                    </div>
                                                </div>
                                                <!-- /.panel -->
                                            </div>
                                        </div><!-- /.modal-body -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" id="btnGuardarPerfil" onClick="jsActualizarPerfil();">Guardar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarLista(\'perfiles.php\');">Cerrar</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->

                            <!-- /.modal -->';
        }

        $array = array(0 => $_form);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fAnularActivarPerfiles($_pIdPerfil)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    $p_funcion="SP_ANULAR_ACTIVAR_PERFILES";
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdPerfil;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>