<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$_pIdPerfilUsuario=$_SESSION["vgnIdPerfilUsuario"];
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

/*INICIO CARGAR CLASES*/
$_operacionesbd=new operacionesbd();
$_appcomponentes=new appcomponentes();
/*FIN CARGAR CLASES*/

/*INICIO DATOS DEL SISTEMA*/
$vlvShortSystemName="SISAEE";
$vlvNumVersion="1.0.1";
$vlvSystemVersion="v ".$vlvNumVersion;
$vlvLargeSystemName="SISTEMA DE ANALISIS DE ESTRUCTURAS DE EDIFICIOS";

$smarty->assign("vlvShortSystemName",$vlvShortSystemName);
$smarty->assign("vlvSystemVersion",$vlvSystemVersion);
$smarty->assign("vlvLargeSystemName",$vlvLargeSystemName);
/*FIN DATOS DEL SISTEMA*/
/*INICIO NAVIGATION BAR*/
$vlvManuBar=$_appcomponentes->f_cargar_menu($_pIdPerfilUsuario);
$smarty->assign("vlvManuBar",$vlvManuBar);
$smarty->assign("vlvNombreUsuario",$_pUsuario);
/*FIN NAVIGATION BAR*/
/*INICIO DE ALERTAS*/
//$vlnAlertas=$_appcomponentes->f_buscar_alertas($pIdUnidadOrganizacional);
$vlnAlertas=0;
if($vlnAlertas==0)
    $smarty->assign("vlnAlerta","");
else
    $smarty->assign("vlnAlerta",$vlnAlertas);
/*FIN DE ALERTAS*/
/*INICIO ESTADISTICAS*/
/*$p_SQL="select b.descripcion, count(b.descripcion) as numero
        from tbl_gestion_documentos a
        inner join tbl_tipo_documento b on a.prefijo_doc=b.prefijo_doc
        where a.id_unidad_organizacional=".$pIdUnidadOrganizacional." and a.enviar_a is null
        group by b.descripcion";
$_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
$vScript="<script>
$(function() {
    Morris.Donut({
        element: 'doc_por_tipo',
        data: [";
foreach ($_vlv_Resultado as $_data)
{
    $_data["descripcion"];
    $_data["numero"];
    $vScript.='{
            label: "'.$_data["descripcion"].'",
            value: '.$_data["numero"].'
        }, ';

}
$vScript=substr($vScript,0,strlen($vScript)-1);
$vScript.="],
        resize: true
        });

});
        </script> ";
$smarty->assign("vScript",$vScript);

$p_SQL1="select 'Ingresados' as descripcion, count(a.id_unidad_organizacional) as numero
        from tbl_gestion_documentos a
        where a.id_unidad_organizacional=".$pIdUnidadOrganizacional." and a.enviar_a is null
        union
        select 'Enviados' as descripcion, count(a.id_unidad_organizacional) as numero
        from tbl_gestion_documentos a
        where a.enviado_por=".$pIdUnidadOrganizacional."
        UNION
        select 'Recibidos' as descripcion, count(a.id_unidad_organizacional) as numero
        from tbl_gestion_documentos a
        where a.enviar_a=".$pIdUnidadOrganizacional."";
$_vlv_Resultado1=$_operacionesbd->f_EjecutaQuery($p_SQL1);
$vScript1="<script>
$(function() {
    Morris.Donut({
        element: 'documentos',
        data: [";
foreach ($_vlv_Resultado1 as $_data1)
{
    $vScript1.='{
            label: "'.$_data1["descripcion"].'",
            value: '.$_data1["numero"].'
        }, ';           
}
$vScript1=substr($vScript1,0,strlen($vScript1)-1);
$vScript1.="],
        resize: true
        });

});
        </script> ";

$smarty->assign("vScript1",$vScript1);*/
/*INICIO ESTADISTICAS*/

$smarty->display(TEMPLATES . '/index.html');

?>
