<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
date_default_timezone_set('America/Guayaquil');

// main paths
define('APP', 				  realpath('../php'));
define('CLASES', 		APP . '/clases');
define('INCLUDES', 		APP . '/include');
define('COMMON', 		APP . '/common');
define('LIB', 			'../vendor');
define('TEMPLATES', 	'../pages');
define('TEMPLATES_C', 	APP . '/templates_c');

  // get route path into constant
$URI = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';

if($URI === '/')
	$URI = '/home';

// parse URI into the triple
list(, $CATEGORY, $ACTION, $ID) = array_pad(explode('/', $URI), 4, null);

 if(is_null($ACTION))
	$ACTION = 'index';

session_start();

require_once(LIB . '/smarty/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir(TEMPLATES);
$smarty->setCompileDir(TEMPLATES_C);

include_once(CLASES . '/dbfunctions.php');
include_once(CLASES . '/operacionesbd.php');
include_once(CLASES . '/appcomponentes.php');
include_once(CLASES . '/passoperations.php');
include_once(CLASES . '/operaciones_usuario.php');
require_once (LIB . '/PHPExcel/Classes/PHPExcel.php');


function now()
{
	return strftime("%Y-%m-%d %H:%M:%S");
}
function Recuperaip()
{
 if (getenv("HTTP_X_FORWARDED_FOR")) {
     $ip   = getenv("HTTP_X_FORWARDED_FOR");
   } else {
     $ip   = getenv("REMOTE_ADDR");
		}
 return $ip;
}
?>