<?php
/**
 * Configuracion base de datos
 */
include("xml_functions.php");
$v_host= f_read_xml_settings('', 'host');
$v_dbname= f_read_xml_settings('', 'dbname');
$v_user= f_read_xml_settings('', 'user');
$v_passwd= f_read_xml_settings('', 'password');
$host=$v_host; $user=$v_user; $password=$v_passwd; $options=""; $dbname=$v_dbname;
$connstr=	"host='".pg_escape_string($host).
            "' user='".pg_escape_string($user).
            "' password='".pg_escape_string($password).
            "' dbname='".pg_escape_string($dbname).
            "' ".$options;

?>