<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/
$_operacionesbd=new operacionesbd();
$p_SQL="SELECT a.id_parametros_seguridad, a.complejidad_password, a.longitud_max_password
        FROM tbl_parametros_seguridad a
        where a.estado IS NULL";
$_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

foreach ($_vlv_Resultado as $_data)
{
    $_pComplejidadPasswd=$_data["complejidad_password"];
    $_pLongitudMaxPasswd=$_data["longitud_max_password"];
}
$vlvRecomendacion="";
if($_pComplejidadPasswd=='S')
{
    $vlvRecomendacion="
                <ul>
                    <li>La clave debe ser mayor a ".$_pLongitudMaxPasswd." caracteres</li>
                    <li>La clave puede contener hasta 25 caracteres</li>
                    <li>Mezclar entre may&uacute;sculas y min&uacute;sculas</li>
                    <li>Use m&aacute;s de un n&uacute;mero</li>
                    <li>Use caracteres especiales (!,@,#,$,%,^,&,*,?,_,~)</li>
                </ul>";

}


$smarty->assign("vlvRecomendacion",$vlvRecomendacion);
$smarty->display(TEMPLATES . '/cambiar_password.html');

?>