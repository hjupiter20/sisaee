<?php
/* Smarty version 3.1.30, created on 2018-08-01 20:59:19
  from "C:\wamp64\www\sisaee\pages\index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b6265774e9102_19149147',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '67daa22812e4739b365f331b6e715e572a3dd6aa' => 
    array (
      0 => 'C:\\wamp64\\www\\sisaee\\pages\\index.html',
      1 => 1533175157,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b6265774e9102_19149147 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $_smarty_tpl->tpl_vars['vlvShortSystemName']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['vlvSystemVersion']->value;?>
 </title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">
    
    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
            	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            	<h2 style="color: #fff">
                	<?php echo $_smarty_tpl->tpl_vars['vlvShortSystemName']->value;?>
&nbsp;<small style="color: #fff"> <?php echo $_smarty_tpl->tpl_vars['vlvSystemVersion']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['vlvLargeSystemName']->value;?>
</small>
                </h2>  
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">                                  
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i> <label id="lblAlerta"><?php echo $_smarty_tpl->tpl_vars['vlnAlerta']->value;?>
</label> <i class="fa fa-caret-down"></i> 
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">                        
                        <li>
                            <a href="#" id="alertas_gestion_documentos">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Documentos Recibidos
                                    <span class="pull-right text-muted small"><label><div id="lblAlerta1"><?php echo $_smarty_tpl->tpl_vars['vlnAlerta']->value;?>
</span></label></div>
                                </div>
                            </a>
                        </li>
                        <!--<li class="divider"></li>-->
                        
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i> <?php echo $_smarty_tpl->tpl_vars['vlvNombreUsuario']->value;?>

                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" onClick="jsDatosUsuario();"><i class="fa fa-user fa-fw"></i> Datos de Usuario</a>
                        <li><a href="#" onClick="jsCambiarPasswd();"><i class="fa fa-gear fa-fw"></i> Cambiar Contrase&ntilde;a</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#" onClick="jsLogout();"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                        </li>
                    </ul>                     
                    <!-- /.dropdown-user -->
                </li>
                <!--<img src="../images/logo_facultad.png" width="65" height="70" alt=""/>-->
                <!-- /.dropdown -->
            </ul>
            
            <!-- /.navbar-top-links -->

            <div class="navbar-inverse sidebar" role="navigation">
                <?php echo $_smarty_tpl->tpl_vars['vlvManuBar']->value;?>

                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <img src="../images/Dashboard.png" alt="" style="width: 100%; height: auto;">
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <?php echo '<script'; ?>
 src="../vendor/jquery/jquery.min.js"><?php echo '</script'; ?>
>

    <!-- Bootstrap Core JavaScript -->
    <?php echo '<script'; ?>
 src="../vendor/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>

    <!-- Metis Menu Plugin JavaScript -->
    <?php echo '<script'; ?>
 src="../vendor/metisMenu/metisMenu.min.js"><?php echo '</script'; ?>
>

    <!-- Morris Charts JavaScript -->
    <?php echo '<script'; ?>
 src="../vendor/raphael/raphael.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="../vendor/morrisjs/morris.min.js"><?php echo '</script'; ?>
>
    <!-- <?php echo $_smarty_tpl->tpl_vars['vScript']->value;?>

    <?php echo $_smarty_tpl->tpl_vars['vScript1']->value;?>
-->
    
    <!-- DataTables JavaScript -->
    <?php echo '<script'; ?>
 src="../vendor/datatables/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="../vendor/datatables-responsive/dataTables.responsive.js"><?php echo '</script'; ?>
>

    <!-- Custom Theme JavaScript -->
    <?php echo '<script'; ?>
 src="../dist/js/sb-admin-2.js"><?php echo '</script'; ?>
>
    
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <?php echo '<script'; ?>
 src="../js/jsFunctionGlobal.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="../js/jsFunctions.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="../js/jsFunctionMd5.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="../vendor/jquery.validate.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="../js/jsValidarFormulario.js"><?php echo '</script'; ?>
>
    
    

</body>

</html>
<?php }
}
