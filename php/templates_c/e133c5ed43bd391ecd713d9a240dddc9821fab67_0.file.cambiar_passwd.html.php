<?php
/* Smarty version 3.1.30, created on 2017-04-19 02:06:26
  from "C:\wamp64\www\sgdes\pages\cambiar_passwd.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58f70c72ab6225_37341965',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e133c5ed43bd391ecd713d9a240dddc9821fab67' => 
    array (
      0 => 'C:\\wamp64\\www\\sgdes\\pages\\cambiar_passwd.html',
      1 => 1492585411,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58f70c72ab6225_37341965 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cambiar Contrase&ntilde;a</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" id="frmCambiarPasswd">
                            <fieldset>
                                <div class="form-group" id="divPasswdAnterior">
                                   	<label class="control-label" for="txtPasswdAnterior">Contrase&ntilde;a Anterior</label> <i class="fa fa-asterisk" style="color:red"></i> 
                                    <input class="form-control" placeholder="Contrase&ntilde;a Anterior" name="txtPasswdAnterior" id="txtPasswdAnterior" type="password" value="" onBlur="jsValidaPasswdAnt();">
                                </div>
                                <div class="form-group" id="divNuevoPasswd">
                                   	<label class="control-label" for="txtNuevoPasswd">Nueva Contrase&ntilde;a</label> <i class="fa fa-asterisk" style="color:red"></i> 
                                    <input class="form-control" placeholder="Nueva Contrase&ntilde;a" name="txtNuevoPasswd" id="txtNuevoPasswd" type="password" value="" onKeyPress="testPassword(this.value);" onBlur="jsValidaPasswdNew();">
                                </div>
                                <div class="form-group" id="divRepetirNuevoPasswd">
                                   	<label class="control-label" for="txtRepetirNuevoPasswd">Repetir Nueva Contrase&ntilde;a</label> <i class="fa fa-asterisk" style="color:red"></i> 
                                    <input class="form-control" placeholder="Repetir Nueva Contrase&ntilde;a" name="txtRepetirNuevoPasswd" id="txtRepetirNuevoPasswd" type="password" value="" onBlur="js_confirma_password_blur(this.value);" 
                                        onKeyPress="js_confirma_password(this.value, event);">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <a href="#" class="btn btn-lg btn-success btn-block" onClick="jsValidarCamposPasswd();">Cambiar</a>
                                <p>
                                	<div id="divNivelPasswd" align="center">
										<span class="alert-link" id="verdict"></span>
									</div>
                                	<input type="hidden" id="hddVeredicto" name="hddVeredicto" value="">
                                </p>
                                <p>
                                	<?php echo $_smarty_tpl->tpl_vars['vlvRecomendacion']->value;?>

								</p>
                           		<p>
									<div id="divMensaje">asdasd</div>
                           		</p>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <div id="divMensaje"></div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <?php echo '<script'; ?>
 src="../vendor/jquery/jquery.min.js"><?php echo '</script'; ?>
>

    <!-- Bootstrap Core JavaScript -->
    <?php echo '<script'; ?>
 src="../vendor/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>

    <!-- Metis Menu Plugin JavaScript -->
    <?php echo '<script'; ?>
 src="../vendor/metisMenu/metisMenu.min.js"><?php echo '</script'; ?>
>

    <!-- Custom Theme JavaScript -->
    <?php echo '<script'; ?>
 src="../dist/js/sb-admin-2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="../js/jsFunctionMd5.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="../js/jsLogin.js"><?php echo '</script'; ?>
>
    

</body>

</html>
<?php }
}
