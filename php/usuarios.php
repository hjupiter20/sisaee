<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIdCodigoUsuario=$_SESSION["vgvIdCodigo"];
$_pIp=Recuperaip();
$_pAccion               =$_POST["pAccion"];
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        fCargarFormulario();
    break;
    case 'VALIDA_USUARIO':
        $_pNombreUsuario    =$_POST['txtNombreUsuario'];
        fValidaUsuario($_pNombreUsuario);
    break;
    case 'GUARDAR':
        $_pCedula           =$_POST['txtCedula'];
        $_pNombreUsuario    =$_POST['txtNombreUsuario'];
        $_pNombre1          =$_POST['txtNombre1'];
        $_pNombre2          =$_POST['txtNombre2'];
        $_pApellido1        =$_POST['txtApellido1'];
        $_pApellido2        =$_POST['txtApellido2'];
        $_pIdPerfilUsuario  =$_POST['cbPerfil'];
        $_pEmail            =$_POST['txtEmail'];
        fGuardarUsuario($_pCedula, $_pNombreUsuario, $_pNombre1, $_pNombre2, $_pApellido1, $_pApellido2, $_pIdPerfilUsuario, $_pEmail);
    break;
    case 'ACTUALIZAR':
        $_pCedula           =$_POST['txtCedula'];
        $_pNombreUsuario    =$_POST['txtNombreUsuario'];
        $_pNombre1          =$_POST['txtNombre1'];
        $_pNombre2          =$_POST['txtNombre2'];
        $_pApellido1        =$_POST['txtApellido1'];
        $_pApellido2        =$_POST['txtApellido2'];
        $_pIdPerfilUsuario  =$_POST['cbPerfil'];
        $_pEmail            =$_POST['txtEmail'];
        $_pIdCodigo         =$_POST['hddIdCodigo'];
        $_pBloqueoUsuario         =$_POST['cbUsuarioBloqueado'];
        fActualizarUsuario($_pCedula, $_pNombreUsuario, $_pNombre1, $_pNombre2, $_pApellido1, $_pApellido2, $_pIdPerfilUsuario, $_pEmail, $_pIdCodigo,$_pBloqueoUsuario);
    break;
    case 'LISTA':
        $_pTipoLista               =$_POST['pTipoLista'];
        fGeneraLista($_pTipoLista);
    break;
    case 'CONSULTA':
        $_pIdCodigo           =$_POST['pIdCodigo'];
        fConsultaUsuario($_pIdCodigo);
    break;
    case 'MODIFICA':
        $_pIdCodigo           =$_POST['pIdCodigo'];
        fModificaUsuario($_pIdCodigo);
    break;
    case 'ANULA_ACTIVA':
        $_pIdCodigo           =$_POST['pIdCodigo'];
        fAnularActivarUsuario($_pIdCodigo);
    break;
    case 'DATOS_USUARIO':
        fDatosUsuario();
    break;

}

function fGuardarUsuario($_pCedula, $_pNombreUsuario, $_pNombre1, $_pNombre2, $_pApellido1, $_pApellido2, $_pIdPerfilUsuario, $_pEmail)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    $_passoperations=new passoperations();
    $_pBloqueoUsuario='1';
    $vlvHash=$_passoperations->f_hash(md5($_pCedula));
    $_pIdCodigo="null";
    $p_funcion="SP_GUARDAR_USUARIO";
    $p_parametros="'".$_pUsuario."','".$_pIp."','".$_pCedula."','".$_pNombreUsuario."','".$_pNombre1."','".$_pNombre2."','".$_pApellido1."','".$_pApellido2."',".$_pIdPerfilUsuario.",'".$_pEmail."',".$_pIdCodigo.",'".$vlvHash."','".$_pBloqueoUsuario."'";

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fActualizarUsuario($_pCedula, $_pNombreUsuario, $_pNombre1, $_pNombre2, $_pApellido1, $_pApellido2, $_pIdPerfilUsuario, $_pEmail, $_pIdCodigo, $_pBloqueoUsuario)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    //$_pIdCodigo="null";
    $vlvHash="null";
    $p_funcion="SP_GUARDAR_USUARIO";
    $p_parametros="'".$_pUsuario."','".$_pIp."','".$_pCedula."','".$_pNombreUsuario."','".$_pNombre1."','".$_pNombre2."','".$_pApellido1."','".$_pApellido2."',".$_pIdPerfilUsuario.",'".$_pEmail."',".$_pIdCodigo.",'".$vlvHash."','".$_pBloqueoUsuario."'";

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fValidaUsuario($_pNombreUsuario)
{
    $_operacionesbd=new operacionesbd();
    $p_SQL="SELECT count(a.usuario) from tbl_usuarios a where a.usuario like '".$_pNombreUsuario."%'";
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    foreach ($_vlv_Resultado as $_data)
    {
        $_vlnNoUsuario=$_data["count"];
    }
    $_jSonArray = array(0 => $_vlnNoUsuario);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    $_appcomponentes=new appcomponentes();
    /*CREAR COMBO PERFILES*/
    $pNombreCombo='cbPerfil';
    $pCamposId='a.id_perfil_usuario';
    $pCamposDetalle='a.nombre_perfil';
    $pTabla='tbl_perfil_usuario a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbPerfil=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);    

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Nuevo Usuario
                        </div>
                        <div class="panel-body">
                         <form role="form" data-toggle="validator" id="frmNuevoUsuario" onClick="bloquearCopiar();">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group" id="divCedula">
                                        <label>C&eacute;dula</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtCedula" name="txtCedula" placeholder="Ingrese el n&uacute;mero de c&eacute;dula"
                                        onBlur="validarid(this.id);">
                                        <p class="help-block">Ingrese el n&uacute;mero de c&eacute;dula.</p>

                                    </div>
                                    <div class="form-group">
                                        <label>Primer Nombre</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtNombre1" name="txtNombre1" placeholder="Ingrese el Primer Nombre"
                                        onBlur="js_solo_mayusculas(this.value, this.id);js_crear_nombre_usuario(\'frmNuevoUsuario\');"
                                        onkeypress="return soloLetras(event)">
                                        <p class="help-block with-errors">Ingrese el Primer Nombre.</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Primer Apellido</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtApellido1" name="txtApellido1" placeholder="Ingrese el Primer Apellido"
                                        onBlur="js_solo_mayusculas(this.value, this.id) js_crear_nombre_usuario(\'frmNuevoUsuario\');"
                                        onkeypress="return soloLetras(event)">
                                        <p class="help-block">Ingrese el Primer Apellido.</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Perfil de Usuario</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        '.$cbPerfil.'
                                        <p class="help-block">Seleccione un perfil  de usuario.</p>
                                    </div>
                                    
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nombre de Usuario</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control" id="txtNombreUsuario" name="txtNombreUsuario" placeholder="" readOnly>
                                        <p class="help-block">Aqu&iacute; se genera el Nombre de Usuario.</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Segundo Nombre</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtNombre2" name="txtNombre2" placeholder="Ingrese el Segundo Nombre"
                                        onBlur="js_solo_mayusculas(this.value, this.id); onkeypress="return soloLetras(event)">
                                        <p class="help-block">Ingrese el Segundo Nombre.</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Segundo Apellido</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtApellido2" name="txtApellido2" placeholder="Ingrese el Segundo Apellido"
                                        onBlur="js_solo_mayusculas(this.value, this.id);js_crear_nombre_usuario(\'frmNuevoUsuario\');"
                                        onkeypress="return soloLetras(event)">
                                        <p class="help-block">Ingrese el Segundo Apellido.</p>
                                    </div>
                                   <label>Correo Electr&oacute;nico</label> <i class="fa fa-asterisk" style="color:red"></i>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon">@</span>
                                        <input type="text" id="txtEmail" name="txtEmail" class="form-control required email" placeholder="Ingrese el correo electr&oacute;nico"  >
                                    </div>

                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="btnGuardarUsuario" onClick="jsGuardarUsuario();">Guardar</button>
                           <button type="button" class="btn btn-primary">Limpiar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    //$_form="ENTRO";
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}
function fGeneraLista($pTipo) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_codigo, a.usuario, a.pghash, a.cedula, a.nombre1, a.nombre2, a.apellido1, a.apellido2, a.fecha_cambio_password,
                a.num_intentos_bloqueo_cuenta, a.id_perfil_usuario, a.email, a.usuario_nuevo, b.nombre_perfil,
                case when a.bloqueado='1' then 'Activo' else 'Bloqueado' end as bloqueado
                FROM tbl_usuarios a
                inner join tbl_perfil_usuario b on a.id_perfil_usuario=b.id_perfil_usuario";
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">';
        if($pTipo=='C')//CONSULTAR
            $_form.='                   Consulta Usuarios';
        else
            $_form.='                   Modificar Usuarios';

        $_form.='                </div>
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="ver_usuario">
                                <thead>
                                    <tr>
                                        <th>Estado</th>
                                        <th>C&oacute;digo</th>
                                        <th>Nombre Usuario</th>
                                        <th>C&eacute;dula</th>
                                        <th>Fec. Cambio Pass</th>
                                        <th>Perfil</th>
                                        <th>Estado Usuario</th>
                                        <th>&nbsp;</th>';
        if($pTipo=='M')//CONSULTAR
            $_form.='                   <th>&nbsp;</th>';

            $_form.='                </tr>
                                </thead>
                                <tbody>';
        foreach ($_vlv_Resultado as $_data)
        {
        $_form.='               <tr class="odd gradeA">
                                        <td class="center">'.$_data["estado"].'</td>
                                        <td class="center">'.$_data["id_codigo"].'</td>
                                        <td class="center">'.$_data["usuario"].'</td>
                                        <td class="center">'.$_data["cedula"].'</td>';
            if($_data["fecha_cambio_password"])
                $_form.='               <td class="center">'.date('d/m/Y',strtotime($_data["fecha_cambio_password"])).'</td>';
            else
                $_form.='               <td class="center">&nbsp;</td>';

            $_form.='                   <td class="center">'.$_data["nombre_perfil"].'</td>
                                        <td class="center">'.$_data["bloqueado"].'</td>';
            if($pTipo=='C')//CONSULTAR
            {
                if($_data["estado"]=='A')
                    $_form.='               <td align="center"><button type="button" class="btn btn-outline btn-danger" data-toggle="modal" data-target="#ModConsultaUsuario" onClick="jsConsultarUsuario('.$_data["id_codigo"].')">Ver</button></td>';
                else
                    $_form.='               <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModConsultaUsuario" onClick="jsConsultarUsuario('.$_data["id_codigo"].')">Ver</button></td>';
            }
            else//MODIFICAR
            {
                if($_data["estado"]=='A')
                {
                    $_form.='               <td align="center">&nbsp;</td>';
                    $_form.='               <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Usuario" onClick="jsAnularActivar('.$_data["id_codigo"].',\'usuarios.php\')"><i class="fa fa-check"></i></button></td>';

                }
                else
                {
                    $_form.='               <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModModificarUsuario" onClick="jsModificarUsuario('.$_data["id_codigo"].')">Modificar</button></td>';
                    $_form.='               <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Usuario" onClick="jsAnularActivar('.$_data["id_codigo"].',\'usuarios.php\')"><i class="fa fa-times"></i></button></td>';
                }
            }

            $_form.='           </tr>';
        }
        $_form.='
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->

                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <script>
            $(document).ready(function() {
                $("#ver_usuario").DataTable({
                    responsive: true,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                        "zeroRecords": "No se encontraron registros",
                        "info": "P&aacutegina _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros",
                        "sSearch":         "Buscar:",
                        "oPaginate": {
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    }
                });
            });
            </script>
            ';
        if($pTipo=='C')//CONSULTAR
        {
            $_form.='<div class="modal fade" id="ModConsultaUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';
        }
        else
        {
            $_form.='<div class="modal fade" id="ModModificarUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';
        }

        $array = array(0 => $_form);

        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fConsultaUsuario($_pIdCodigo) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_codigo, a.usuario, a.pghash, a.cedula, a.nombre1, a.nombre2, a.apellido1, a.apellido2, a.fecha_cambio_password,
                a.num_intentos_bloqueo_cuenta, a.id_perfil_usuario, a.email, a.usuario_nuevo, b.nombre_perfil, a.fecha_prox_cambio_pass,
                case when a.bloqueado='1' then 'Activo' else 'Bloqueado' end as bloqueado
                FROM tbl_usuarios a
                inner join tbl_perfil_usuario b on a.id_perfil_usuario=b.id_perfil_usuario
                where a.id_codigo=".$_pIdCodigo;
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL); 

        foreach ($_vlv_Resultado as $_data)
        {
            if($_data["fecha_cambio_password"])
                $_vldFecCambioPass=date('d/m/Y',strtotime($_data["fecha_cambio_password"]));
            else
                $_vldFecCambioPass="";

            if($_data["fecha_prox_cambio_pass"])
                $_vldFecProxCambioPass=date('d/m/Y',strtotime($_data["fecha_prox_cambio_pass"]));
            else
                $_vldFecProxCambioPass="";
            $_form='<!-- Modal -->
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Consulta Usuario - C&oacute;digo Usuario No. '.$_data["id_codigo"].'
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>C&eacute;dula</label>
                                                    <input class="form-control" id="txtCedula" name="txtCedula" value="'.$_data["cedula"].'" readOnly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Primer Nombre</label>
                                                    <input class="form-control" id="txtNombre1" name="txtNombre1" value="'.$_data["nombre1"].'" readOnly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Primer Apellido</label>
                                                    <input class="form-control" id="txtApellido1" name="txtApellido1" value="'.$_data["apellido1"].'" readOnly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Perfil de Usuario</label>
                                                    <input class="form-control" id="txtPerfilUsuarioario" name="txtPerfilUsuario" value="'.$_data["nombre_perfil"].'" readOnly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Fecha Cambio Contrase&ntilde;a</label>
                                                    <input class="form-control" id="txtFecCambioPass" name="txtFecCambioPass" value="'.$_vldFecCambioPass.'" readOnly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Estado de Usuario</label>
                                                    <input class="form-control" id="cbBloqueoUsuario" name="cbBloqueoUsuario" value="'.$_data["bloqueado"].'" readOnly>
                                                </div>
                                                
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Nombre de Usuario</label>
                                                    <input class="form-control" id="txtNombreUsuario" name="txtNombreUsuario" value="'.$_data["usuario"].'" readOnly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Segundo Nombre</label>
                                                    <input class="form-control" id="txtNombre2" name="txtNombre2" value="'.$_data["nombre2"].'" readOnly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Segundo Apellido</label>
                                                    <input class="form-control" id="txtApellido2" name="txtApellido2" value="'.$_data["apellido2"].'" readOnly>
                                                </div>
                                                <label>Correo Electr&oacute;nico</label>
                                                <div class="form-group input-group">
                                                    <span class="input-group-addon">@</span>
                                                    <input type="text" id="txtEmail" name="txtEmail" class="form-control" value="'.$_data["email"].'" readOnly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Fecha Pr&oacute;ximo Cambio Contrase&ntilde;a</label>
                                                    <input class="form-control" id="txtFecProxCambioPass" name="txtFecProxCambioPass" value="'.$_vldFecProxCambioPass.'" readOnly>
                                                </div>
                                                

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.panel -->
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                    </div><!-- /.modal-body -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        <!-- /.modal -->';
        }

        $array = array(0 => $_form);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fModificaUsuario($_pIdCodigo) {
        $_operacionesbd=new operacionesbd();
        $_appcomponentes=new appcomponentes();
        $jsondata = array();
        $vlnNum=0;
        /*CREAR COMBO PERFILES*/
        $pNombreCombo='cbPerfil';
        $pCamposId='a.id_perfil_usuario';
        $pCamposDetalle='a.nombre_perfil';
        $pTabla='tbl_perfil_usuario a';
        $pInner='';
        $pWhere='';
        $pOrder='';
        $pGroupBy='';
        $pRequerido='required';

      
        $p_SQL="SELECT a.estado, a.id_codigo, a.usuario, a.pghash, a.cedula, a.nombre1, a.nombre2, a.apellido1, a.apellido2, a.fecha_cambio_password,
                a.num_intentos_bloqueo_cuenta, a.id_perfil_usuario, a.email, a.usuario_nuevo, b.nombre_perfil, a.bloqueado
                FROM tbl_usuarios a
                inner join tbl_perfil_usuario b on a.id_perfil_usuario=b.id_perfil_usuario
                where a.id_codigo=".$_pIdCodigo;
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        $pNombreCombo2="cbUsuarioBloqueado";
        $pRequerido2='required';
        foreach ($_vlv_Resultado as $_data)
        {
            $pSelected=$_data["id_perfil_usuario"];
            $cbPerfil=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

            $pSelected2=$_data["bloqueado"];
            $cbBloqueoUsuario=$_appcomponentes->f_ComboUsuarioBloqueado($pNombreCombo2, $pSelected2, $pRequerido2);
            $_form='<!-- Modal -->
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarLista(\'usuarios.php\');">&times;</button>
                        <div id="divMensajeModal"></div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Modificar Usuario - C&oacute;digo Usuario No. '.$_data["id_codigo"].'
                                    </div>
                                    <div class="panel-body">
                                     <form role="form" data-toggle="validator" id="frmModificaUsuario" >
                                      <input type="hidden" id="hddIdCodigo" name="hddIdCodigo" value="'.$_data["id_codigo"].'">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>C&eacute;dula</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                    <input class="form-control required" id="txtCedula" name="txtCedula" value="'.$_data["cedula"].'">
                                                    <p class="help-block">Ingrese el n&uacute;mero de c&eacute;dula.</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Primer Nombre</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                    <input class="form-control required" id="txtNombre1" name="txtNombre1" value="'.$_data["nombre1"].'"
                                                    onBlur="js_solo_mayusculas(this.value, this.id);js_crear_nombre_usuario(\'frmModificaUsuario\');"
                                                    onkeypress="return soloLetras(event)">
                                                    <p class="help-block">Ingrese el Primer Nombre.</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Primer Apellido</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                    <input class="form-control required" id="txtApellido1" name="txtApellido1" value="'.$_data["apellido1"].'"
                                                    onBlur="js_solo_mayusculas(this.value, this.id);js_crear_nombre_usuario(\'frmModificaUsuario\');"
                                                    onkeypress="return soloLetras(event)">
                                                    <p class="help-block">Ingrese el Primer Apellido.</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Perfil de Usuario</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                    '.$cbPerfil.'
                                                    <p class="help-block">Seleccione un perfil  de usuario.</p>
                                                </div>
                                               <p class="help-block">Ingrese el correo electr&oacute;nico.</p>
                                                <div class="form-group">
                                                    <label>Estado de Usuario</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                    '.$cbBloqueoUsuario.'
                                                    <p class="help-block">Seleccione un estado para el usuario.</p>
                                                </div>

                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Nombre de Usuario</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                    <input class="form-control" id="txtNombreUsuario" name="txtNombreUsuario" value="'.$_data["usuario"].'" readOnly>
                                                    <p class="help-block">Aqu&iacute; se genera el Nombre de Usuario.</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Segundo Nombre</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                    <input class="form-control required" id="txtNombre2" name="txtNombre2" value="'.$_data["nombre2"].'"  onPaste="javascript:this.value=\'\'"
                                                    onBlur="js_solo_mayusculas(this.value, this.id);" onkeypress="return soloLetras(event)" >
                                                    <p class="help-block">Ingrese el Segundo Nombre.</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Segundo Apellido</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                    <input class="form-control required" id="txtApellido2" name="txtApellido2" value="'.$_data["apellido2"].'"
                                                    onBlur="js_solo_mayusculas(this.value, this.id);js_crear_nombre_usuario(\'frmModificaUsuario\');"
                                                    onkeypress="return soloLetras(event)">
                                                    <p class="help-block">Ingrese el Segundo Apellido.</p>
                                                </div>
                                                <label>Correo Electr&oacute;nico</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <div class="form-group input-group">
                                                    <span class="input-group-addon">@</span>
                                                    <input type="text" id="txtEmail" name="txtEmail" class="form-control required email" value="'.$_data["email"].'">
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <!-- /.row (nested) -->
                                        </form>
                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                                <!-- /.panel -->
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                    </div><!-- /.modal-body -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onClick="jsActualizarUsuario();">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarLista(\'usuarios.php\');">Salir</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        <!-- /.modal -->';

        }

        $array = array(0 => $_form);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fAnularActivarUsuario($_pIdCodigo)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    $p_funcion="SP_ANULAR_ACTIVAR_USUARIO";
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdCodigo;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fDatosUsuario()
{
    global $_pIdCodigoUsuario;
    $_operacionesbd=new operacionesbd();
    $jsondata = array();
    $p_SQL="SELECT a.estado, a.id_codigo, a.usuario, a.pghash, a.cedula, a.nombre1, a.nombre2, a.apellido1, a.apellido2, a.fecha_cambio_password,
            a.num_intentos_bloqueo_cuenta, a.id_perfil_usuario, a.email, a.usuario_nuevo, b.nombre_perfil, a.fecha_prox_cambio_pass
            FROM tbl_usuarios a
            inner join tbl_perfil_usuario b on a.id_perfil_usuario=b.id_perfil_usuario
            where a.id_codigo=".$_pIdCodigoUsuario;
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    foreach ($_vlv_Resultado as $_data)
    {
        if($_data["fecha_cambio_password"])
            $_vldFecCambioPass=date('d/m/Y',strtotime($_data["fecha_cambio_password"]));
        else
            $_vldFecCambioPass="";

        if($_data["fecha_prox_cambio_pass"])
            $_vldFecProxCambioPass=date('d/m/Y',strtotime($_data["fecha_prox_cambio_pass"]));
        else
            $_vldFecProxCambioPass="";
        $_form='<div class="row">
                    <div class="col-lg-12">
                        <div id="divMensaje">&nbsp;</div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Datos del Usuario
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>C&eacute;dula</label>
                                            <input class="form-control" id="txtCedula" name="txtCedula" value="'.$_data["cedula"].'" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Primer Nombre</label>
                                            <input class="form-control" id="txtNombre1" name="txtNombre1" value="'.$_data["nombre1"].'" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Primer Apellido</label>
                                            <input class="form-control" id="txtApellido1" name="txtApellido1" value="'.$_data["apellido1"].'" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Perfil de Usuario</label>
                                            <input class="form-control" id="txtPerfilUsuario" name="txtPerfilUsuario" value="'.$_data["nombre_perfil"].'" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Fecha Cambio Contrase&ntilde;a</label>
                                            <input class="form-control" id="txtFecCambioPass" name="txtFecCambioPass" value="'.$_vldFecCambioPass.'" readOnly>
                                        </div>
                                        <label>Correo Electr&oacute;nico</label>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">@</span>
                                            <input type="text" id="txtEmail" name="txtEmail" class="form-control" value="'.$_data["email"].'" readonly>
                                        </div>

                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nombre de Usuario</label>
                                            <input class="form-control" id="txtNombreUsuario" name="txtNombreUsuario" value="'.$_data["usuario"].'" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Segundo Nombre</label>
                                            <input class="form-control" id="txtNombre2" name="txtNombre2" value="'.$_data["nombre2"].'" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Segundo Apellido</label>
                                            <input class="form-control" id="txtApellido2" name="txtApellido2" value="'.$_data["apellido2"].'" readonly>
                                        </div>                                       
                                        <div class="form-group">
                                            <label>Fecha Pr&oacute;ximo Cambio Contrase&ntilde;a</label>
                                            <input class="form-control" id="txtFecProxCambioPass" name="txtFecProxCambioPass" value="'.$_vldFecProxCambioPass.'" readOnly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                            <div class="panel-footer">

                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>';
    }

    $array = array(0 => $_form);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}

?>