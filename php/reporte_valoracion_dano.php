<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$_pAccion               =$_POST["pAccion"];
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/



/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'COMBO':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $pSelected           =$_POST['pSelected'];
        $pId           =$_POST['pId'];
        fComboNivel($pSelected, $pId);
    break;
    case 'NUEVO':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        fCargarFormulario();
    break;
    case 'VER_REPORTE':
        $_pIdEdificio    =$_POST['pIdEdificio'];
        $_pIdNivel    =$_POST['pIdNivel'];
        fVerReporte($_pIdEdificio, $_pIdNivel);
    break;
}
function fComboNivel($pSelected, $pId)
{
    $_appcomponentes=new appcomponentes();
    if($pId)
    {
        $pNombreCombo='cbNiveles';
        $pCamposId='a.id_nivel';
        $pCamposDetalle="b.detalle";
        $pTabla='tbl_nivel a';
        $pInner=' inner join tbl_tipo_nivel b on a.id_tipo_nivel=b.id_tipo_nivel';
        $pWhere=' where a.estado is null and a.id_edificio='.$pId;
        $pOrder=' order by b.detalle';
        $pGroupBy='';
        $pSelected=$pSelected;
        $pRequerido='required';
        $cbEdificio=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);
    }
    else
        $cbEdificio='<select class="form-control">
                        <option value="">Seleccione</option>
                    </select>';

    $vlvResultado=explode("|",$cbEdificio);
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    global $_pNombreArchivo;
    $_appcomponentes=new appcomponentes();
    $_operacionesbd=new operacionesbd();

    /*CREAR COMBO EDIFICIO*/
    $pNombreCombo='cbEdificio';
    $pCamposId='a.id_edificio';
    $pCamposDetalle='a.nombre_edificio';
    $pTabla='tbl_edificio a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder=' order by a.nombre_edificio';
    $pGroupBy='';
    $pSelected='NULL';
    $pRequerido='required';
    $pOnChange="jsComboNiveles('".$_pNombreArchivo."','NULL', this.value)";
    $cbEdificio=$_appcomponentes->f_crear_combo_change($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido,$pOnChange);

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Reporte de Edificio
                        </div>
                        <div class="panel-body">
                            <form role="form" data-toggle="validator" id="frmAccesoSistema" >
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Edificio</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbEdificio.'
                                            <p class="help-block">Seleccione edificio.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Niveles</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            <div id="divCbNiveles">
                                                <select class="form-control">
                                                <option value="">Seleccione</option>
                                                </select>
                                            </div>
                                            <p class="help-block">Seleccione nivel.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </form>
                            </div>
                                <!-- /.panel-body -->
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary" id="btnGuardarOpcionSistema" onClick="jsVerReporte(\''.$_pNombreArchivo.'\');">Ver Reporte</button>
                                    <button type="button" class="btn btn-success" onClick="jsExportarExcelEvaluacion();">Exportar</button>
                                </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div id="divReporte"></div>
                    </div>
                </div>
                </div>
            </div>
            <script>
            $(document).ready(function() {
                $("#ver_accesos").DataTable({
                    responsive: true,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                        "zeroRecords": "No se encontraron registros",
                        "info": "P&aacutegina _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros",
                        "sSearch":         "Buscar:",
                        "oPaginate": {
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    }
                });
            });
            </script>';
    //$_form="ENTRO";
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}
function fVerReporte($_pIdEdificio, $_pIdNivel) {
    $_operacionesbd=new operacionesbd();
    $jsondata = array();
    $vlnNum=0;
    if($_pIdEdificio<>'')
    {
        if($_pIdNivel<>'')
            $where="and a.id_edificio=".$_pIdEdificio." and a.id_nivel=".$_pIdNivel;
        else
            $where="and a.id_edificio=".$_pIdEdificio;

    }
    else
        $where="";

    $p_SQL="SELECT
            b.id_tipo_elemento , 
            b.detalle , 
            c.detalle_corto , 
            a.valoracion , 
            c.detalle as criterio
            FROM tbl_valoracion_dano a
            inner join tbl_tipo_elemento b on a.id_elemento = b.id_tipo_elemento
            inner join tbl_dano_detalle c on a.id_dano_detalle = c.id_dano_detalle
            WHERE a.estado is null ".$where;
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    $_form='<div class="row">
            <div class="col-lg-12">
                <div id="divMensaje">&nbsp;</div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">';

        $_form.='      Reporte Edificios';

    $_form.='       </div>
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="ver_reportes">
                            <thead>
                                <tr>
                                    <th>Codigo Elemento</th>
                                    <th>Nombre Elemento</th>
                                    <th>Gravedad de Dano</th>
                                    <th>Porcentaje de Dano</th>
                                    <th>Criterio de Dano</th>
                                    
                                </tr>';

    $_form.='
                            </thead>
                            <tbody>';
    foreach ($_vlv_Resultado as $_data)
    {
    $_form.='                   <tr class="odd gradeA">
                                    <td class="center">'.$_data["id_tipo_elemento"].'</td>
                                    <td class="center">'.$_data["detalle"].'</td>
                                    <td class="center">'.$_data["detalle_corto"].'</td>
                                    <td class="center">'.number_format($_data["valoracion"],2).'</td>';
    if(number_format($_data["valoracion"],2) >= 60){
        $_form.='<td class="center" style="background-color:#EC0D0D";><p style="color:white";>'.$_data["criterio"].'</p></td>';
    }
    if(number_format($_data["valoracion"],2) >= 30 and number_format($_data["valoracion"],2) < 60){
        $_form.='<td class="center" style="background-color:#A6A705";><p style="color:white";>'.$_data["criterio"].'</p></td>';
    }
    if(number_format($_data["valoracion"],2) < 30 ){
        $_form.='<td class="center" style="background-color:#048B0D";><p style="color:white";>'.$_data["criterio"].'</p></td>';
    }
    $_form.='
                                </tr>';
    }
    $_form.='
                            </tbody>
                        </table>
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <script>
        $(document).ready(function() {
            $("#ver_reportes").DataTable({
                responsive: false,
                "scrollX": true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
        });
        </script>
        ';

    $array = array(0 => $_form);

    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}


?>