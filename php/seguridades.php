<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$_pAccion               =$_POST["pAccion"];
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        fCargarFormulario();
    break;
    case 'GUARDAR':
        $_pHistorialPassword        =$_POST['txtHistorialPassword'];
        $_pEdadMaxPassword          =$_POST['txtEdadMaxPassword'];
        $_pComplejidadPassword      =$_POST['cbComplejidadPassword'];
        $_pTiempoBloqueoPassword    =$_POST['txtTiempoBloqueoPassword'];
        $_pLongitudMaxPassword      =$_POST['txtLongitudMaxPassword'];
        $_pEdadMinPassword          =$_POST['txtEdadMinPassword'];
        $_pNoIntentosPassword       =$_POST['txtNoIntentosPassword'];
        //$_vArrtext=$_pHistorialPassword." ".$_pEdadMaxPassword." ".$_pComplejidadPassword;
        fGuardarParametro($_pHistorialPassword, $_pEdadMaxPassword, $_pComplejidadPassword, $_pTiempoBloqueoPassword, $_pLongitudMaxPassword, $_pEdadMinPassword, $_pNoIntentosPassword);
    break;
    case 'ACTUALIZAR':
        $_pHistorialPassword        =$_POST['txtHistorialPassword'];
        $_pEdadMaxPassword          =$_POST['txtEdadMaxPassword'];
        $_pComplejidadPassword      =$_POST['cbComplejidadPassword'];
        $_pTiempoBloqueoPassword    =$_POST['txtTiempoBloqueoPassword'];
        $_pLongitudMaxPassword      =$_POST['txtLongitudMaxPassword'];
        $_pEdadMinPassword          =$_POST['txtEdadMinPassword'];
        $_pNoIntentosPassword       =$_POST['txtNoIntentosPassword'];
        $_pIdParametro              =$_POST['hddIdParametro'];
        //$_vArrtext=$_pHistorialPassword." ".$_pEdadMaxPassword." ".$_pComplejidadPassword;
        fActualizarParametro($_pHistorialPassword, $_pEdadMaxPassword, $_pComplejidadPassword, $_pTiempoBloqueoPassword, $_pLongitudMaxPassword, $_pEdadMinPassword, $_pNoIntentosPassword, $_pIdParametro);
    break;
    case 'LISTA':
        $_pTipoLista=$_POST['pTipoLista'];
        fGeneraLista($_pTipoLista);
    break;
    case 'CONSULTA':
        $_pIdParametroSeguridad=$_POST['pIdParametroSeguridad'];
        fConsultarSeguridad($_pIdParametroSeguridad);
    break;
    case 'MODIFICA':
        $_pIdParametroSeguridad=$_POST['pIdParametroSeguridad'];
        fModificarSeguridad($_pIdParametroSeguridad);
    break;

}

function fGuardarParametro($_pHistorialPassword, $_pEdadMaxPassword, $_pComplejidadPassword, $_pTiempoBloqueoPassword, $_pLongitudMaxPassword, $_pEdadMinPassword, $_pNoIntentosPassword)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    $_pIdParametro="null";
    $p_funcion="SP_GUARDAR_PARAMETRO";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pHistorialPassword.",".$_pEdadMaxPassword.",'".$_pComplejidadPassword."',".$_pTiempoBloqueoPassword.",".$_pLongitudMaxPassword.",".$_pEdadMinPassword.",".$_pNoIntentosPassword.",".$_pIdParametro;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fActualizarParametro($_pHistorialPassword, $_pEdadMaxPassword, $_pComplejidadPassword, $_pTiempoBloqueoPassword, $_pLongitudMaxPassword, $_pEdadMinPassword, $_pNoIntentosPassword,$_pIdParametro)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    //$_pIdParametro="null";
    $p_funcion="SP_GUARDAR_PARAMETRO";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pHistorialPassword.",".$_pEdadMaxPassword.",'".$_pComplejidadPassword."',".$_pTiempoBloqueoPassword.",".$_pLongitudMaxPassword.",".$_pEdadMinPassword.",".$_pNoIntentosPassword.",".$_pIdParametro;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Nuevo Par&aacute;metro del Sistema
                        </div>
                        <div class="panel-body">
                         <form role="form" data-toggle="validator" id="frmSeguridad" >
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Historial de Constrase&ntilde;a</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required digits" id="txtHistorialPassword" name="txtHistorialPassword" placeholder="Ingrese el n&uacute;mero de contrase&ntilde;as que desea recordar" required>

                                        <p class="help-block">Ingrese el n&uacute;mero de contrase&ntilde;as que desea recordar.</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Edad m&aacute;xima de Constrase&ntilde;a</label><i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required digits" id="txtEdadMaxPassword" name="txtEdadMaxPassword" placeholder="Ingrese el n&uacute;mero de d&iacute;as m&aacute;ximo para solicitar cambio de contrase&ntilde;as" required>
                                        <p class="help-block">Ingrese el n&uacute;mero de d&iacute;as m&aacute;ximo para solicitar cambio de contrase&ntilde;as.</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Complejidad de Constrase&ntilde;a</label><i class="fa fa-asterisk" style="color:red"></i>
                                        <select class="form-control required" id="cbComplejidadPassword" name="cbComplejidadPassword" required>
                                            <option value="">Seleccione</option>
                                            <option value="S">Si</option>
                                            <option value="N">No</option>
                                        </select>
                                        <p class="help-block">Indique si desea aplicar validaci&oacute;n de complejidad para la Constrase&ntilde;a.</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Tiempo de bloqueo de Constrase&ntilde;a</label><i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required digits" id="txtTiempoBloqueoPassword" name="txtTiempoBloqueoPassword" placeholder="Ingrese el n&uacute;mero de minutos que se bloqueara la contrase&ntilde;a cuando sea errada" required>
                                        <p class="help-block">Ingrese el n&uacute;mero de minutos que se bloqueara la contrase&ntilde;a cuando sea errada.</p>
                                    </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Longitud m&aacute;xima de Constrase&ntilde;a</label><i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required digits" id="txtLongitudMaxPassword" name="txtLongitudMaxPassword" placeholder="Ingrese el n&uacute;mero m&aacute;ximo de caracteres para la contrase&ntilde;a" required>
                                        <p class="help-block">Ingrese el n&uacute;mero m&aacute;ximo de caracteres para la contrase&ntilde;a.</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Edad m&iacute;nima de Constrase&ntilde;a</label><i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required digits" id="txtEdadMinPassword" name="txtEdadMinPassword" placeholder="Ingrese el n&uacute;mero de d&iacute;as m&iacute;nimo que debe mantener la misma contrase&ntilde;as" required>
                                        <p class="help-block">Ingrese el n&uacute;mero de d&iacute;as m&iacute;nimo que debe mantener la misma contrase&ntilde;as.</p>
                                    </div>
                                    <div class="form-group">
                                        <label>No. intentos para bloqueo de Constrase&ntilde;a</label><i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required digits" id="txtNoIntentosPassword" name="txtNoIntentosPassword" placeholder="Ingrese el n&uacute;mero de intentos para ingresar contrase&ntilde;as incorrectas" required>
                                        <p class="help-block">Ingrese el n&uacute;mero de intentos para ingresar contrase&ntilde;as incorrectas.</p>
                                    </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="btnGuardarParametro" onClick="jsGuardarSeguridades();">Guardar</button>
                           <button type="button" class="btn btn-primary">Limpiar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    //$_form="ENTRO";
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}

function fGeneraLista($pTipo) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_parametros_seguridad, case when a.complejidad_password='S' then 'SI' else 'NO' end as complejidad_password, a.edad_max_password, a.edad_min_password, a.historia_password,a.longitud_max_password,
                a.tiempo_duracion_password,a.num_intentos_bloqueo_cuenta
                FROM tbl_parametros_seguridad a";
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">';
        if($pTipo=='C')//CONSULTAR
            $_form.='       Consultar Par&aacute;metros del Sistema';
        else
            $_form.='       Modificar Par&aacute;metros del Sistema';

        $_form.='       </div>
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="ver_seguridades">
                                <thead>
                                    <tr>
                                        <th>Estado</th>
                                        <th>Id. Parametro</th>
                                        <th>Complejidad</th>
                                        <th>Edad Max. Pass</th>
                                        <th>Edad Min. Pass</th>
                                        <th>Historia Pass</th>
                                        <th>Long. Max. Pass</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>';
        foreach ($_vlv_Resultado as $_data)
        {
        $_form.='               <tr class="odd gradeA">
                                        <td class="center">'.$_data["estado"].'</td>
                                        <td class="center">'.$_data["id_parametros_seguridad"].'</td>
                                        <td class="center">'.$_data["complejidad_password"].'</td>
                                        <td class="center">'.$_data["edad_max_password"].'</td>
                                        <td class="center">'.$_data["edad_min_password"].'</td>
                                        <td class="center">'.$_data["historia_password"].'</td>
                                        <td class="center">'.$_data["longitud_max_password"].'</td>';
        if($pTipo=='C')//CONSULTAR
            {
                if($_data["estado"]=='A')
                    $_form.='               <td align="center"><button type="button" class="btn btn-outline btn-danger" data-toggle="modal" data-target="#ModConsultaSeguridad" onClick="jsConsultarParametroSeguridad('.$_data["id_parametros_seguridad"].')">Ver</button></td>';
                else
                    $_form.='               <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModConsultaSeguridad" onClick="jsConsultarParametroSeguridad('.$_data["id_parametros_seguridad"].')">Ver</button></td>';
            }
            else//MODIFICAR
            {
                if($_data["estado"]=='A')
                    $_form.='               <td align="center">&nbsp;</td>';
                else
                    $_form.='               <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModModificaSeguridad" onClick="jsModificarParametroSeguridad('.$_data["id_parametros_seguridad"].')">Modificar</button></td>';
            }
        $_form.='
                                </tr>';
        }
        $_form.='
                                </tbody>

                            </table>
                        </div>
                        <!-- /.panel-body -->

                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <script>
            $(document).ready(function() {
                $("#ver_seguridades").DataTable({
                    responsive: true,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                        "zeroRecords": "No se encontraron registros",
                        "info": "P&aacutegina _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros",
                        "sSearch":         "Buscar:",
                        "oPaginate": {
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    }
                });
            });
            </script>
            ';
        if($pTipo=='C')//CONSULTAR
        {
            $_form.='<div class="modal fade" id="ModConsultaSeguridad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';
        }
        else
        {
            $_form.='<div class="modal fade" id="ModModificaSeguridad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';
        }
        $array = array(0 => $_form);

        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fConsultarSeguridad($_pIdParametroSeguridad) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_parametros_seguridad, a.historia_password, a.edad_max_password, a.edad_min_password, a.longitud_max_password,
                a.tiempo_duracion_password, case when a.complejidad_password='S' then 'SI' else 'NO' end as complejidad_password, a.num_intentos_bloqueo_cuenta
                FROM tbl_parametros_seguridad a
                where a.id_parametros_seguridad=".$_pIdParametroSeguridad;
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        foreach ($_vlv_Resultado as $_data)
        {
            $_form='<!-- Modal -->
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">
                                                            Consultar Par&aacute;metros del Sistema - ID. '.$_data["id_parametros_seguridad"].'
                                                        </div>
                                                        <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>Historial de Constrase&ntilde;a</label>
                                                                            <input class="form-control" id="txtHistorialPassword" name="txtHistorialPassword" value="'.$_data["historia_password"].'" readonly>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Edad m&aacute;xima de Constrase&ntilde;a</label>
                                                                            <input class="form-control" id="txtEdadMaxPassword" name="txtEdadMaxPassword" value="'.$_data["edad_max_password"].'" readonly>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Complejidad de Constrase&ntilde;a</label>
                                                                            <input class="form-control" id="cbComplejidadPassword" name="cbComplejidadPassword" value="'.$_data["complejidad_password"].'" readonly>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Tiempo de bloqueo de Constrase&ntilde;a</label><i class="fa fa-asterisk" style="color:red"></i>
                                                                            <input class="form-control" id="txtTiempoBloqueoPassword" name="txtTiempoBloqueoPassword" value="'.$_data["tiempo_duracion_password"].'" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <!-- /.col-lg-6 (nested) -->
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>Longitud m&aacute;xima de Constrase&ntilde;a</label>
                                                                            <input class="form-control" id="txtLongitudMaxPassword" name="txtLongitudMaxPassword" value="'.$_data["longitud_max_password"].'" readonly>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Edad m&iacute;nima de Constrase&ntilde;a</label>
                                                                            <input class="form-control" id="txtEdadMinPassword" name="txtEdadMinPassword" value="'.$_data["edad_min_password"].'" readonly>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>No. intentos bloqueo Constrase&ntilde;a</label>
                                                                            <input class="form-control" id="txtNoIntentosPassword" name="txtNoIntentosPassword" value="'.$_data["num_intentos_bloqueo_cuenta"].'" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <!-- /.col-lg-6 (nested) -->
                                                                </div>
                                                                <!-- /.row (nested) -->
                                                        </div>
                                                        <!-- /.panel-body -->
                                                    </div>
                                                    <!-- /.panel -->
                                                </div>
                                                <!-- /.col-lg-12 -->
                                            </div>
                                        </div><!-- /.modal-body -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->

                            <!-- /.modal -->';
        }

        $array = array(0 => $_form);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fModificarSeguridad($_pIdParametroSeguridad) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_parametros_seguridad, a.historia_password, a.edad_max_password, a.edad_min_password, a.longitud_max_password,
                a.tiempo_duracion_password, a.complejidad_password, a.num_intentos_bloqueo_cuenta
                FROM tbl_parametros_seguridad a
                where a.id_parametros_seguridad=".$_pIdParametroSeguridad;
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        foreach ($_vlv_Resultado as $_data)
        {
            switch($_data["complejidad_password"])
            {
                case 'S':
                    $vlvCombo='<select class="form-control required" id="cbComplejidadPassword" name="cbComplejidadPassword" required>
                                            <option value="">Seleccione</option>
                                            <option value="S" selected>Si</option>
                                            <option value="N">No</option>
                                        </select>';
                break;
                case 'N':
                    $vlvCombo='<select class="form-control required" id="cbComplejidadPassword" name="cbComplejidadPassword" required>
                                            <option value="">Seleccione</option>
                                            <option value="S">Si</option>
                                            <option value="N" selected>No</option>
                                        </select>';
                break;
                default:
                    $vlvCombo='<select class="form-control required" id="cbComplejidadPassword" name="cbComplejidadPassword" required>
                                            <option value="" selected>Seleccione</option>
                                            <option value="S">Si</option>
                                            <option value="N">No</option>
                                        </select>';
                break;
            }


            $_form='<!-- Modal -->
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarLista(\'seguridades.php\');">&times;</button>
                                            <div id="divMensajeModal"></div>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="panel panel-primary">
                                                        <div class="panel-heading">
                                                            Modeificar Par&aacute;metros del Sistema - ID. '.$_data["id_parametros_seguridad"].'
                                                        </div>
                                                        <div class="panel-body">
                                                            <form role="form" data-toggle="validator" id="frmSeguridad" >
                                                                <input type="hidden" id="hddIdParametro" name="hddIdParametro" value="'.$_data["id_parametros_seguridad"].'">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>Historial de Constrase&ntilde;a</label>
                                                                            <input class="form-control required digits" id="txtHistorialPassword" name="txtHistorialPassword" value="'.$_data["historia_password"].'">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Edad m&aacute;xima de Constrase&ntilde;a</label>
                                                                            <input class="form-control required digits" id="txtEdadMaxPassword" name="txtEdadMaxPassword" value="'.$_data["edad_max_password"].'">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Complejidad de Constrase&ntilde;a</label>
                                                                            '.$vlvCombo.'
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Tiempo de bloqueo de Constrase&ntilde;a</label><i class="fa fa-asterisk" style="color:red"></i>
                                                                            <input class="form-control required digits" id="txtTiempoBloqueoPassword" name="txtTiempoBloqueoPassword" value="'.$_data["tiempo_duracion_password"].'">
                                                                        </div>
                                                                    </div>
                                                                    <!-- /.col-lg-6 (nested) -->
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>Longitud m&aacute;xima de Constrase&ntilde;a</label>
                                                                            <input class="form-control required digits" id="txtLongitudMaxPassword" name="txtLongitudMaxPassword" value="'.$_data["longitud_max_password"].'">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Edad m&iacute;nima de Constrase&ntilde;a</label>
                                                                            <input class="form-control required digits" id="txtEdadMinPassword" name="txtEdadMinPassword" value="'.$_data["edad_min_password"].'">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>No. intentos bloqueo Constrase&ntilde;a</label>
                                                                            <input class="form-control required digits" id="txtNoIntentosPassword" name="txtNoIntentosPassword" value="'.$_data["num_intentos_bloqueo_cuenta"].'">
                                                                        </div>
                                                                    </div>
                                                                    <!-- /.col-lg-6 (nested) -->
                                                                </div>
                                                                <!-- /.row (nested) -->
                                                            </form>
                                                        </div>
                                                        <!-- /.panel-body -->
                                                    </div>
                                                    <!-- /.panel -->
                                                </div>
                                                <!-- /.col-lg-12 -->
                                            </div>
                                        </div><!-- /.modal-body -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" onClick="jsActualizarSeguridades();">Guardar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarLista(\'seguridades.php\');">Cerrar</button>

                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->

                            <!-- /.modal -->';
        }

        $array = array(0 => $_form);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
?>