<?php
require_once('../php/include/init.php');

header("Content-Type: application/vnd.ms-excel");

header("Expires: 0");

header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

$pFecha=date('d-m-Y_H:i:s',strtotime(now()));

header("content-disposition: attachment;filename=reporte_edificios_".$pFecha.".xls");

$_pIdEdificio    =$_REQUEST['pIdEdificio'];
$_pIdNivel    =$_REQUEST['pIdNivel'];

$_operacionesbd=new operacionesbd();
if($_pIdEdificio<>'')
    {
        if($_pIdNivel<>'')
            $where="and a.id_edificio=".$_pIdEdificio." and a.id_nivel=".$_pIdNivel;
        else
            $where="and a.id_edificio=".$_pIdEdificio;

    }
    else
        $where="";

$vlnNum=0;
$p_SQL="SELECT
        b.id_tipo_elemento , 
            b.detalle , 
            c.detalle_corto , 
            a.valoracion , 
            c.detalle as criterio
            FROM tbl_valoracion_dano a
            inner join tbl_tipo_elemento b on a.id_elemento = b.id_tipo_elemento
            inner join tbl_dano_detalle c on a.id_dano_detalle = c.id_dano_detalle
        WHERE a.estado is null ".$where;
$_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
$vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
$_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

$_form='<!DOCTYPE html>
<html>
<body>
            ';
$_form.='
        <table width="100%" id="ver_reportes" border=1>
            <thead>
            <tr>
            <th>Codigo Elemento</th>
            <th>Nombre Elemento</th>
            <th>Gravedad de Dano</th>
            <th>Porcentaje de Dano</th>
            <th>Criterio de Dano</th>
            
        </tr>';

$_form.='
            </thead>
        <tbody>';
foreach ($_vlv_Resultado as $_data)
{
$_form.='       <tr class="odd gradeA">
                    <td class="center">'.$_data["id_tipo_elemento"].'</td>
                    <td class="center">'.$_data["detalle"].'</td>
                    <td class="center">'.$_data["detalle_corto"].'</td>
                    <td class="center">'.number_format($_data["valoracion"],2).'</td>';
                    if(number_format($_data["valoracion"],2) >= 60){
                        $_form.='<td class="center" style="background-color:#EC0D0D";><p style="color:white";>'.$_data["criterio"].'</p></td>';
                    }
                    if(number_format($_data["valoracion"],2) >= 30 and number_format($_data["valoracion"],2) < 60){
                        $_form.='<td class="center" style="background-color:#A6A705";><p style="color:white";>'.$_data["criterio"].'</p></td>';
                    }
                    if(number_format($_data["valoracion"],2) < 30 ){
                        $_form.='<td class="center" style="background-color:#048B0D";><p style="color:white";>'.$_data["criterio"].'</p></td>';
                    }
                    $_form.='
                </tr>';

}
$_form.='
        </tbody>
    </table>
    </body>
     </html>
    ';
 ECHO $_form;
?>