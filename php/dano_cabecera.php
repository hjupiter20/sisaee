<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pAccion               =$_POST["pAccion"];
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$pNombreTabla="tbl_dano_cabecera";
$pNombreDataTable="ver_tipo_area";
$pNombreModal="ModModificarTipoArea";
$pNombreHeader="Daño Cabecera";
$pPlaceHolder="Ingrese el tipo de &aacute;rea";

$p_funcion="SP_GUARDAR_DANO_CABECERA";
$p_funcionAnulaActiva="SP_ANULAR_ACTIVAR_DANO_CABECERA";

/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        $_pNombreArchivo        =$_POST["pNombreArchivo"];
        fCargarFormulario();
    break;
    case 'GUARDAR':
         $_pDetalle    =$_POST['txtDetalle'];
         $_pIdElemento    =$_POST['cbTipoElemento'];
        fGuardar($_pDetalle, $_pIdElemento);
    break;
    case 'ACTUALIZAR':
        $_pDetalle    =$_POST['txtDetalle'];
        $_pId     =$_POST['hddId'];
        $_pIdElemento    =$_POST['cbTipoElemento'];
        fActualizar($_pId, $_pDetalle, $_pIdElemento);
    break;
    case 'VER_PANTALLA_MODIFICAR':
        $_pNombreArchivo        =$_POST["pNombreArchivo"];
        $_pId     =$_POST['pId'];
        fVerPantallaModificar($_pId);
    break;
    case 'ANULA_ACTIVA':
        $_pIdOpcion           =$_POST['pIdCodigo'];
        fAnularActivar($_pIdOpcion);
    break;
}

function fGuardar($_pDetalle, $_pIdElemento)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $_pId="null";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."','".strtoupper($_pDetalle)."',".$_pId.",".$_pIdElemento;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

function fActualizar($_pId, $_pDetalle, $_pIdElemento)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $p_parametros="'".$_pUsuario."','".$_pIp."','".strtoupper($_pDetalle)."',".$_pId.",".$_pIdElemento;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    global $_pNombreArchivo, $pNombreTabla,$pNombreDataTable,$pNombreModal, $pNombreFrm,$pNombreHeader,$pPlaceHolder;
    $_operacionesbd=new operacionesbd();
    $pNombreFrm="frmNuevo";
    $pNombreBtnGuardar="btnTipoEstructura";

    $_appcomponentes=new appcomponentes();
    /*CREAR COMBO ELEMENTOS*/
    $pNombreCombo='cbTipoElemento';
    $pCamposId='a.id_tipo_elemento';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_elemento a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbTipoElemento=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);


    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            '.$pNombreHeader.'
                        </div>
                        <div class="panel-body">
                            <form role="form" id="'.$pNombreFrm.'" >
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Descripci&oacute;n</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtDetalle" name="txtDetalle" placeholder="'.$pPlaceHolder.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Tipo de Elemento</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        '.$cbTipoElemento.'
                                        <p class="help-block"></p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="'.$pNombreBtnGuardar.'" onClick="jsGuardar(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                           <button type="button" class="btn btn-primary" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">Limpiar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_dano_cabecera, a.detalle, e.detalle as detalle_elemento, a.id_elemento
            FROM ".$pNombreTabla." a inner join tbl_tipo_elemento e on a.id_elemento = e.id_tipo_elemento ";
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

     $_form.='
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="'.$pNombreDataTable.'">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Id. Registro</th>
                                    <th>Descripci&oacute;n</th>
                                    <th>Elemento</th>
                                    <th>Id Elemento</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>';

   foreach ($_vlv_Resultado as $_data)
    {
        $_form.='           <tr class="odd gradeA">
                                <td class="center">'.$_data["estado"].'</td>
                                <td class="center">'.$_data["id_dano_cabecera"].'</td>
                                <td class="center">'.$_data["detalle"].'</td>
                                <td class="center">'.$_data["detalle_elemento"].'</td>
                                <td class="center">'.$_data["id_elemento"].'</td>
                            ';
        if($_data["estado"]=='A')
            {
                $_form.='       <td align="center">&nbsp;</td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Registro" onClick="jsAnularActivar('.$_data["id_dano_cabecera"].',\''.$_pNombreArchivo.'\')"><i class="fa fa-check"></i></button></td>';

            }
            else
            {
                $_form.='       <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#'.$pNombreModal.'" onClick="jsVerPantallaModificar('.$_data["id_dano_cabecera"].',\''.$_pNombreArchivo.'\',\''.$pNombreModal.'\')">Modificar</button></td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Registro" onClick="jsAnularActivar('.$_data["id_dano_cabecera"].',\''.$_pNombreArchivo.'\')"><i class="fa fa-times"></i></button></td>';
            }
        $_form.='           </tr>';
    }
    $_form.='               </tbody>
                        </table>
                    </div> <!-- /.panel-body -->
                </div>  <!-- /.panel -->
            </div> <!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <script>
        $(document).ready(function() {
            $("#'.$pNombreDataTable.'").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                },
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 0, 1 ]
                } ]
            });
        });
        </script>
        ';
    $_form.='<div class="modal fade" id="'.$pNombreModal.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             </div>';
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}

function fVerPantallaModificar($_pIdOpcion)
{
    global $_pNombreArchivo, $pNombreTabla;
    $pNombreFrm="frmActualizar";
    $_operacionesbd=new operacionesbd();
    $jsondata = array();
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_dano_cabecera, a.detalle, a.id_elemento
            FROM ".$pNombreTabla." a
            where a.id_dano_cabecera=".$_pIdOpcion;
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);


    $_appcomponentes=new appcomponentes();
    /*CREAR COMBO ELEMENTOS*/
    $pNombreCombo='cbTipoElemento';
    $pCamposId='a.id_tipo_elemento';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_elemento a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pRequerido='required';
    
    foreach ($_vlv_Resultado as $_data)
    {
        $pSelected=$_data["id_elemento"];
        $cbTipoElemento=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);
        $_form='
        <!-- Modal -->
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">&times;</button>
                        <div id="divMensajeModal"></div>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Modificar Registro - ID: '.$_data["id_dano_cabecera"].'
                                </div>
                                <div class="panel-body">
                                <form role="form" id="'.$pNombreFrm.'" >
                                <input type="hidden" id="hddId" name="hddId" value="'.$_data["id_dano_cabecera"].'">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Descripci&oacute;n</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                    <input class="form-control required" id="txtDetalle" name="txtDetalle"
                                                    value="'.$_data["detalle"].'">
                                                    <p class="help-block with-errors">Ingrese la descripci&oacute;n.</p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tipo de Elemento</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                    '.$cbTipoElemento.'
                                                    <p class="help-block"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </form>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div><!-- /.modal-body -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onClick="jsActualizar(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        <!-- /.modal -->';
    }

    $array = array(0 => $_form);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}
function fAnularActivar($_pIdOpcion)
{
    global $_pUsuario, $_pIp, $p_funcionAnulaActiva;
    $_operacionesbd=new operacionesbd();

    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdOpcion;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcionAnulaActiva,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>