<?php
require_once('../php/include/init.php');

header("Content-Type: application/vnd.ms-excel");

header("Expires: 0");

header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

$pFecha=date('d-m-Y_H:i:s',strtotime(now()));

header("content-disposition: attachment;filename=reporte_edificios_".$pFecha.".xls");

$_pIdEdificio    =$_REQUEST['pIdEdificio'];
$_pIdNivel    =$_REQUEST['pIdNivel'];

$_operacionesbd=new operacionesbd();
if($_pIdEdificio<>'')
    {
        if($_pIdNivel<>'')
            $where="and a.id_edificio=".$_pIdEdificio." and b.id_nivel=".$_pIdNivel;
        else
            $where="and a.id_edificio=".$_pIdEdificio;

    }
    else
        $where="";

$vlnNum=0;
$p_SQL="SELECT
        a.id_edificio, a.nombre_edificio, a.direccion, a.propietario, a.predio
        , a.ancho, a.alto, a.fondo, a.codigo_obra, a.valor_edificio, a.contratista
        , a.id_tipo_edificio, d.detalle as tipo_edificio
        , b.id_nivel, c.detalle as tipo_nivel, b.alto as altura_niveles
        , b.ancho as ancho_nivel, b.fondo as largo_nivel, e.detalle as tipo_estructura
        , f.id_area_nivel, g.detalle as tipo_area, f.alto as altura_niveles_area
        , f.fondo as ancho_area, f.largo as largo_area, h.id_nivel_area_elemento
        , j.detalle as tipo_elemento_edificio
        , k.nombre_elemento, l.detalle as tipo_elemento_nivel, k.ancho as ancho_elementos
        , k.largo as largo_elementos
        , k.alto as altura_niveles_elemento, k.espesor, k.armadura
        , m.detalle as tipo_material_elemento, k.resistencia, k.acero_frecuencia
        FROM tbl_edificio a
        left OUTER join tbl_nivel b on a.id_edificio=b.id_edificio and b.estado is NULL
        left OUTER join tbl_tipo_nivel c on b.id_tipo_nivel=c.id_tipo_nivel
        left OUTER join tbl_tipo_edificio d on a.id_tipo_edificio=d.id_tipo_edificio
        left OUTER join tbl_tipo_estructura e on b.id_tipo_estructura=e.id_tipo_estructura
        left OUTER join tbl_area_nivel f on b.id_nivel=f.id_nivel and f.estado is null
        left OUTER join tbl_tipo_area g on f.id_tipo_area=g.id_tipo_area
        left outer join tbl_nivel_area_elemento h on f.id_area_nivel=h.id_area_nivel and h.estado is NULL
        left OUTER join  tbl_elemento_nivel i on h.id_elemento_nivel=i.id_elemento_nivel
        left OUTER join tbl_tipo_elemento j on i.id_tipo_elemento=j.id_tipo_elemento
        left outer join tbl_elemento_nivel k on b.id_nivel=k.id_nivel and k.estado is null
        left outer join tbl_tipo_elemento l on k.id_tipo_elemento=l.id_tipo_elemento and l.estado is null
        left outer join tbl_tipo_material m on m.id_tipo_material=k.id_tipo_material
        WHERE a.estado is null ".$where;
$_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
$vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
$_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

$_form='<!DOCTYPE html>
<html>
<body>
            ';
$_form.='
        <table width="100%" id="ver_reportes" border=1>
            <thead>
                <tr>
                    <th colspan="33" align="center" style="color: #fff;background-color: #337ab7;border-color: #337ab7;text-align: center;">Reporte Edificios</th>
                </tr>
                <tr>
                    <th colspan="12" style="color: #31708f;background-color: #d9edf7;text-align: center;">Edificio</th>
                    <th colspan="5" style="color: #31708f;background-color: #d9edf7;text-align: center;">Nivel</th>
                    <th colspan="5" style="color: #31708f;background-color: #d9edf7;text-align: center;">&Aacute;rea</th>
                    <th colspan="1" style="color: #31708f;background-color: #d9edf7;text-align: center;">&nbsp;</th>
                    <th colspan="10" style="color: #31708f;background-color: #d9edf7;text-align: center;">Elemento</th>
                </tr>
                <tr>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Id Edificio</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Edificio</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Direcci&oacute;n</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Propietario</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Predio</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Ancho</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Alto</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Fondo</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Cod. Obra</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Valor</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Contratista</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Tipo Edificio</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Id Nivel</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Tipo Nivel</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Altura Nivel</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Ancho Nivel</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Largo Nivel</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Tipo Estructura</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Tipo &Aacute;rea</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Altura &Aacute;rea</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Ancho &Aacute;rea</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Largo &Aacute;rea</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Elemento Edificio</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Nombre Elemento</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Tipo Elemento</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Ancho Elemento</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Largo Elemento</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Altura Elemento</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Espesor</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Armadura</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Tipo Material</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Resistencia</th>
                    <th style="color: #8a6d3b;background-color: #fcf8e3;">Acero/Frecuencia</th>
                </tr>';

$_form.='
            </thead>
        <tbody>';
foreach ($_vlv_Resultado as $_data)
{
$_form.='       <tr class="odd gradeA">
                    <td class="center">'.$_data["id_edificio"].'</td>
                    <td class="center">'.$_data["nombre_edificio"].'</td>
                    <td class="center">'.$_data["direccion"].'</td>
                    <td class="center">'.$_data["propietario"].'</td>
                    <td class="center">'.$_data["predio"].'</td>
                    <td class="center">'.$_data["ancho"].'</td>
                    <td class="center">'.$_data["alto"].'</td>
                    <td class="center">'.$_data["fondo"].'</td>
                    <td class="center">'.$_data["codigo_obra"].'</td>
                    <td class="center">'.$_data["valor_edificio"].'</td>
                    <td class="center">'.$_data["contratista"].'</td>
                    <td class="center">'.$_data["tipo_edificio"].'</td>
                    <td class="center">'.$_data["id_nivel"].'</td>
                    <td class="center">'.$_data["tipo_nivel"].'</td>
                    <td class="center">'.$_data["altura_niveles"].'</td>
                    <td class="center">'.$_data["ancho_nivel"].'</td>
                    <td class="center">'.$_data["largo_nivel"].'</td>
                    <td class="center">'.$_data["tipo_estructura"].'</td>
                    <td class="center">'.$_data["tipo_area"].'</td>
                    <td class="center">'.$_data["altura_niveles_area"].'</td>
                    <td class="center">'.$_data["ancho_area"].'</td>
                    <td class="center">'.$_data["largo_area"].'</td>
                    <td class="center">'.$_data["tipo_elemento_edificio"].'</td>
                    <td class="center">'.$_data["nombre_elemento"].'</td>
                    <td class="center">'.$_data["tipo_elemento_nivel"].'</td>
                    <td class="center">'.$_data["ancho_elementos"].'</td>
                    <td class="center">'.$_data["largo_elementos"].'</td>
                    <td class="center">'.$_data["altura_niveles_elemento"].'</td>
                    <td class="center">'.$_data["espesor"].'</td>
                    <td class="center">'.$_data["armadura"].'</td>
                    <td class="center">'.$_data["tipo_material_elemento"].'</td>
                    <td class="center">'.$_data["resistencia"].'</td>
                    <td class="center">'.$_data["acero_frecuencia"].'</td>
                </tr>
                ';

}
$_form.='
        </tbody>
    </table>
    </body>
     </html>
    ';
 ECHO $_form;
?>