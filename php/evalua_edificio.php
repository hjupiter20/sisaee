<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pAccion               =$_POST["pAccion"];
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$pNombreTabla="tbl_edificio_evaluacion";
$pNombreDataTable="ver_edificio";
$pNombreModal="ModModificarEdificio";
$pNombreHeader="Edificio Evaluar";
$pPlaceHolder="Ingrese la descripci&oacute;n";
$pPlaceHolder1="Ingrese Ancho Frontal";
$pPlaceHolder2="Ingrese Alto Frontal";
$pPlaceHolder3="Ingrese el Largo";
$pPlaceHolder4="Ingrese el valor";
$pPlaceHolder5="Nombre del Contratista";
$pPlaceHolder6="Ingrese Ancho Posterior";
$pPlaceHolder7="Ingrese Alto Posterior";
$pPlaceHolder8="Ingrese Cota del Edificio";
$pPlaceHolder9="Ingrese el # Ruc Contratista";

$p_funcion="SP_GUARDAR_EDIFICIO_EVALUACION";
$p_funcionAnulaActiva="SP_ANULAR_EDIFICIO_EVALUACION";

/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        fCargarFormulario();
    break;
    case 'GUARDAR':
        $_pFechaSismo               =$_POST['txtFechaSismo'];
        $_pEpicentroCismo           =$_POST['txtEpicentroCismo'];
        $_pMagnitud                 =$_POST['txtMagnitud'];
        $_pEdificio                 =$_POST['cbEdificio'];
        $_pFrontalIzquierda         =$_POST['txtFrontalIzquierda'];
        $_pFrontalDerecha           =$_POST['txtFrontalDerecha'];
        $_pCotaPosteriorDerecha     =$_POST['txtCotaPosteriorDerecha'];
        $_pCotaPosteriorIzquierda   =$_POST['txtCotaPosteriorIzquierda'];
        
        fGuardar($_pFechaSismo, $_pEpicentroCismo, $_pMagnitud, $_pEdificio, $_pFrontalIzquierda, $_pFrontalDerecha, $_pCotaPosteriorDerecha, 
        $_pCotaPosteriorIzquierda);
    break;
    case 'ACTUALIZAR':
        $_pId               =$_POST['hddId'];
        $_pFechaSismo               =$_POST['txtFechaSismo'];
        $_pEpicentroCismo           =$_POST['txtEpicentroCismo'];
        $_pMagnitud                 =$_POST['txtMagnitud'];
        $_pEdificio                 =$_POST['cbEdificio'];
        $_pFrontalIzquierda         =$_POST['txtFrontalIzquierda'];
        $_pFrontalDerecha           =$_POST['txtFrontalDerecha'];
        $_pCotaPosteriorDerecha     =$_POST['txtCotaPosteriorDerecha'];
        $_pCotaPosteriorIzquierda   =$_POST['txtCotaPosteriorIzquierda'];
        fActualizar($_pId, $_pFechaSismo, $_pEpicentroCismo, $_pMagnitud, $_pEdificio, $_pFrontalIzquierda, $_pFrontalDerecha, $_pCotaPosteriorDerecha,
        $_pCotaPosteriorIzquierda);
    break;
    case 'VER_PANTALLA_MODIFICAR':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $_pId               =$_POST['pId'];
        fVerPantallaModificar($_pId);
    break;
    case 'ANULA_ACTIVA':
        $_pIdOpcion         =$_POST['pIdCodigo'];
        fAnularActivar($_pIdOpcion);
    break;
}

function fGuardar($_pFechaSismo, $_pEpicentroCismo, $_pMagnitud, $_pEdificio, $_pFrontalIzquierda, $_pFrontalDerecha, $_pCotaPosteriorDerecha, $_pCotaPosteriorIzquierda)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $_pId="null";
    //$vlvResultado=array();
    $p_parametros=  "'".$_pUsuario.
                    "','".$_pIp.
                    "','".$_pFechaSismo.
                    "','".$_pEpicentroCismo.
                    "',".$_pMagnitud.
                    ",".$_pEdificio.
                    ",".$_pFrontalIzquierda.
                    ",".$_pFrontalDerecha;
    $p_parametros.= ",".$_pCotaPosteriorDerecha.
                    ",".$_pCotaPosteriorIzquierda.
                    ",".$_pId;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

function fActualizar($_pId, $_pFechaSismo, $_pEpicentroCismo, $_pMagnitud, $_pEdificio, $_pFrontalIzquierda, $_pFrontalDerecha, $_pCotaPosteriorDerecha, $_pCotaPosteriorIzquierda)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $p_parametros=  "'".$_pUsuario.
                    "','".$_pIp.
                    "','".$_pFechaSismo.
                    "','".$_pEpicentroCismo.
                    "',".$_pMagnitud.
                    ",".$_pEdificio.
                    ",".$_pFrontalIzquierda.
                    ",".$_pFrontalDerecha;
    $p_parametros.= ",".$_pCotaPosteriorDerecha.
                    ",".$_pCotaPosteriorIzquierda.
                    ",".$_pId;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    global $_pNombreArchivo, $pNombreTabla,$pNombreDataTable,$pNombreModal, $pNombreFrm,$pNombreHeader,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3,$pPlaceHolder4,$pPlaceHolder5,$pPlaceHolder6,$pPlaceHolder7,$pPlaceHolder8,$pPlaceHolder9;
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $pNombreFrm="frmNuevo";
    $pNombreBtnGuardar="btnEdificio";

    /*CREAR COMBO EDIFICIO*/
    $pNombreCombo='cbEdificio';
    $pCamposId='a.id_edificio';
    $pCamposDetalle='a.nombre_edificio';
    $pTabla='tbl_edificio a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbEdificio=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            '.$pNombreHeader.'
                        </div>
                        <div class="panel-body">
                            <form role="form" id="'.$pNombreFrm.'" >
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Fecha del Sismo</label>
                                        <input class="form-control required" id="txtFechaSismo" type="date" name="txtFechaSismo">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Epicentro del Cismo</label>
                                        <input class="form-control required" placeholder="Ingrese el Epicentro del Cismo"
                                        onkeypress="return soloLetras(event)" id="txtEpicentroCismo" name="txtEpicentroCismo">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Magnitud</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input onkeydown="return validarNumerosNegativos(event)" class="form-control required number" 
                                        type="number" min="0" step=".01" id="txtMagnitud" name="txtMagnitud" placeholder="Ingresar Magnitud">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Edificio a Evaluar</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        '.$cbEdificio.'
                                        <p class="help-block"></p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Cota Frontal Izquierda</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required number" type="number" min="0" 
                                        step=".01" id="txtFrontalIzquierda" name="txtFrontalIzquierda" placeholder="Ingresar Cota Frontal Izquierda">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Cota Frontal Derecha</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required number" type="number" min="0" 
                                        step=".01" id="txtFrontalDerecha" name="txtFrontalDerecha" placeholder="Ingresar Cota Frontal Derecha">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Cota Posterior Derecha</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required number" type="number" min="0" 
                                        step=".01" id="txtCotaPosteriorDerecha" name="txtCotaPosteriorDerecha" placeholder="Ingresar Cota Posterior Derecha">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Cota Posterior Izquierda</label>
                                        <input class="form-control required number" type="number" min="0" 
                                        step=".01" id="txtCotaPosteriorIzquierda" name="txtCotaPosteriorIzquierda" placeholder="Ingresar Cota Posterior Izquierda">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="'.$pNombreBtnGuardar.'" onClick="jsGuardar(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                           <button type="button" class="btn btn-primary" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">Limpiar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_edificio_evaluacion, a.epicentro_cismo, a.fecha_epicentro, a.magnitud, a.cota_fi, a.cota_fd, a.cota_pi, a.cota_pd, e.cota
            FROM ".$pNombreTabla." a inner join tbl_edificio e on a.id_edificio = e.id_edificio";
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

     $_form.='
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="'.$pNombreDataTable.'">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Estado Edificio</th>
                                    <th>Id. Edificio Eva.</th>
                                    <th>Epicentro</th>
                                    <th>Fecha</th>
                                    <th>Magnitud</th>
                                    <th>Cota Frontal Izquierda</th>
                                    <th>Cota Frontal Derecha</th>
                                    <th>Cota Posterior Izquierda</th>
                                    <th>Cota Posterior Derecha</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>';

    $pEstadoEdificioDesigual=0;
    foreach ($_vlv_Resultado as $_data)
    {
        $_form.='           <tr class="odd gradeA">
                                    <td class="center">'.$_data["estado"].'</td>';
        if($_data["cota_fi"] == $_data["cota_fd"] and $_data["cota_fi"] == $_data["cota_pi"] and $_data["cota_fi"]== $_data["cota_pd"]){
            if(abs($_data["cota_fi"] - $_data["cota"]) > 3){
                $_form.='<td class="center" style="background-color:#EC0D0D;"><p style="color:white";>El edificio se encuentra en graves condiciones y no es habitable</p>';
            }
            if(abs($_data["cota_fi"] - $_data["cota"]) > 1 and abs($_data["cota_fi"] - $_data["cota"]) < 3){
                $_form.='<td class="center" style="background-color:#A6A705;"><p style="color:white";>Asentamiento admisible, pero hay que tomar en cuenta las instalaciones eléctricas , as, a.a.pp </p>';
            }
            if(abs($_data["cota_fi"] - $_data["cota"]) < 1){
                $_form.='<td class="center" style="background-color:#048B0D;"><p style="color:white";>No presenta mayor problema</p>';
            }
        }
        else{
            if( abs($_data["cota_fi"] - $_data["cota"]) > 1 or 
                abs($_data["cota_fd"] - $_data["cota"]) > 1 or 
                abs($_data["cota_pi"] - $_data["cota"]) > 1 or 
                abs($_data["cota_pd"] - $_data["cota"]) > 1){
                $_form.='<td class="center" style="background-color:#EC0D0D;"><p style="color:white";>No es tolerable y el edificio se encuentra en condiciones graves</p>';
                $pEstadoEdificioDesigual = 1;
            }
            if( ((abs($_data["cota_fi"] - $_data["cota"]) > 0.25 and abs($_data["cota_fi"] - $_data["cota"]) < 1) or 
                (abs($_data["cota_fd"] - $_data["cota"]) > 0.25 and abs($_data["cota_fd"] - $_data["cota"]) < 1) or 
                (abs($_data["cota_pi"] - $_data["cota"]) > 0.25 and abs($_data["cota_pi"] - $_data["cota"]) < 1) or 
                (abs($_data["cota_pd"] - $_data["cota"]) > 0.25 and abs($_data["cota_pd"] - $_data["cota"]) < 1)) and 
                $pEstadoEdificioDesigual == 0){
                $_form.='<td class="center" style="background-color:#A6A705;"><p style="color:white";>Se debe realizar reparaciones al edificio</p>';
                $pEstadoEdificioDesigual = 2;
            }
            if( (abs($_data["cota_fi"] - $_data["cota"]) < 0.25 or 
                abs($_data["cota_fd"] - $_data["cota"]) < 0.25 or 
                abs($_data["cota_pi"] - $_data["cota"]) < 0.25 or 
                abs($_data["cota_pd"] - $_data["cota"]) < 0.25) and
                $pEstadoEdificioDesigual == 0){
                $_form.='<td class="center" style="background-color:#048B0D;"><p style="color:white";>Es tolerable pero se debería realizar un seguimiento al edificio y una evaluación a sus elementos estructurales</p>';
            }
        }
        $_form.='                            </td>
                                    <td class="center">'.$_data["id_edificio_evaluacion"].'</td>
                                    <td class="center">'.$_data["epicentro_cismo"].'</td>
                                    <td class="center">'.$_data["fecha_epicentro"].'</td>
                                    <td class="center">'.$_data["magnitud"].'</td>
                                    <td class="center">'.$_data["cota_fi"].'</td>
                                    <td class="center">'.$_data["cota_fd"].'</td>
                                    <td class="center">'.$_data["cota_pi"].'</td>
                                    <td class="center">'.$_data["cota_pd"].'</td>
                            ';
        if($_data["estado"]=='A')
            {
                $_form.='       <td align="center">&nbsp;</td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Registro" onClick="jsAnularActivar('.$_data["id_edificio_evaluacion"].',\''.$_pNombreArchivo.'\')"><i class="fa fa-check"></i></button></td>';

            }
            else
            {
                $_form.='       <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#'.$pNombreModal.'" onClick="jsVerPantallaModificar('.$_data["id_edificio_evaluacion"].',\''.$_pNombreArchivo.'\',\''.$pNombreModal.'\')">Modificar</button></td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Registro" onClick="jsAnularActivar('.$_data["id_edificio_evaluacion"].',\''.$_pNombreArchivo.'\')"><i class="fa fa-times"></i></button></td>';
            }
        $_form.='           </tr>';
    }
    $_form.='               </tbody>
                        </table>
                    </div> <!-- /.panel-body -->
                </div>  <!-- /.panel -->
            </div> <!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <script>
        $(document).ready(function() {
            $("#'.$pNombreDataTable.'").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                },
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 0, 1 ]
                } ]
            });
        });
        </script>
        ';
    $_form.='<div class="modal fade" id="'.$pNombreModal.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             </div>';
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}

function fVerPantallaModificar($_pIdOpcion)
{
    global $_pNombreArchivo, $pNombreTabla,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3;
    $pNombreFrm="frmActualizar";
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $jsondata = array();
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_edificio_evaluacion, a.epicentro_cismo, a.fecha_epicentro, a.magnitud, a.cota_fi, a.cota_fd, a.cota_pi, a.cota_pd, a.id_edificio
            FROM ".$pNombreTabla." a
            where a.id_edificio_evaluacion=".$_pIdOpcion;
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    /*CREAR COMBO TIPO EDIFICIO*/
    $pNombreCombo='cbEdificio';
    $pCamposId='a.id_edificio';
    $pCamposDetalle='a.nombre_edificio';
    $pTabla='tbl_edificio a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pRequerido='required';

    foreach ($_vlv_Resultado as $_data)
    {
        $pSelected=$_data["id_edificio_evaluacion"];
        $cbEdificio=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);
        $_form='
        <!-- Modal -->
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">&times;</button>
                        <div id="divMensajeModal"></div>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Modificar Registro - ID: '.$_data["id_edificio"].'
                                </div>
                                <div class="panel-body">
                                <form role="form" id="'.$pNombreFrm.'" >
                                <input type="hidden" id="hddId" name="hddId" value="'.$_data["id_edificio"].'">
                                    <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                <label>Fecha del Sismo</label>
                                                <input class="form-control required" id="txtFechaSismo" type="date" name="txtFechaSismo"
                                                value="'.$_data["fecha_epicentro"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Epicentro del Cismo</label>
                                                <input class="form-control required" placeholder="Ingrese el Epicentro del Cismo"
                                                onkeypress="return soloLetras(event)" id="txtEpicentroCismo" name="txtEpicentroCismo"
                                                placeholder="'.$pPlaceHolder.'" value="'.$_data["magnitud"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Magnitud</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input onkeydown="return validarNumerosNegativos(event)" class="form-control required number" 
                                                type="number" min="0" step=".01" id="txtMagnitud" name="txtMagnitud" placeholder="Ingresar Magnitud" 
                                                placeholder="'.$pPlaceHolder.'" value="'.$_data["magnitud"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Tipo de edificio</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                '.$cbEdificio.'
                                                <p class="help-block"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Cota Frontal Izquierda</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" type="number" min="0" 
                                                step=".01" id="txtFrontalIzquierda" name="txtFrontalIzquierda"
                                                placeholder="'.$pPlaceHolder.'" value="'.$_data["cota_fi"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Cota Frontal Derecha</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" type="number" min="0" 
                                                step=".01" id="txtFrontalDerecha" name="txtFrontalDerecha" 
                                                placeholder="'.$pPlaceHolder.'" value="'.$_data["cota_fd"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Cota Posterior Derecha</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" type="number" min="0" 
                                                step=".01" id="txtCotaPosteriorDerecha" name="txtCotaPosteriorDerecha"
                                                placeholder="'.$pPlaceHolder.'" value="'.$_data["cota_pd"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Cota Posterior Izquierda</label>
                                                <input class="form-control required number" type="number" min="0" 
                                                step=".01" id="txtCotaPosteriorIzquierda" name="txtCotaPosteriorIzquierda"
                                                placeholder="'.$pPlaceHolder.'" value="'.$_data["cota_pi"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </form>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div><!-- /.modal-body -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onClick="jsActualizar(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        <!-- /.modal -->';
    }

    $array = array(0 => $_form);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}
function fAnularActivar($_pIdOpcion)
{
    global $_pUsuario, $_pIp, $p_funcionAnulaActiva;
    $_operacionesbd=new operacionesbd();

    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdOpcion;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcionAnulaActiva,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>
