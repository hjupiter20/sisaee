<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pAccion               =$_POST["pAccion"];
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$pNombreTabla="tbl_edificio";
$pNombreDataTable="ver_edificio";
$pNombreModal="ModModificarEdificio";
$pNombreHeader="Edificio";
$pPlaceHolder="Ingrese la descripci&oacute;n";
$pPlaceHolder1="Ingrese Ancho Frontal";
$pPlaceHolder2="Ingrese Alto Frontal";
$pPlaceHolder3="Ingrese el Largo";
$pPlaceHolder4="Ingrese el valor";
$pPlaceHolder5="Nombre del Contratista";
$pPlaceHolder6="Ingrese Ancho Posterior";
$pPlaceHolder7="Ingrese Alto Posterior";
$pPlaceHolder8="Ingrese Cota del Edificio";
$pPlaceHolder9="Ingrese el # Ruc Contratista";

$p_funcion="SP_GUARDAR_EDIFICIO";
$p_funcionAnulaActiva="SP_ANULAR_ACTIVAR_EDIFICIO";

/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        fCargarFormulario();
    break;
    case 'GUARDAR':
        $_pNombreEdificio   =$_POST['txtNombreEdificio'];
        $_pPropietario      =$_POST['txtPropietario'];
        $_pAncho            =$_POST['txtAncho'];
        $_pAlto             =$_POST['txtAlto'];
        $_pValor            =$_POST['txtValor'];
        $_pTipoEdificio     =$_POST['cbTipoEdificio'];
        $_pDireccion        =$_POST['txtDireccion'];

        $_pPredio           =$_POST['txtPredio'];
        $_pManzana          =$_POST['txtManzana'];
        $_pLote             =$_POST['txtLote'];
        $_pDivision         =$_POST['txtDivision'];
        $_pPhv              =$_POST['txtPhv'];
        $_pPhh              =$_POST['txtPhh'];
        $_pNumero           =$_POST['txtNumero'];
        
        $_pCodigoObra       =$_POST['txtCodigoObra'];
        $_pFondo            =$_POST['txtFondo'];
        $_pContratista      =$_POST['txtContratista'];
        $_pTipoEstructura   =$_POST['cbTipoEstructura'];
        $_pAltoPosterior    =$_POST['txtAltoPosterior'];
        $_pAnchoPosterior   =$_POST['txtAnchoPosterior'];
        $_pCota             =$_POST['txtCota'];
        $_pRuc              =$_POST['txtRuc'];
        fGuardar($_pNombreEdificio, $_pPropietario, $_pAncho,$_pAlto, $_pValor,$_pTipoEdificio, $_pDireccion, 
        $_pPredio, $_pManzana, $_pLote, $_pDivision, $_pPhv, $_pPhh, $_pNumero,
        $_pCodigoObra, $_pFondo, $_pContratista, $_pTipoEstructura,$_pAltoPosterior, $_pAnchoPosterior, $_pCota, $_pRuc);
    break;
    case 'ACTUALIZAR':
        $_pId               =$_POST['hddId'];
        $_pNombreEdificio   =$_POST['txtNombreEdificio'];
        $_pPropietario      =$_POST['txtPropietario'];
        $_pAncho            =$_POST['txtAncho'];
        $_pAlto             =$_POST['txtAlto'];
        $_pValor            =$_POST['txtValor'];
        $_pTipoEdificio     =$_POST['cbTipoEdificio'];
        $_pDireccion        =$_POST['txtDireccion'];
        $_pPredio           =$_POST['txtPredio'];
        $_pCodigoObra       =$_POST['txtCodigoObra'];
        $_pFondo            =$_POST['txtFondo'];
        $_pContratista      =$_POST['txtContratista'];
        $_pTipoEstructura   =$_POST['cbTipoEstructura'];
        $_pAltoPosterior    =$_POST['txtAltoPosterior'];
        $_pAnchoPosterior   =$_POST['txtAnchoPosterior'];
        $_pCota             =$_POST['txtCota'];
        $_pRuc              =$_POST['txtRuc'];
        fActualizar($_pId, $_pNombreEdificio, $_pPropietario, $_pAncho,$_pAlto, $_pValor,$_pTipoEdificio, $_pDireccion, $_pPredio, $_pCodigoObra, $_pFondo, $_pContratista, $_pTipoEstructura, $_pAltoPosterior, $_pAnchoPosterior, $_pCota, $_pRuc);
    break;
    case 'VER_PANTALLA_MODIFICAR':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $_pId               =$_POST['pId'];
        fVerPantallaModificar($_pId);
    break;
    case 'ANULA_ACTIVA':
        $_pIdOpcion         =$_POST['pIdCodigo'];
        fAnularActivar($_pIdOpcion);
    break;
}

function fGuardar($_pNombreEdificio, $_pPropietario, $_pAncho, $_pAlto, $_pValor,$_pTipoEdificio, $_pDireccion, 
$_pPredio, $_pManzana, $_pLote, $_pDivision, $_pPhv, $_pPhh, $_pNumero,
$_pCodigoObra, $_pFondo, $_pContratista, $_pTipoEstructura, $_pAltoPosterior, $_pAnchoPosterior, $_pCota, $_pRuc)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $_pId="null";
    $_pPredio = $_pPredio."-".$_pManzana."-".$_pLote."-".$_pDivision."-".$_pPhv."-".$_pPhh."-".$_pNumero;
    //$vlvResultado=array();
    $p_parametros=  "'".$_pUsuario.
                    "','".$_pIp.
                    "','".strtoupper($_pNombreEdificio).
                    "','".strtoupper($_pPropietario).
                    "',".$_pAncho.
                    ",".$_pAlto.
                    ",".$_pValor.
                    ",".$_pTipoEdificio;
    $p_parametros.= ",'".strtoupper($_pDireccion).
                    "','".strtoupper($_pPredio).
                    "','".strtoupper($_pCodigoObra).
                    "',".$_pFondo.
                    ",'".strtoupper($_pContratista);
    $p_parametros.= "',".$_pTipoEstructura.
                    ",".$_pId.
                    ",".$_pAltoPosterior.
                    ",".$_pAnchoPosterior;
    $p_parametros.= ",".$_pCota.
                    ",'".$_pRuc."'";

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

function fActualizar($_pId, $_pNombreEdificio, $_pPropietario, $_pAncho, $_pAlto, $_pValor,$_pTipoEdificio, $_pDireccion, $_pPredio, $_pCodigoObra, $_pFondo, $_pContratista, $_pTipoEstructura, $_pAltoPosterior, $_pAnchoPosterior, $_pCota, $_pRuc)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $p_parametros="'".$_pUsuario."','".$_pIp."','".strtoupper($_pNombreEdificio)."','".strtoupper($_pPropietario)."',".$_pAncho.",".$_pAlto.",".$_pValor.",".$_pTipoEdificio;
    $p_parametros.=",'".strtoupper($_pDireccion)."','".strtoupper($_pPredio)."','".strtoupper($_pCodigoObra)."',".$_pFondo.",'".strtoupper($_pContratista);
    $p_parametros.= "',".$_pTipoEstructura.
                    ",".$_pId.
                    ",".$_pAltoPosterior.
                    ",".$_pAnchoPosterior;
    $p_parametros.= ",".$_pCota.
                    ",'".$_pRuc."'";

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    global $_pNombreArchivo, $pNombreTabla,$pNombreDataTable,$pNombreModal, $pNombreFrm,$pNombreHeader,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3,$pPlaceHolder4,$pPlaceHolder5,$pPlaceHolder6,$pPlaceHolder7,$pPlaceHolder8,$pPlaceHolder9;
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $pNombreFrm="frmNuevo";
    $pNombreBtnGuardar="btnEdificio";

    /*CREAR COMBO TIPO EDIFICIO*/
    $pNombreCombo='cbTipoEdificio';
    $pCamposId='a.id_tipo_edificio';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_edificio a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbTipoEdificio=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    /*CREAR COMBO TIPO ESTRUCTURA*/
    $pNombreCombo='cbTipoEstructura';
    $pCamposId='a.id_tipo_estructura';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_estructura a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbTipoEstructura=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            '.$pNombreHeader.'
                        </div>
                        <div class="panel-body">
                            <form role="form" id="'.$pNombreFrm.'" >
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input class="form-control required" id="txtNombreEdificio" name="txtNombreEdificio" placeholder="'.$pPlaceHolder.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Propietario</label>
                                        <input class="form-control" id="txtPropietario" name="txtPropietario" placeholder="'.$pPlaceHolder.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Ancho Frontal (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input onkeydown="return validarNumerosNegativos(event)" class="form-control required number" type="number" min="0" step=".01" id="txtAncho" name="txtAncho" placeholder="'.$pPlaceHolder1.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Alto Frontal (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input onkeydown="return validarNumerosNegativos(event)" class="form-control required number" type="number" min="0" step=".01" id="txtAlto" name="txtAlto" placeholder="'.$pPlaceHolder2.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Valor edificio</label>
                                        <input onkeydown="return validarNumerosNegativos(event)" class="form-control number" type="number" min="0" step=".01" id="txtValor" name="txtValor" placeholder="'.$pPlaceHolder4.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Cota (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input onkeydown="return validarNumerosNegativos(event)" class="form-control required number" type="number" min="0" step=".01" id="txtCota" name="txtCota" placeholder="'.$pPlaceHolder8.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Ruc: </label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtRuc" name="txtRuc" placeholder="'.$pPlaceHolder9.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Tipo de edificio</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        '.$cbTipoEdificio.'
                                        <p class="help-block"></p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Direcci&oacute;n</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtDireccion" name="txtDireccion" placeholder="'.$pPlaceHolder.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Predio</label>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4">
                                                <input class="form-control" id="txtPredio" name="txtPredio" placeholder="Sector">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <input class="form-control" id="txtManzana" name="txtManzana" placeholder="Manzana">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <input class="form-control" id="txtLote" name="txtLote" placeholder="Lote">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <input class="form-control" id="txtDivision" name="txtDivision" placeholder="Division">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <input class="form-control" id="txtPhv" name="txtPhv" placeholder="Phv">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <input class="form-control" id="txtPhh" name="txtPhh" placeholder="Phh">
                                                <p class="help-block with-errors"></p> 
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <input class="form-control" id="txtNumero" name="txtNumero" placeholder="Numero">
                                                <p class="help-block with-errors"></p> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Ancho Posterior (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input onkeydown="return validarNumerosNegativos(event)" class="form-control required number" type="number" min="0" step=".01" id="txtAnchoPosterior" name="txtAnchoPosterior" placeholder="'.$pPlaceHolder6.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Alto Posterior (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input onkeydown="return validarNumerosNegativos(event)" class="form-control required number" type="number" min="0" step=".01" id="txtAltoPosterior" name="txtAltoPosterior" placeholder="'.$pPlaceHolder7.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>C&oacute;digo de obra</label>
                                        <input class="form-control" id="txtCodigoObra" name="txtCodigoObra" placeholder="'.$pPlaceHolder.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Largo (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input onkeydown="return validarNumerosNegativos(event)" type="number" min="0" step=".01" class="form-control required number" id="txtFondo" name="txtFondo" placeholder="'.$pPlaceHolder3.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Contratista</label>
                                        <input class="form-control" id="txtContratista" name="txtContratista" placeholder="'.$pPlaceHolder5.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Tipo de estructura</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        '.$cbTipoEstructura.'
                                        <p class="help-block"></p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="'.$pNombreBtnGuardar.'" onClick="jsGuardar(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                           <button type="button" class="btn btn-primary" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">Limpiar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_edificio, a.direccion, a.predio, a.propietario, a.codigo_obra
            FROM ".$pNombreTabla." a";
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

     $_form.='
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="'.$pNombreDataTable.'">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Id. Registro</th>
                                    <th>Direcci&oacute;n</th>
                                    <th>Predio</th>
                                    <th>Propietario</th>
                                    <th>C&oacute;digo Obra</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>';

   foreach ($_vlv_Resultado as $_data)
    {
        $_form.='           <tr class="odd gradeA">
                                <td class="center">'.$_data["estado"].'</td>
                                <td class="center">'.$_data["id_edificio"].'</td>
                                <td class="center">'.$_data["direccion"].'</td>
                                <td class="center">'.$_data["predio"].'</td>
                                <td class="center">'.$_data["propietario"].'</td>
                                <td class="center">'.$_data["codigo_obra"].'</td>
                            ';
        if($_data["estado"]=='A')
            {
                $_form.='       <td align="center">&nbsp;</td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Registro" onClick="jsAnularActivar('.$_data["id_edificio"].',\''.$_pNombreArchivo.'\')"><i class="fa fa-check"></i></button></td>';

            }
            else
            {
                $_form.='       <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#'.$pNombreModal.'" onClick="jsVerPantallaModificar('.$_data["id_edificio"].',\''.$_pNombreArchivo.'\',\''.$pNombreModal.'\')">Modificar</button></td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Registro" onClick="jsAnularActivar('.$_data["id_edificio"].',\''.$_pNombreArchivo.'\')"><i class="fa fa-times"></i></button></td>';
            }
        $_form.='           </tr>';
    }
    $_form.='               </tbody>
                        </table>
                    </div> <!-- /.panel-body -->
                </div>  <!-- /.panel -->
            </div> <!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <script>
        $(document).ready(function() {
            $("#'.$pNombreDataTable.'").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                },
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 0, 1 ]
                } ]
            });
        });
        </script>
        ';
    $_form.='<div class="modal fade" id="'.$pNombreModal.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             </div>';
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}

function fVerPantallaModificar($_pIdOpcion)
{
    global $_pNombreArchivo, $pNombreTabla,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3;
    $pNombreFrm="frmActualizar";
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $jsondata = array();
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_edificio, a.nombre_edificio, a.propietario, a.ancho, a.alto, a.valor_edificio,
            a.id_tipo_edificio,a.direccion,a.predio, a.codigo_obra, a.fondo, a.contratista, a.id_tipo_estructura,
            a.alto_posterior, a.ancho_posterior, a.cota, a.ruc
            FROM ".$pNombreTabla." a
            where a.id_edificio=".$_pIdOpcion;
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    /*CREAR COMBO TIPO EDIFICIO*/
    $pNombreCombo='cbTipoEdificio';
    $pCamposId='a.id_tipo_edificio';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_edificio a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pRequerido='required';

    /*CREAR COMBO TIPO ESTRUCTURA*/
    $pNombreCombo1='cbTipoEstructura';
    $pCamposId1='a.id_tipo_estructura';
    $pCamposDetalle1='a.detalle';
    $pTabla1='tbl_tipo_estructura a';
    $pInner1='';
    $pWhere1=' where a.estado is null';
    $pOrder1='';
    $pGroupBy1='';
    $pRequerido1='required';

    foreach ($_vlv_Resultado as $_data)
    {
        $pSelected=$_data["id_tipo_edificio"];
        $cbTipoEdificio=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

        $pSelected1=$_data["id_tipo_estructura"];
        $cbTipoEstructura=$_appcomponentes->f_crear_combo($pNombreCombo1,$pCamposId1,$pCamposDetalle1,$pTabla1,$pInner1,$pWhere1,$pOrder1,$pGroupBy1,$pSelected1,$pRequerido1);
        $_form='
        <!-- Modal -->
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">&times;</button>
                        <div id="divMensajeModal"></div>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Modificar Registro - ID: '.$_data["id_edificio"].'
                                </div>
                                <div class="panel-body">
                                <form role="form" id="'.$pNombreFrm.'" >
                                <input type="hidden" id="hddId" name="hddId" value="'.$_data["id_edificio"].'">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input class="form-control required" id="txtNombreEdificio" name="txtNombreEdificio"
                                                placeholder="'.$pPlaceHolder.'" value="'.$_data["nombre_edificio"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Propietario</label>
                                                <input class="form-control" id="txtPropietario" name="txtPropietario"
                                                placeholder="'.$pPlaceHolder1.'" value="'.$_data["propietario"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Ancho Frontal (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" id="txtAncho" name="txtAncho"
                                                placeholder="'.$pPlaceHolder1.'" value="'.$_data["ancho"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Alto Frontal (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" id="txtAlto" name="txtAlto"
                                                placeholder="'.$pPlaceHolder1.'" value="'.$_data["alto"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Valor edificio</label>
                                                <input class="form-control number" id="txtValor" name="txtValor"
                                                placeholder="'.$pPlaceHolder1.'" value="'.$_data["valor_edificio"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Cota (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input onkeydown="return validarNumerosNegativos(event)" 
                                                class="form-control required number" type="number" min="0" step=".01" 
                                                id="txtCota" name="txtCota" placeholder="'.$pPlaceHolder1.'"
                                                value="'.$_data["cota"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Ruc: </label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required" 
                                                id="txtRuc" name="txtRuc" placeholder="'.$pPlaceHolder1.'"
                                                value="'.$_data["ruc"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Tipo de edificio</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                '.$cbTipoEdificio.'
                                                <p class="help-block"></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Direcci&oacute;n</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required" id="txtDireccion" name="txtDireccion"
                                                placeholder="'.$pPlaceHolder.'" value="'.$_data["direccion"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Predio</label>
                                                <input class="form-control" id="txtPredio" name="txtPredio"
                                                placeholder="'.$pPlaceHolder2.'" value="'.$_data["predio"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Ancho Posterior (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input onkeydown="return validarNumerosNegativos(event)" 
                                                class="form-control required number" type="number" min="0" step=".01" 
                                                id="txtAnchoPosterior" name="txtAnchoPosterior" placeholder="'.$pPlaceHolder1.'"
                                                value="'.$_data["ancho_posterior"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Alto Posterior (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input onkeydown="return validarNumerosNegativos(event)" 
                                                class="form-control required number" type="number" min="0" step=".01" 
                                                id="txtAltoPosterior" name="txtAltoPosterior" placeholder="'.$pPlaceHolder1.'"
                                                value="'.$_data["alto_posterior"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>C&oacute;digo de obra</label>
                                                <input class="form-control" id="txtCodigoObra" name="txtCodigoObra"
                                                placeholder="'.$pPlaceHolder3.'" value="'.$_data["codigo_obra"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Fondo (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" id="txtFondo" name="txtFondo"
                                                placeholder="'.$pPlaceHolder1.'" value="'.$_data["fondo"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Contratista</label>
                                                <input class="form-control" id="txtContratista" name="txtContratista"
                                                placeholder="'.$pPlaceHolder1.'" value="'.$_data["contratista"].'">
                                                <p class="help-block with-errors"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Tipo de estructura</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                '.$cbTipoEstructura.'
                                                <p class="help-block"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </form>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div><!-- /.modal-body -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onClick="jsActualizar(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        <!-- /.modal -->';
    }

    $array = array(0 => $_form);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}
function fAnularActivar($_pIdOpcion)
{
    global $_pUsuario, $_pIp, $p_funcionAnulaActiva;
    $_operacionesbd=new operacionesbd();

    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdOpcion;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcionAnulaActiva,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>
