<?php
class passoperations
{
    // Opciones de contraseña:
    const HASH = PASSWORD_DEFAULT;
    const COST = 12;
    public static function f_hash($password)
    {
        return password_hash($password, self::HASH, ['cost' => self::COST]);
    }
    public static function f_verify($password, $hash)
    {
        return password_verify($password, $hash);
    }

}


?>