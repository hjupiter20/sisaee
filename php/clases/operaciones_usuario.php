<?php
class operaciones_usuario extends operacionesbd
{
    // Opciones de contraseña:
    const HASH = PASSWORD_DEFAULT;
    const COST = 12;
    // Permite el cambio de contraseña:
    public function f_setPassword($_pPassword,$_pUsuario)
    {
         $pIp   = getenv("HTTP_X_FORWARDED_FOR");
         $p_funcion="SP_ACTUALIZAR_PGHASH";
         $vlvPgHash=password_hash($_pPassword, self::HASH, ['cost' => self::COST]);
         $p_parametros="'".$_pUsuario."','".$vlvPgHash."','".$pIp."'";
         $vlvResultado=$this->f_EjecutaFuncion($p_funcion,$p_parametros);
         return $vlvResultado;
    }
    public function f_cambiarPasswd($_pPassword,$_pUsuario,$_pIdCodigo)
    {
        $pIp   = getenv("HTTP_X_FORWARDED_FOR");
        $p_funcion="SP_SHADOW";
        $vlvPgHash=password_hash($_pPassword, self::HASH, ['cost' => self::COST]);
        $p_parametros="'".$vlvPgHash."',".$_pIdCodigo.",'".$_pUsuario."','".$pIp."'";
        $vlvResultado=$this->f_EjecutaFuncion($p_funcion,$p_parametros);
        return $vlvResultado;
    }
    public function f_valida_primer_ingreso($pUsuario, $pPassword)
    {
        // Primero comprobamos si se ha empleado una contraseña correcta:
        $pTabla=' tbl_usuarios a';
        $pCampos=' a.usuario_nuevo, a.pghash, a.usuario, a.id_codigo ';
        $pInner='';
        $pWhere=" where a.usuario='".$pUsuario."'";
        $pOrder='';
        $pGroupBy='';

        $rs=$this->f_EjecutaQueryParametros($pTabla, $pCampos, $pInner, $pWhere, $pOrder, $pGroupBy);
        $numrows=$this->f_GetNumRows();
        if($numrows>0)
        {
            foreach ($rs as $_data)
            {
                $vgvPgHash=$_data["pghash"];
                $_vgvUsuarioNuevo=$_data["usuario_nuevo"];
                $vlvUsuario=$_data["usuario"];
                $_vlnIdCodigo=$_data["id_codigo"];
            }
            if($_vgvUsuarioNuevo=='1')
            {
                return '1';//no es usuario nuevo
            }
            else
            {
                if (password_verify($pPassword, $vgvPgHash))
                {
                    $_SESSION["vgvUsuario"]=$vlvUsuario;
                    $_SESSION["vgvIdCodigo"]=$_vlnIdCodigo;
                    return '6';//es usuario nuevo
                }
            }
        }
        else
            return '5';//usuario no existe
    }
    public function f_valida_password($pUsuario, $pPassword)
    {
        // Primero comprobamos si se ha empleado una contraseña correcta:
        $pTabla=' tbl_usuarios a';
        $pCampos=' a.usuario_nuevo, a.pghash, a.usuario ';
        $pInner='';
        $pWhere=" where a.usuario='".$pUsuario."'";
        $pOrder='';
        $pGroupBy='';

        $rs=$this->f_EjecutaQueryParametros($pTabla, $pCampos, $pInner, $pWhere, $pOrder, $pGroupBy);
        $numrows=$this->f_GetNumRows();
        if($numrows>0)
        {
            foreach ($rs as $_data)
            {
                $vgvPgHash=$_data["pghash"];
            }
            if (password_verify($pPassword, $vgvPgHash))
            {
                return true;
            }
        }
        else
            return false;
    }
    public function f_valida_historia_passwd($pPassword, $pIdCodigo)
    {
        $p_SQL="SELECT a.historia_password
                FROM tbl_parametros_seguridad a
                where a.estado IS NULL";
        $_rs=$this->f_EjecutaQuery($p_SQL);
        $numrows=0;
        foreach ($_rs as $_data)
        {
            $_pHistoriaPasswd=$_data["historia_password"];
            //$_pLongitudMaxPasswd=$_data["longitud_max_password"];
        }
        for($i=1;$i<=$_pHistoriaPasswd;$i++)
        {
            $pSQL1="select a.clave, a.num_password
                    from tbl_shadow a
                    where  a.id_codigo=".$pIdCodigo." and a.num_password=".$i." and
                    a.clave='".$pPassword."'";
            $rs1=$this->f_EjecutaQuery($pSQL1);
            $numrows=$this->f_GetNumRows();
            if($numrows>0)
            {
                return true;
            }
        }
        return false;
    }
    // Logear un usuario:
    public function f_login($pPassword,$pUsuario)
    {
        // Primero comprobamos si se ha empleado una contraseña correcta:
        $numrows1=0;
        $pTabla=' tbl_usuarios a';
        $pCampos=' a.pghash, a.usuario, a.id_perfil_usuario, a.id_codigo,  a.num_intentos_bloqueo_cuenta, a.bloqueado';
        $pInner='';
        $pWhere=" where a.usuario='".$pUsuario."'";
        $pOrder='';
        $pGroupBy='';

        $rs=$this->f_EjecutaQueryParametros($pTabla, $pCampos, $pInner, $pWhere, $pOrder, $pGroupBy);
        $numrows=$this->f_GetNumRows();
        if($numrows>0)
        {
            foreach ($rs as $_data)
            {
                $vgvPgHash=$_data["pghash"];
                $vgnNumIntBloqueoUsuario=$_data["num_intentos_bloqueo_cuenta"];
                $vgvBloqueado=$_data["bloqueado"];
            }
            if($vgvBloqueado=='2')//usuario bloqueado
            {
                return '4';//contrasena bloqueada
            }
            else//usuario sin bloqueo
            {
                $pSQL1="select count(a.usuario)
                        from tbl_usuarios a
                        where a.usuario='".$pUsuario."' and CURRENT_DATE<=a.fecha_prox_cambio_pass";
                $rs1=$this->f_EjecutaQuery($pSQL1);
                foreach ($rs1 as $data1)
                {
                    $numrows1=$data1["count"];
                }
                if($numrows1>0)//verifica si la clave esta caducada
                {
                    if (password_verify($pPassword, $vgvPgHash))//verifica si la contrasena es correcta
                    {
                        // Exito, ahora se comprueba si la contraseña necesita un rehash:
                        if (password_needs_rehash($vgvPgHash, self::HASH, ['cost' => self::COST]))
                        {
                            $rehash=$this->f_setPassword($pPassword,$pUsuario);
                        }
                        $rs2=$this->f_EjecutaQueryParametros($pTabla, $pCampos, $pInner, $pWhere, $pOrder, $pGroupBy);
                        $numrows2=$this->f_GetNumRows();
                        if($numrows2>0)
                        {
                            foreach ($rs2 as $data2)
                            {
                                $_SESSION["vgvUsuario"]=$data2["usuario"];
                                $_SESSION["vgvIdCodigo"]=$data2["id_codigo"];
                                $_SESSION["vgnIdPerfilUsuario"]=$data2["id_perfil_usuario"];
                                
                            }
                        }
                        return '1'; // contrasena correcta
                    }
                    else
                    {
                        $pSQL3="select a.num_intentos_bloqueo_cuenta
                                FROM tbl_parametros_seguridad a
                                where a.estado IS NULL";
                        $rs3=$this->f_EjecutaQuery($pSQL3);
                        $numrows3=$this->f_GetNumRows();
                        if($numrows3>0)
                        {
                            foreach ($rs3 as $_data3)
                            {
                                $_pNoIntBloqueoParametro=$_data3["num_intentos_bloqueo_cuenta"];
                            }
                        }
                        if($vgnNumIntBloqueoUsuario==$_pNoIntBloqueoParametro)
                        {
                            $pSQL4="update
                                      tbl_usuarios
                                     set
                                      bloqueado='2'
                                     where usuario='".$pUsuario."'";
                            $rs4=$this->f_EjecutaQuery($pSQL4);
                            return '4';//contrasena bloqueada
                        }
                        else
                        {
                            $pSQL4="update
                                      tbl_usuarios
                                     set
                                      num_intentos_bloqueo_cuenta=num_intentos_bloqueo_cuenta+1
                                     where usuario='".$pUsuario."'";
                            $rs4=$this->f_EjecutaQuery($pSQL4);
                            return '2';//contrasena incorrecta
                        }
                    }
                }
                else
                {
                    $_SESSION["vgvUsuario"]=$pUsuario;
                    $_SESSION["vgvIdCodigo"]=$_data["id_codigo"];
                    return '3';//contrasena caducada
                }
            }
        }
        return '5';//el usuario no existe
    }
}


?>