<?php
/**
* clase base de datos
*/
include_once(INCLUDES."/config.php");
class dbfunctions
{
    public static $conn = null;
    public function __construct(){}

    function db_connect() {
        global $connstr;

    $conn=pg_connect($connstr);
    if(!$conn)
        trigger_error("Unable to connect",E_USER_ERROR);
    return $conn;
    }

    function db_close($conn)
    {
        return pg_close($conn);
    }

    function db_query($qstring,$conn) {
        if(version_compare(phpversion(),"4.2.0")>=0)
            $ret=pg_query($conn,$qstring);
        else
            $ret=pg_exec($conn,$qstring);
        if(!$ret)
        {
          trigger_error($this->db_error(), E_USER_ERROR);
        }
        return $ret;

    }

    function db_exec($qstring,$conn)
    {
        return db_query($qstring,$conn);
    }

    function db_numrows($qhandle) {
    // return only if qhandle exists, otherwise 0
        if ($qhandle) {
            return @pg_num_rows($qhandle);
        } else {
            return 0;
        }
    }

    function db_fetch_array($qhandle) {
        $ret=pg_fetch_array($qhandle);
    //	remove numeric indexes
        if(!$ret)
            return false;
        foreach($ret as $key=>$value)
            if(is_int($key))
                unset($ret[$key]);
        return $ret;
    }

    function db_fetch_array_num($qhandle) {
        $ret=pg_fetch_array($qhandle);
    //	remove numeric indexes
        return $ret;
    }

    function db_fetch_numarray($qhandle) {
        return @pg_fetch_row($qhandle);
    }
    function f_SetNumRows($p_numrows)
    {
        $this->vgn_NumRows=$p_numrows;
    }

    function f_GetNumRows()
    {
        return $this->vgn_NumRows;
    }

    function db_error() {
    global $conn;
    if(version_compare(phpversion(),"4.2.0")>=0)
        return @pg_last_error($conn);
    else
        return "PostgreSQL error happened";

}
}