<?php
Class appcomponentes  extends operacionesbd
{
    public function __construct(){}
    function f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected, $pRequerido)//crea un conmbo sin la consulta
    {
        $rs=array();
        $pCampos=$pCamposId.",".$pCamposDetalle;
        $rs=$this->f_EjecutaQueryParametros($pTabla, $pCampos, $pInner, $pWhere, $pOrder, $pGroupBy);
        $numrows=$this->f_GetNumRows();
        $vloCombo='<select class="form-control '.$pRequerido.'" id="'.$pNombreCombo.'" name="'.$pNombreCombo.'">';
        $vloCombo.='<option value="">Seleccione</option>';
        
        IF($numrows)
        {
            foreach($rs as $data)
            {
                $vlnId          =$data[substr($pCamposId,2)];
                $vlvDetalle     =$data[substr($pCamposDetalle,2)];                
                if($pSelected==$vlnId)
                    $vloCombo.='<option value="'.$vlnId.'" selected>'.$vlvDetalle.'</option>';
                else
                    $vloCombo.='<option value="'.$vlnId.'">'.$vlvDetalle.'</option>';
                //$vloCombo=$vlnId;
            }
        }
        $vloCombo.="</select>";
        return $vloCombo;
    }
    function f_crear_combo_bloqueado($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected, $pRequerido)//crea un conmbo sin la consulta
    {
        $rs=array();
        $pCampos=$pCamposId.",".$pCamposDetalle;
        $rs=$this->f_EjecutaQueryParametros($pTabla, $pCampos, $pInner, $pWhere, $pOrder, $pGroupBy);
        $numrows=$this->f_GetNumRows();
        $vloCombo='<select class="form-control '.$pRequerido.'" id="'.$pNombreCombo.'" name="'.$pNombreCombo.'" disabled>';
        $vloCombo.='<option value="">Seleccione</option>';
        IF($numrows)
        {
            foreach($rs as $data)
            {
                $vlnId          =$data[substr($pCamposId,2)];
                $vlvDetalle     =$data[substr($pCamposDetalle,2)];
                if($pSelected==$vlnId)
                    $vloCombo.='<option value="'.$vlnId.'" selected>'.$vlvDetalle.'</option>';
                else
                    $vloCombo.='<option value="'.$vlnId.'">'.$vlvDetalle.'</option>';
                //$vloCombo=$vlnId;
            }
        }
        $vloCombo.="</select>";
        return $vloCombo;
    }
    function f_crear_combo_change($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected, $pRequerido, $pOnChange)//crea un conmbo sin la consulta
    {
        $rs=array();
        $pCampos=$pCamposId.",".$pCamposDetalle;
        $rs=$this->f_EjecutaQueryParametros($pTabla, $pCampos, $pInner, $pWhere, $pOrder, $pGroupBy);
        $numrows=$this->f_GetNumRows();
        $vloCombo='<select class="form-control '.$pRequerido.'" id="'.$pNombreCombo.'" name="'.$pNombreCombo.'" onChange="'.$pOnChange.'">';

        $vloCombo.='<option value="">Seleccione</option>';
        IF($numrows)
        {
            foreach($rs as $data)
            {
                $vlnId          =$data[substr($pCamposId,2)];
                $vlvDetalle     =$data[substr($pCamposDetalle,2)];
                if($pSelected==$vlnId)
                    $vloCombo.='<option value="'.$vlnId.'" selected>'.$vlvDetalle.'</option>';
                else
                    $vloCombo.='<option value="'.$vlnId.'">'.$vlvDetalle.'</option>';
                //$vloCombo=$vlnId;
            }
        }
        $vloCombo.="</select>";
        return $vloCombo;
    }
    function f_ObtieneComboAnios($pNombreCombo,$pAnioDesde, $pSelected,$pRequerido)
    {
        //$vln_elementos = count($p_arreglo);
        $vloCombo="";
        $vAnioActual=date('Y',strtotime(now()));
        $vloCombo='<select class="form-control '.$pRequerido.'" id="'.$pNombreCombo.'" name="'.$pNombreCombo.'">';
        $vloCombo.='<option value="">Seleccione</option>';
        for ($vln_registros = $pAnioDesde; $vln_registros <= $vAnioActual; $vln_registros++)
        {
            $vls_CampoClave=$vln_registros;
            $vls_CampoDescripcion=$vln_registros;
            $vls_CampoSelected=$pSelected;

            if($vls_CampoSelected==true)
                $vloCombo.="<option value='".$vls_CampoClave."' selected='selected'>".$vls_CampoDescripcion."</option>";
            else
                $vloCombo.="<option value='".$vls_CampoClave."'>".$vls_CampoDescripcion."</option>";
        }
        $vloCombo.="</select>";
        return $vloCombo;
    }
    function f_ComboUsuarioBloqueado($pNombreCombo, $pSelected, $pRequerido)
    {
        //$vln_elementos = count($p_arreglo);
        $vloCombo="";
        $vloCombo='<select class="form-control '.$pRequerido.'" id="'.$pNombreCombo.'" name="'.$pNombreCombo.'">';
        $vloCombo.='<option value="">Seleccione</option>';
        switch($pSelected)
        {
            case '1':
                $vloCombo.="<option value='1' selected='selected'>Activo</option>";
                $vloCombo.="<option value='2'>Bloqueado</option>";
            break;
            case '2':
                $vloCombo.="<option value='1'>Activo</option>";
                $vloCombo.="<option value='2' selected='selected'>Bloqueado</option>";
            break;
            default:
                $vloCombo.="<option value='1'>Activo</option>";
                $vloCombo.="<option value='2'>Bloqueado</option>";
            break;
        }

        $vloCombo.="</select>";
        return $vloCombo;
    }
    function f_buscar_alertas($pIdUnidadOrganizacional)
    {
        $p_SQL='SELECT  count(a.id_gestion_documentos) as recibidos
            FROM tbl_gestion_documentos a
            inner join tbl_carrera b on a.id_carrera=b.id_carrera
            inner join tbl_unidad_organizativa c on a.prefijo=c.prefijo
            inner join tbl_periodos_academicos d on a.id_periodo_academico=d.id_periodo_academico
            inner join tbl_tipo_documento e on a.prefijo_doc=e.prefijo_doc
            where a.enviar_a='.$pIdUnidadOrganizacional.' and a.leido is NULL';
        $rs=$this->f_EjecutaQuery($p_SQL);
        foreach($rs as $data)
        {
            $vlnRecibidos          =$data["recibidos"];
        }
        return $vlnRecibidos;

    }
    function f_cargar_menu($pIdPerfilUsuario)
    {
        $pTabla=' tbl_acceso_sistema a';
        $pCampos=' DISTINCT a.id_menu_sistema, b.nombre_menu, b.nombre_icono ';
        $pInner=' inner join tbl_menu_sistema b on a.id_menu_sistema=b.id_menu_sistema';
        $pWhere=' where a.id_perfil_usuario='.$pIdPerfilUsuario;
        $pOrder=' order by a.id_menu_sistema';
        $pGroupBy='';

        $rs=$this->f_EjecutaQueryParametros($pTabla, $pCampos, $pInner, $pWhere, $pOrder, $pGroupBy);
        $numrows=$this->f_GetNumRows();
        $vlvMenu='';

        IF($numrows)
        {
            $vlvMenu='<div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                            </div>
                                <!-- /input-group -->
                        </li>
                        <li>
                                <a href="../php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>';
            foreach($rs as $data)
            {
                //$vlnId          =$data["id_menu_sistema"];
                $vlvMenu.=' <li>
                                <a href="#"><i class="fa '.$data["nombre_icono"].' fa-fw"></i> '.$data["nombre_menu"].'<span class="fa arrow"></span></a>
                            ';
                $pTabla1=' tbl_acceso_sistema a';
                $pCampos1=' a.id_menu_sistema, a.id_opcion_sistema, b.nombre_opcion, b.alias_opcion, b.nombre_archivo, c.accion ';
                $pInner1=' LEFT OUTER join tbl_opcion_sistema b on a.id_opcion_sistema=b.id_opcion_sistema
                            INNER JOIN tbl_acciones c on b.id_acciones=c.id_acciones';
                $pWhere1=' where a.estado is null and a.id_perfil_usuario='.$pIdPerfilUsuario.' and a.id_menu_sistema='.$data["id_menu_sistema"];
                $pOrder1=' order by a.id_menu_sistema, a.id_opcion_sistema';
                $pGroupBy1='';

                $rs1=$this->f_EjecutaQueryParametros($pTabla1, $pCampos1, $pInner1, $pWhere1, $pOrder1, $pGroupBy1);
                $numrows1=$this->f_GetNumRows();
                IF($numrows)
                {
                    $vlvMenu.='<ul class="nav nav-second-level">';
                    foreach($rs1 as $data1)
                    {
                        $vlvMenu.=' <li>
                                        <a href="#" id="'.$data1["alias_opcion"].'" onClick="jsOpcionesMenu(\''.$data1["nombre_archivo"].'\',\''.$data1["accion"].'\');">'.$data1["nombre_opcion"].'</a>
                                    </li>';
                    }
                    $vlvMenu.='</ul>';
                }

            }
            $vlvMenu.='     </li>
                        </ul>
                    </div>';
        }
        return $vlvMenu;

    }
}


?>