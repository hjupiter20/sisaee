<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
$vAnioActual=date('Y',strtotime(now()));
setcookie("pAnio", $vAnioActual, time()+1800);
setcookie("pCarrera", "1", time()+1800);
setcookie("pUnidadOrganizativa", "d", time()+1800);
setcookie("pPeriodo", "d", time()+1800);
setcookie("pMes", "d", time()+1800);
setcookie("pResumenDoc", "d", time()+1800);
setcookie("pTipoDoc", "d", time()+1800);

/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIdUnidadOrganizativa=$_SESSION["vgnIdUnidadOrganizativa"];
$_pIp=Recuperaip();
$_pAccion               =$_POST["pAccion"];
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        fCargarFormulario();
    break;
    case 'LISTA':
        //$_pTipoLista    =$_POST['pTipoLista'];
        fGeneraLista();
    break;
    case 'LISTA_RECIBIDOS':
        //$_pTipoLista    =$_POST['pTipoLista'];
        fGeneraListaRecibidos();
    break;
    case 'VER_PDF':
        $pAnio    =$_POST['pAnio'];
        $pNombreArchivo    =$_POST['pNombreArchivo'];
        $pIdDocumento    =$_POST['pIdDocumento'];
        fVerPDF($pAnio,$pNombreArchivo,$pIdDocumento);
    break;
    case 'ENVIAR_PDF':
        $_pEnviar_a    =$_POST['pEnviado_por'];
        $_pIdDocumento    =$_POST['pIdDocumento'];
        fEnviarPDF($_pEnviar_a,$_pIdDocumento);
    break;
    case 'ANULA_ACTIVA':
        $_pIdCodigo           =$_POST['pIdCodigo'];
        fAnularDocumento($_pIdCodigo);
    break;
}

function fEnviarPDF($_pEnviar_a,$_pIdDocumento)
{
    global $_pUsuario, $_pIp, $_pIdUnidadOrganizativa;
    $_pEnviado_por=$_pIdUnidadOrganizativa;
    $_dboperations=new dboperations();
    $p_funcion="SP_ENVIAR_DOCUMENTO";
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdDocumento.",".$_pEnviado_por.",".$_pEnviar_a;
    $vlvResultado=explode("|",$_dboperations->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);


}

function fCargarFormulario()
{
    $_appcomponentes=new appcomponentes();
    $_dboperations=new dboperations();

    /*CREAR COMBO CARRERAS*/
    $pNombreCombo='cbCarreras';
    $pCamposId='a.id_carrera';
    $pCamposDetalle='a.nombre_carrera';
    $pTabla='tbl_carrera a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder=' order by a.nombre_carrera';
    $pGroupBy='';
    $pSelected='NULL';
    $pRequerido='required';
    $cbCarrera=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    /*CREAR COMBO UNIDAD ORGANIZATIVA*/
    $pNombreCombo='cbUnidadOrganizativa';
    $pCamposId='a.prefijo';
    $pCamposDetalle='a.nombre_unidad_organizativa';
    $pTabla='tbl_unidad_organizativa a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder=' order by a.nombre_unidad_organizativa';
    $pGroupBy='';
    $pSelected='NULL';
    $pRequerido='required';
    $cbUnidadOrganizativa=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    /*CREAR COMBO PERIODOS*/
    $pNombreCombo='cbPeriodos';
    $pCamposId='a.id_periodo_academico';
    $pCamposDetalle='a.descripcion';
    $pTabla='tbl_periodos_academicos a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder=' order by a.descripcion';
    $pGroupBy='';
    $pSelected='NULL';
    $pRequerido='required';
    $cbPeriodos=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    /*CREAR COMBO MESES*/
    $pNombreCombo='cbMeses';
    $pCamposId='a.id_meses';
    $pCamposDetalle='a.descripcion';
    $pTabla='tbl_meses a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder=' order by a.id_meses';
    $pGroupBy='';
    //$pSelected='NULL';
    $pSelected=date('m',strtotime(now()));
    $pRequerido='required';
    $cbMeses=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    /*CREAR TIPO DOCUMENTOS*/
    $pNombreCombo='cbTipoDocumento';
    $pCamposId='a.prefijo_doc';
    $pCamposDetalle='a.descripcion';
    $pTabla='tbl_tipo_documento a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder=' order by a.id_tipo_documento';
    $pGroupBy='';
    $pSelected='NULL';
    $pRequerido='required';
    $cbTipoDocumento=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    $pNombreCombo='cbAnios';
    $pAnioDesde='2010';
    $pSelected='NULL';
    $pRequerido='required';
    $cbAnios=$_appcomponentes->f_ObtieneComboAnios($pNombreCombo,$pAnioDesde,$pSelected,$pRequerido);

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Asignar Opciones a Perfiles
                        </div>
                        <div class="panel-body">
                            <form role="form" data-toggle="validator" id="frmGestionDocumentos" >
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Carrera</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbCarrera.'
                                            <p class="help-block">Seleccione una Carrera.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Unidad Organizativa</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbUnidadOrganizativa.'
                                            <p class="help-block">Seleccione una Unidad Organizativa.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>A&ntilde;o</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbAnios.'
                                            <p class="help-block">Seleccione el a&ntilde;o.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Periodo</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbPeriodos.'
                                            <p class="help-block">Seleccione el periodo.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Mes</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbMeses.'
                                            <p class="help-block">Seleccione el mes.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Tipo Documento</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbTipoDocumento.'
                                            <p class="help-block">Seleccione el tipo del documento.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Seleccione Archivo</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            <input type="file"  id="fileToUpload" name="fileToUpload">
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Res&uacute;men de Archivo</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            <textarea class="form-control required" rows="1" id="txResumenDoc" name="txResumenDoc"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </form>
                            </div>
                                <!-- /.panel-body -->
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary" id="btnGuardarOpcionSistema" onClick="js_upload();">Guardar</button>
                                    <button type="button" class="btn btn-primary">Limpiar</button>
                                </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->';
    //$_form="ENTRO";
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}
function fGeneraLista()
{
    global $_pIdUnidadOrganizativa;
    $_appcomponentes=new appcomponentes();
    $vlnAlertas=$_appcomponentes->f_buscar_alertas($_pIdUnidadOrganizativa);
    $jsondata = array();
    $vlnNum=0;
    $_form='
    <div class="row">
        <div class="col-lg-12">
            <div id="divMensaje">&nbsp;</div>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Consulta Documentos </div>
                    <div class="panel-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#ingresados" data-toggle="tab">Ingresados</a>
                            </li>
                            <li><a href="#recibidos" data-toggle="tab">Recibidos</a>
                            </li>
                            <li><a href="#enviados" data-toggle="tab">Enviados</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="ingresados">
                                <p></p>
                                <div class="table-responsive">
                                '.fListaIngresados().'
                                </div>
                            </div>
                            <div class="tab-pane fade" id="recibidos">
                                <p></p>
                                <div class="table-responsive">
                                '.fListaDocRecibidos().'
                                </div>
                            </div>
                            <div class="tab-pane fade" id="enviados">
                                <p></p>
                                <div class="table-responsive">
                                '.fListaDocEnviados().'
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                 <!-- /.panel header -->
            </div>
             <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>';

    $array = array(0 => $_form, 1=>$vlnAlertas);

    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}
function fGeneraListaRecibidos()
{
    global $_pIdUnidadOrganizativa;
    $_appcomponentes=new appcomponentes();
    $vlnAlertas=$_appcomponentes->f_buscar_alertas($_pIdUnidadOrganizativa);
    $jsondata = array();
    $vlnNum=0;
    $_form='
    <div class="row">
        <div class="col-lg-12">
            <div id="divMensaje">&nbsp;</div>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">Consulta Documentos </div>
                    <div class="panel-body">
                        <ul class="nav nav-tabs">
                            <li><a href="#ingresados" data-toggle="tab">Ingresados</a>
                            </li>
                            <li class="active"><a href="#recibidos" data-toggle="tab">Recibidos</a>
                            </li>
                            <li><a href="#enviados" data-toggle="tab">Enviados</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade" id="ingresados">
                                <p></p>
                                '.fListaIngresados().'
                            </div>
                            <div class="tab-pane fade in active" id="recibidos">
                                <p></p>
                                '.fListaDocRecibidos().'
                            </div>
                            <div class="tab-pane fade" id="enviados">
                                <p></p>
                                '.fListaDocEnviados().'
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                 <!-- /.panel header -->
            </div>
             <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>';

    $array = array(0 => $_form, 1=>$vlnAlertas);

    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}
function fListaIngresados()
{
    global $_pUsuario, $_pIdUnidadOrganizativa;
    $_dboperations=new dboperations();
    $_appcomponentes=new appcomponentes();
    $i=0;

    /*CREAR COMBO UNIDAD ORGANIZATIVA*/

    $pCamposId='a.id_unidad_organizativa';
    $pCamposDetalle='a.nombre_unidad_organizativa';
    $pTabla='tbl_unidad_organizativa a';
    $pInner='';
    $pWhere=' where a.estado is null and a.id_unidad_organizativa<>'.$_pIdUnidadOrganizativa;
    $pOrder=' order by a.nombre_unidad_organizativa';
    $pGroupBy='';
    $pSelected='NULL';
    $pRequerido='required';


    $p_SQL="SELECT  a.estado, a.id_gestion_documentos, a.anio, a.mes, a.id_carrera, b.nombre_carrera, a.prefijo, c.nombre_unidad_organizativa,
            a.id_periodo_academico, d.descripcion as parcial, a.secuencial, a.resumen_documento, a.nombre_archivo, a.fecha_ingreso,
            upper(e.descripcion) as tipo_doc, a.nombre_archivo
            FROM tbl_gestion_documentos a
            inner join tbl_carrera b on a.id_carrera=b.id_carrera
            inner join tbl_unidad_organizativa c on a.prefijo=c.prefijo
            inner join tbl_periodos_academicos d on a.id_periodo_academico=d.id_periodo_academico
            inner join tbl_tipo_documento e on a.prefijo_doc=e.prefijo_doc
            where a.id_unidad_organizacional='".$_pIdUnidadOrganizativa."' and a.enviar_a is null
            order by a.fecha_ingreso DESC";
    $_vlv_Resultado=$_dboperations->f_EjecutaQuery($p_SQL);
    $vlnNum=$_dboperations->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_dboperations->f_EjecutaQuery($p_SQL);
    $_form='
    <table width="100%" class="table table-striped table-bordered table-hover" id="ver_documentos">
        <thead>
            <tr>
                <th>A&ntilde;o</th>
                <th>Mes</th>
                <th>Tipo doc.</th>
                <th>U. Organiza.</th>
                <th>Nombre Archivo</th>
                <th>Resumen</th>
                <th>Fec. Ing.</th>
                <th><i class="fa fa-search"></i></th>
                <th>Enviar Doc <i class="fa fa-paper-plane"></i></th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>';
    foreach ($_vlv_Resultado as $_data)
    {
        $i=$i+1;
        $pNombreCombo='cbUnidadOrganizativa'.$i;
        $cbUnidadOrganizativa=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);
        $pRutaArchivo = '../file_storage/'.$_data["anio"]."/".$_data["nombre_archivo"].".pdf";
        if($_data["estado"]=='A')
        {
            $_form.='
                <tr class="odd gradeA">
                    <td class="center text-danger">'.$_data["anio"].'</td>
                    <td class="center text-danger">'.$_data["mes"].'</td>
                    <td class="center text-danger">'.$_data["tipo_doc"].'</td>
                    <td class="center text-danger">'.$_data["nombre_unidad_organizativa"].'</td>
                    <td class="center text-danger">'.$_data["nombre_archivo"].'</td>
                    <td class="center text-danger">'.$_data["resumen_documento"].'</td>
                    <td class="center text-danger">';
            IF($_data["fecha_ingreso"])
                $_form.=date('d/m/Y',strtotime($_data["fecha_ingreso"]));
            ELSE
               $_form.='';

            $_form.='
                    </td>
                    <td align="center text-danger">
                        &nbsp;
                    </td>
                    <td align="center text-danger">
                       &nbsp;
                    </td>';
            $_form.='<td align="center text-danger">&nbsp;</td>';
            $_form.='</tr>';
        }
        else
        {
            $_form.='
                <tr class="odd gradeA">
                    <td class="center">'.$_data["anio"].'</td>
                    <td class="center">'.$_data["mes"].'</td>
                    <td class="center">'.$_data["tipo_doc"].'</td>
                    <td class="center">'.$_data["nombre_unidad_organizativa"].'</td>
                    <td class="center">'.$_data["nombre_archivo"].'</td>
                    <td class="center">'.$_data["resumen_documento"].'</td>
                    <td class="center">';
            IF($_data["fecha_ingreso"])
                $_form.=date('d/m/Y',strtotime($_data["fecha_ingreso"]));
            ELSE
               $_form.='';

            $_form.='
                    </td>
                    <td align="center">
                        <button type="button" class="btn btn-outline btn-success" onClick="mostrar_pdf(\''.$pRutaArchivo.'\');" title="Mostrar documento PDF">
                            <i class="fa fa-search"></i>
                        </button>
                    </td>
                    <td align="center">
                        '.$cbUnidadOrganizativa.'
                        <button type="button" class="btn btn-outline btn-success"
                            onClick="jsEnviarPDF('.$i.',\''.$_data["id_gestion_documentos"].'\');" title="Enviar documento a">
                            <i class="fa fa-paper-plane"></i>
                        </button>
                    </td>';
            $_form.='<td align="center"><button type="button" class="btn btn-danger btn-circle" data-placement="top" title="Anular Documento" onClick="jsAnularActivar('.$_data["id_gestion_documentos"].',\'gestion_documentos.php\')"><i class="fa fa-times"></i></button></td>';
            $_form.='</tr>';
        }
    }
    $_form.='
        </tbody>
    </table>';
    $_form.='
    <script>
        $(document).ready(function() {
            $("#ver_documentos").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
        });
    </script>';
    return $_form;
}

function fListaDocRecibidos()
{
    global $_pUsuario, $_pIdUnidadOrganizativa;
    $_dboperations=new dboperations();

    $p_SQL="SELECT  a.id_gestion_documentos, a.anio, a.mes, a.id_carrera, b.nombre_carrera, a.prefijo, c.nombre_unidad_organizativa,
            a.id_periodo_academico, d.descripcion as parcial, a.secuencial, a.resumen_documento, a.nombre_archivo, a.fecha_ingreso,
            upper(e.descripcion) as tipo_doc, a.leido
            FROM tbl_gestion_documentos a
            inner join tbl_carrera b on a.id_carrera=b.id_carrera
            inner join tbl_unidad_organizativa c on a.prefijo=c.prefijo
            inner join tbl_periodos_academicos d on a.id_periodo_academico=d.id_periodo_academico
            inner join tbl_tipo_documento e on a.prefijo_doc=e.prefijo_doc
            where a.enviar_a='".$_pIdUnidadOrganizativa."' and a.estado is null
            order by a.fecha_ingreso DESC";
    $_vlv_Resultado=$_dboperations->f_EjecutaQuery($p_SQL);
    $vlnNum=$_dboperations->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_dboperations->f_EjecutaQuery($p_SQL);
    $_form='
    <table width="100%" class="table table-striped table-bordered table-hover" id="doc_recibidos">
        <thead>
            <tr>
                <th>A&ntilde;o</th>
                <th>Mes</th>
                <th>Carrera</th>
                <th>Tipo doc.</th>
                <th>U. Organiza.</th>
                <th>Nombre Archivo</th>
                <th>Secuencia</th>
                <th>Resumen</th>
                <th>Fec. Ing.</th>
                <th><i class="fa fa-search"></i></th>
            </tr>
        </thead>
        <tbody>';
    foreach ($_vlv_Resultado as $_data)
    {
        if($_data["leido"]=='1')
        {
            $_form.='
                <tr class="odd gradeA">
                    <td class="center">'.$_data["anio"].'</td>
                    <td class="center">'.$_data["mes"].'</td>
                    <td class="center">'.$_data["nombre_carrera"].'</td>
                    <td class="center">'.$_data["tipo_doc"].'</td>
                    <td class="center">'.$_data["nombre_unidad_organizativa"].'</td>
                    <td class="center">'.$_data["nombre_archivo"].'</td>
                    <td class="center">'.$_data["secuencial"].'</td>
                    <td class="center">'.$_data["resumen_documento"].'</td>
                    <td class="center">';
            IF($_data["fecha_ingreso"])
                $_form.=date('d/m/Y',strtotime($_data["fecha_ingreso"]));
            ELSE
               $_form.='';

            $pRutaArchivo = '../file_storage/'.$_data["anio"]."/".$_data["nombre_archivo"].".pdf";
            $_form.='
                    </td>
                    <td align="center">
                        <button type="button" class="btn btn-outline btn-success" onClick="mostrar_pdf(\''.$pRutaArchivo.'\');" title="Mostrar documento PDF">
                            <i class="fa fa-search"></i>
                        </button>
                    </td>
                </tr>';
        }
        else
        {
            $_form.='
                <tr class="odd gradeA">
                    <td class="center"><b>'.$_data["anio"].'</b></td>
                    <td class="center"><b>'.$_data["mes"].'</b></td>
                    <td class="center"><b>'.$_data["nombre_carrera"].'</b></td>
                    <td class="center"><b>'.$_data["tipo_doc"].'</b></td>
                    <td class="center"><b>'.$_data["nombre_unidad_organizativa"].'</b></td>
                    <td class="center"><b>'.$_data["nombre_archivo"].'</b></td>
                    <td class="center"><b>'.$_data["secuencial"].'</b></td>
                    <td class="center"><b>'.$_data["resumen_documento"].'</b></td>
                    <td class="center"><b>';
            IF($_data["fecha_ingreso"])
                $_form.=date('d/m/Y',strtotime($_data["fecha_ingreso"]));
            ELSE
               $_form.='';

            $pRutaArchivo = '../file_storage/'.$_data["anio"]."/".$_data["nombre_archivo"].".pdf";
            $_form.='
                    </b></td>
                    <td align="center">
                        <button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModVerPDF" onClick="jsVerPdf(\''.$_data["anio"].'\',\''.$_data["nombre_archivo"].'\',\''. $_data["id_gestion_documentos"].'\');" title="Mostrar documento PDF">
                            <i class="fa fa-search"></i>
                        </button>
                    </td>
                </tr>';
        }

    }


    $_form.='
        </tbody>
    </table>
    <div class="modal fade" id="ModVerPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';
    $_form.='
    <script>

        $(document).ready(function() {
            $("#doc_recibidos").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
        });
    </script>';
    return $_form;
}
function fListaDocEnviados()
{
    global $_pUsuario, $_pIdUnidadOrganizativa;
    $_dboperations=new dboperations();
    $p_SQL="SELECT  a.id_gestion_documentos, a.anio, a.mes, a.id_carrera, b.nombre_carrera, a.prefijo, c.nombre_unidad_organizativa,
            a.id_periodo_academico, d.descripcion as parcial, a.secuencial, a.resumen_documento, a.nombre_archivo, a.fecha_ingreso,
            upper(e.descripcion) as tipo_doc
            FROM tbl_gestion_documentos a
            inner join tbl_carrera b on a.id_carrera=b.id_carrera
            inner join tbl_unidad_organizativa c on a.prefijo=c.prefijo
            inner join tbl_periodos_academicos d on a.id_periodo_academico=d.id_periodo_academico
            inner join tbl_tipo_documento e on a.prefijo_doc=e.prefijo_doc
            where a.enviado_por='".$_pIdUnidadOrganizativa."' and a.estado is null
            order by a.fecha_ingreso DESC";
    $_vlv_Resultado=$_dboperations->f_EjecutaQuery($p_SQL);
    $vlnNum=$_dboperations->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_dboperations->f_EjecutaQuery($p_SQL);
    $_form='
    <table width="100%" class="table table-striped table-bordered table-hover" id="doc_enviados">
        <thead>
            <tr>
                <th>A&ntilde;o</th>
                <th>Mes</th>
                <th>Carrera</th>
                <th>Tipo doc.</th>
                <th>U. Organiza.</th>
                <th>Nombre Archivo</th>
                <th>Secuencia</th>
                <th>Resumen</th>
                <th>Fec. Ing.</th>
                <th><i class="fa fa-search"></i></th>
            </tr>
        </thead>
        <tbody>';
    foreach ($_vlv_Resultado as $_data)
    {
        $_form.='
            <tr class="odd gradeA">
                <td class="center">'.$_data["anio"].'</td>
                <td class="center">'.$_data["mes"].'</td>
                <td class="center">'.$_data["nombre_carrera"].'</td>
                <td class="center">'.$_data["tipo_doc"].'</td>
                <td class="center">'.$_data["nombre_unidad_organizativa"].'</td>
                <td class="center">'.$_data["nombre_archivo"].'</td>
                <td class="center">'.$_data["secuencial"].'</td>
                <td class="center">'.$_data["resumen_documento"].'</td>
                <td class="center">';
        IF($_data["fecha_ingreso"])
            $_form.=date('d/m/Y',strtotime($_data["fecha_ingreso"]));
        ELSE
           $_form.='';

        $pRutaArchivo = '../file_storage/'.$_data["anio"]."/".$_data["nombre_archivo"].".pdf";
        $_form.='
                </td>
                <td align="center">
                    <button type="button" class="btn btn-outline btn-success" onClick="mostrar_pdf(\''.$pRutaArchivo.'\');" title="Mostrar documento PDF">
                        <i class="fa fa-search"></i>
                    </button>
                </td>
            </tr>';
    }
    $_form.='
        </tbody>
    </table>';
    $_form.='
    <script>
        $(document).ready(function() {
            $("#doc_enviados").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
        });
    </script>';
    return $_form;
}
function fVerPDF($pAnio,$pNombreArchivo,$pIdDocumento)
{
    global $_pIdUnidadOrganizativa;
    $_dboperations=new dboperations();
    $_appcomponentes=new appcomponentes();
    $jsondata = array();
    $vlnNum=0;
    $_pIdPerfilUsuario=$_SESSION["vgnIdPerfilUsuario"];
    $_pUsuario=$_SESSION["vgvUsuario"];
    $_pIp=Recuperaip();

    $_pIdDocumento=$pIdDocumento;
    $_pAnio=$pAnio;
    $_pNombreArchivo=$pNombreArchivo;
    $pRutaArchivo = '..//file_storage//'.$_pAnio."//".$_pNombreArchivo.".pdf";
    //echo $pRutaArchivo;   pIdDocumento
    $_dboperations=new dboperations();
    $p_funcion="SP_LEER_DOCUMENTO";
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdDocumento;

    $vlvResultado=$_dboperations->f_EjecutaFuncion($p_funcion,$p_parametros);
    $vlnAlertas=$_appcomponentes->f_buscar_alertas($_pIdUnidadOrganizativa);


    $_form='<!-- Modal -->

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarListaPDF();">&times;</button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                                <div class="row">  ';
    $_form.="                       <embed src='".$pRutaArchivo."' width='100%' height='500px'>";
    $_form.='                   </div>
                            <!-- /.panel-body -->
                        </div>
                    <!-- /.col-lg-12 -->
                </div>
            </div><!-- /.modal-body -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    <!-- /.modal -->
    ';

    $array = array(0 => $_form, 1 => $vlnAlertas);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}

function fAnularDocumento($_pIdCodigo)
{
    global $_pUsuario, $_pIp;
    $_dboperations=new dboperations();
    $p_funcion="SP_ANULAR_DOCUMENTO";
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdCodigo;
    $vlvResultado=explode("|",$_dboperations->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>