<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pAccion               =$_POST["pAccion"];
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$pNombreTabla="tbl_elemento_area";
$pNombreDataTable="ver_elemento_area";
$pNombreModal="ModModificarElementoArea";
$pNombreHeader="Elementos del &aacute;rea No";
$pPlaceHolder="Ingrese la descripci&oacute;n";
$pPlaceHolder1="Ingrese el Largo";
$pPlaceHolder2="Ingrese el Alto";
$pPlaceHolder3="Ingrese el Fondo";

$p_funcion="SP_GUARDAR_ELEMENTO_AREA";
$p_funcionAnulaActiva="SP_ANULAR_ACTIVAR_ELEMENTO_AREA";

/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $pIdEdificio    =$_POST["pIdEdificio"];
        $_pIdNivel    =$_POST["pIdNivel"];
        fCargarFormulario($pIdEdificio,$_pIdNivel);
    break;
    case 'GUARDAR':
        $_pIdTipoElemento  =$_POST['cbTipoElemento'];
        $_pIdNivel  =$_POST['hddIdNivel'];
        $_pAlto     =$_POST['txtAlto'];
        $_pLargo    =$_POST['txtLargo'];
        fGuardar($_pIdTipoElemento, $_pIdNivel, $_pAlto, $_pLargo);
    break;
    case 'ACTUALIZAR':
        $_pIdTipoElemento  =$_POST['cbTipoElemento'];
        $_pIdNivel  =$_POST['hddIdNivel'];
        $_pId       =$_POST['hddId'];
        $_pAlto     =$_POST['txtAlto'];
        $_pLargo    =$_POST['txtLargo'];
        fActualizar($_pId, $_pIdTipoElemento, $_pIdNivel, $_pAlto, $_pLargo);
    break;
    case 'VER_PANTALLA_MODIFICAR':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $_pId               =$_POST['pId'];
        $_pIdEdificio               =$_POST['pIdEdificio'];
        fVerPantallaModificar($_pId,$_pIdEdificio);
    break;
    case 'ANULA_ACTIVA':
        $_pIdOpcion         =$_POST['pIdCodigo'];
        fAnularActivar($_pIdOpcion);
    break;
}

function fGuardar($_pIdTipoElemento, $_pIdNivel, $_pAlto, $_pLargo)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $_pId="null";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdTipoElemento.",".$_pIdNivel.",".$_pAlto.",".$_pLargo.",".$_pId;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

function fActualizar($_pId, $_pIdTipoElemento, $_pIdNivel, $_pAlto, $_pLargo, $_pFondo)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdTipoElemento.",".$_pIdNivel.",".$_pAlto.",".$_pLargo.",".$_pId;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario($pIdEdificio,$pIdNivel)
{
    global $_pNombreArchivo, $pNombreTabla,$pNombreDataTable,$pNombreModal, $pNombreFrm,$pNombreHeader,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3;
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $pNombreFrm="frmNuevo";
    $pNombreBtnGuardar="btnAreaElemento";

    /*CREAR COMBO TIPO AREA*/
    $pNombreCombo='cbElementoNivel';
    $pCamposId='a.id_elemento_nivel';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_elemento a
            inner join ';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbTipoElemento=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Edificio No. '.$pIdEdificio.' - '.$pNombreHeader.' '.$pIdNivel.'
                        </div>
                        <div class="panel-body">
                            <form role="form" id="'.$pNombreFrm.'" >
                            <input type="hidden" id="hddIdNivel" name="hddIdNivel" value="'.$pIdNivel.'">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Tipo de elemento</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        '.$cbTipoElemento.'
                                        <p class="help-block"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Largo (Valor en m2)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required number" id="txtLargo" name="txtLargo" placeholder="'.$pPlaceHolder1.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Alto (Valor en m2)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required number" id="txtAlto" name="txtAlto" placeholder="'.$pPlaceHolder2.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="'.$pNombreBtnGuardar.'" onClick="jsGuardar3(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\',\''.$pIdEdificio.'\',\''.$pIdNivel.'\');">Guardar</button>
                           <button type="button" class="btn btn-primary" onClick="jsRefrescarForm3(\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\',\''.$pIdNivel.'\');">Limpiar</button>
                           <button type="button" class="btn btn-primary" onClick="jsAbrirNivel(\'nivel_edificio.\',\'NUEVO\',\''.$pIdEdificio.'\');">Regresar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_elemento_nivel, a.id_nivel, a.id_tipo_elemento, b.detalle as tipo_elemento, a.alto, a.largo
            FROM ".$pNombreTabla." a
            inner join tbl_tipo_elemento b on a.id_tipo_elemento=b.id_tipo_elemento";

    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

     $_form.='
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="'.$pNombreDataTable.'">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Id. Registro</th>
                                    <th>Tipo Elemento</th>
                                    <th>Alto</th>
                                    <th>Largo</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>';

   foreach ($_vlv_Resultado as $_data)
    {
        $_form.='           <tr class="odd gradeA">
                                <td class="center">'.$_data["estado"].'</td>
                                <td class="center">'.$_data["id_elemento_nivel"].'</td>
                                <td class="center">'.$_data["tipo_elemento"].'</td>
                                <td class="center">'.$_data["alto"].'</td>
                                <td class="center">'.$_data["largo"].'</td>
                            ';
        if($_data["estado"]=='A')
            {
                $_form.='       <td align="center">&nbsp;</td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Registro" onClick="jsAnularActivar3('.$_data["id_elemento_nivel"].',\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\',\''.$pIdNivel.'\')"><i class="fa fa-check"></i></button></td>';

            }
            else
            {
                $_form.='       <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#'.$pNombreModal.'" onClick="jsVerPantallaModificar2('.$_data["id_elemento_nivel"].',\''.$_pNombreArchivo.'\',\''.$pNombreModal.'\',\''.$pIdEdificio.'\')">Modificar</button></td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Registro" onClick="jsAnularActivar3('.$_data["id_elemento_nivel"].',\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\',\''.$pIdNivel.'\')"><i class="fa fa-times"></i></button></td>';
            }
        $_form.='           </tr>';
    }
    $_form.='               </tbody>
                        </table>
                    </div> <!-- /.panel-body -->
                </div>  <!-- /.panel -->
            </div> <!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <script>
        $(document).ready(function() {
            $("#'.$pNombreDataTable.'").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                },
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 0, 1 ]
                } ]
            });
        });
        </script>
        ';
    $_form.='<div class="modal fade" id="'.$pNombreModal.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             </div>';
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}

function fVerPantallaModificar($_pIdOpcion,$pIdEdificio)
{
    global $_pNombreArchivo, $pNombreTabla,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3;
    $pNombreFrm="frmActualizar";
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $jsondata = array();
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_elemento_nivel, a.id_tipo_elemento, a.id_nivel, a.alto, a.largo
            FROM ".$pNombreTabla." a
            where a.id_elemento_nivel=".$_pIdOpcion;
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    /*CREAR COMBO TIPO ESTRUCTURA*/
    $pNombreCombo='cbTipoElemento';
    $pCamposId='a.id_tipo_elemento';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_elemento a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pRequerido='required';

    foreach ($_vlv_Resultado as $_data)
    {
        $pSelected=$_data["id_tipo_elemento"];
        $cbTipoElemento=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);
        $_form='
        <!-- Modal -->
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarForm3(\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\',\''.$_data["id_nivel"].'\');">&times;</button>
                        <div id="divMensajeModal"></div>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Modificar Registro - ID: '.$_data["id_elemento_nivel"].'
                                </div>
                                <div class="panel-body">
                                <form role="form" id="'.$pNombreFrm.'" >
                                <input type="hidden" id="hddId" name="hddId" value="'.$_data["id_elemento_nivel"].'">
                                <input type="hidden" id="hddIdNivel" name="hddIdNivel" value="'.$_data["id_nivel"].'">
                                <input type="hidden" id="hddIdEdificio" name="hddIdEdificio" value="'.$pIdEdificio.'">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Tipo de elemento</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                '.$cbTipoElemento.'
                                                <p class="help-block"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Largo (Valor en m2)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" id="txtLargo" name="txtLargo"
                                                value="'.$_data["largo"].'">
                                                <p class="help-block with-errors">'.$pPlaceHolder1.'</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Alto (Valor en m2)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" id="txtAlto" name="txtAlto"
                                                value="'.$_data["alto"].'">
                                                <p class="help-block with-errors">'.$pPlaceHolder2.'</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </form>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div><!-- /.modal-body -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onClick="jsActualizar(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarForm3(\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\',\''.$_data["id_nivel"].'\');">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        <!-- /.modal -->';
    }

    $array = array(0 => $_form);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}
function fAnularActivar($_pIdOpcion)
{
    global $_pUsuario, $_pIp, $p_funcionAnulaActiva;
    $_operacionesbd=new operacionesbd();

    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdOpcion;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcionAnulaActiva,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>