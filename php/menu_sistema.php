<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pAccion               =$_POST["pAccion"];
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        fCargarFormulario();
    break;
    case 'GUARDAR':
        $_pNombreMenu    =$_POST['txtNombreMenu'];
        fGuardarMenuSistema($_pNombreMenu);
    break;
    case 'ACTUALIZAR':
        $_pNombreMenu    =$_POST['txtNombreMenu'];
        $_pIdMenu     =$_POST['hddIdMenu'];
        fActualizarMenuSistema($_pNombreMenu,$_pIdMenu);
    break;
    case 'LISTA':
        $_pTipoLista    =$_POST['pTipoLista'];
        fGeneraLista($_pTipoLista);
    break;
    case 'CONSULTA':
        $_pIdMenu     =$_POST['pIdMenu'];
        fConsultarMenu($_pIdMenu);
    break;
    case 'MODIFICA':
        $_pIdMenu     =$_POST['pIdMenu'];
        fModificarMenu($_pIdMenu);
    break;
    case 'ANULA_ACTIVA':
        $_pIdOpcion           =$_POST['pIdCodigo'];
        fAnularActivarMenu($_pIdOpcion);
    break;
}

function fGuardarMenuSistema($_pNombreMenu)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    $_pIdMenu="null";
    $p_funcion="SP_GUARDAR_MENU";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."','".strtoupper($_pNombreMenu)."',".$_pIdMenu;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

function fActualizarMenuSistema($_pNombreMenu, $_pIdMenu)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    //$_pIdOpcionSistema="null";
    $p_funcion="SP_GUARDAR_MENU";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."','".strtoupper($_pNombreMenu)."',".$_pIdMenu;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Nuevo Men&uacute; del Sistema
                        </div>
                        <div class="panel-body">
                         <form role="form" id="frmMenuSistema" >
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Nombre del Men&uacute;</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtNombreMenu" name="txtNombreMenu">
                                        <p class="help-block with-errors">Ingrese el nombre del nuevo men&uacute;.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="btnGuardarMenuSistema" onClick="jsGuardarMenuSistema();">Guardar</button>
                           <button type="button" class="btn btn-primary">Limpiar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    //$_form="ENTRO";
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}
function fGeneraLista($pTipo) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_menu_sistema, a.nombre_menu
                FROM tbl_menu_sistema a";
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        $_form='<div class="row">
                <div class="col-lg-10">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-10">
                    <div class="panel panel-primary">
                        <div class="panel-heading">';
        if($pTipo=='C')//CONSULTAR
            $_form.='       Consultar Men&uacute; del Sistema';
        else
            $_form.='       Modificar Men&uacute; del Sistema';

        $_form.='       </div>
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="ver_menu_sistema">
                                <thead>
                                    <tr>
                                        <th>Estado</th>
                                        <th>Id. Opci&oacute;n</th>
                                        <th>Nombre Opci&oacute;n</th>
                                        <th>&nbsp;</th>';
        if($pTipo=='M')//CONSULTAR
            $_form.='                   <th>&nbsp;</th>';

        $_form.='                   </tr>
                                </thead>
                                <tbody>';
        foreach ($_vlv_Resultado as $_data)
        {
        $_form.='                   <tr class="odd gradeA">
                                        <td class="center">'.$_data["estado"].'</td>
                                        <td class="center">'.$_data["id_menu_sistema"].'</td>
                                        <td class="center">'.$_data["nombre_menu"].'</td>
                                        ';
        if($pTipo=='C')//CONSULTAR
            {
                if($_data["estado"]=='A')
                    $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-danger" data-toggle="modal" data-target="#ModConsultaMenuSistema" onClick="jsConsultarMenuSistema('.$_data["id_menu_sistema"].')">Ver</button></td>';
                else
                    $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModConsultaMenuSistema" onClick="jsConsultarMenuSistema('.$_data["id_menu_sistema"].')">Ver</button></td>';
            }
            else//MODIFICAR
            {
                if($_data["estado"]=='A')
                {
                    $_form.='           <td align="center">&nbsp;</td>';
                    $_form.='           <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Men&uacute;" onClick="jsAnularActivar('.$_data["id_menu_sistema"].',\'menu_sistema.php\')"><i class="fa fa-check"></i></button></td>';

                }
                else
                {
                    $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModModificarMenuSistema" onClick="jsModificarMenuSistema('.$_data["id_menu_sistema"].')">Modificar</button></td>';
                    $_form.='           <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Men&uacute;" onClick="jsAnularActivar('.$_data["id_menu_sistema"].',\'menu_sistema.php\')"><i class="fa fa-times"></i></button></td>';
                }
            }
        $_form.='
                                    </tr>';
        }
        $_form.='
                                </tbody>

                            </table>
                        </div>
                        <!-- /.panel-body -->

                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <script>
            $(document).ready(function() {
                $("#ver_menu_sistema").DataTable({
                    responsive: true,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                        "zeroRecords": "No se encontraron registros",
                        "info": "P&aacutegina _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros",
                        "sSearch":         "Buscar:",
                        "oPaginate": {
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    }
                });
            });
            </script>
            ';
        if($pTipo=='C')//CONSULTAR
        {
            $_form.='<div class="modal fade" id="ModConsultaMenuSistema" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';
        }
        else
        {
            $_form.='<div class="modal fade" id="ModModificarMenuSistema" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';
        }
        $array = array(0 => $_form);

        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fConsultarMenu($_pIdMenu) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_menu_sistema, a.nombre_menu
                FROM tbl_menu_sistema a
                where a.id_menu_sistema=".$_pIdMenu;
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        foreach ($_vlv_Resultado as $_data)
        {
            $_form='<!-- Modal -->
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-lg-12">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        Consulta Men&uacute; - ID: '.$_data["id_menu_sistema"].'
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label>Nombre del Men&uacute;</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                                        <input class="form-control" id="txtNombreMenu" name="txtNombreMenu" onBlur="js_solo_mayusculas(this.value, this.id);"
                                                                        value="'.$_data["nombre_menu"].'" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.row (nested) -->
                                                    </div>
                                                </div>
                                                <!-- /.panel -->
                                            </div>
                                        </div><!-- /.modal-body -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->

                            <!-- /.modal -->';
        }

        $array = array(0 => $_form);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fModificarMenu($_pIdOpcion) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_menu_sistema, a.nombre_menu
                FROM tbl_menu_sistema a
                where a.id_menu_sistema=".$_pIdOpcion;
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        foreach ($_vlv_Resultado as $_data)
        {
            $_form='<!-- Modal -->
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarLista(\'menu_sistema.php\');">&times;</button>
                                            <div id="divMensajeModal"></div>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-lg-12">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        Modificar Men&uacute; - ID: '.$_data["id_menu_sistema"].'
                                                    </div>
                                                    <div class="panel-body">
                                                    <form role="form" id="frmMenuSistema" >
                                                    <input type="hidden" id="hddIdMenu" name="hddIdMenu" value="'.$_data["id_menu_sistema"].'">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label>Nombre del Men&uacute;</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                                        <input class="form-control required" id="txtNombreMenu" name="txtNombreMenu"
                                                                        value="'.$_data["nombre_menu"].'">
                                                                        <p class="help-block with-errors">Ingrese el nombre del nuevo Men&uacute;.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.row (nested) -->
                                                    </form>
                                                    </div>
                                                </div>
                                                <!-- /.panel -->
                                            </div>
                                        </div><!-- /.modal-body -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" onClick="jsActualizarMenuSistema();">Guardar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarLista(\'menu_sistema.php\');">Cerrar</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->

                            <!-- /.modal -->';
        }

        $array = array(0 => $_form);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fAnularActivarMenu($_pIdOpcion)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    $p_funcion="SP_ANULAR_ACTIVAR_MENUS_SISTEMA";
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdOpcion;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>