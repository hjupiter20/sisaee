<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/
$_pAccion               =$_POST["pAccion"];

switch($_pAccion)
{
    case 'LOGIN':
        $_pUser    =$_POST['pUser'];
        $_pPasswd    =$_POST['pPasswd'];
        fLogin($_pUser, $_pPasswd);
    break;
    case 'LOGOUT':
        fLogout();
    break;
    case 'CAMBIAR_PASSWD':
        $_pUser         =$_SESSION["vgvUsuario"];
        $_pPasswdNew    =$_POST['pPasswdNew'];
        $_pIdCodigo     =$_SESSION["vgvIdCodigo"];
        //fCambiarPasswd($_pUser, $_pPasswdNew);
        fCambiarPasswd($_pUser, $_pPasswdNew, $_pIdCodigo);
    break;
    case 'VALIDA_PASSWD_ANT':
        $_pUser             =$_SESSION["vgvUsuario"];
        $_pPasswdAnterior   =$_POST['pPasswdAnterior'];
        fValidaPasswdAnt($_pUser, $_pPasswdAnterior);
    break;
    case 'VALIDA_PASSWD_NEW':
        $_pUser             =$_SESSION["vgvUsuario"];
        $_pIdCodigo         =$_SESSION["vgvIdCodigo"];
        $_pPasswdNew   =$_POST['pPasswdNew'];
        fValidaPasswdNew($_pUser, $_pPasswdNew,$_pIdCodigo);
    break;
}
$_operacionesbd=new operacionesbd();

function  fLogin($_pUser, $_pPasswd)
{
    $_operacionesbd=new operacionesbd();
    $_operaciones_usuario=new operaciones_usuario();
    $_pValidaPrimerLogin=$_operaciones_usuario->f_valida_primer_ingreso($_pUser, $_pPasswd);
    if($_pValidaPrimerLogin=='1')
    {
        $_pValidacion=$_operaciones_usuario->f_login($_pPasswd,$_pUser);

        switch($_pValidacion)
        {
            case 1:
                header('Content-type: application/json; charset=utf-8');
                $vlvResultado[1]="1";
                $vlvResultado[0]="El usuario o contraseña son correctos";
                $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
            break;
            case 2:
                header('Content-type: application/json; charset=utf-8');
                $vlvResultado[1]="2";
                $vlvResultado[0]="El usuario o contraseña son incorrectos";
                $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
            break;
            case 3:
                header('Content-type: application/json; charset=utf-8');
                $vlvResultado[1]="3";
                $vlvResultado[0]="La contraseña se encuentra caducada";
                $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
            break;
            case 4:
                header('Content-type: application/json; charset=utf-8');
                $vlvResultado[1]="4";
                $vlvResultado[0]="Su contraseña se encuentra bloqueada, favor contactar al administrador";
                $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
            break;
            case 5:
                header('Content-type: application/json; charset=utf-8');
                $vlvResultado[1]="5";
                $vlvResultado[0]="El usuario no existe";
                $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
            break;
        } 
    }
    elseif($_pValidaPrimerLogin=='5')
    {
        header('Content-type: application/json; charset=utf-8');
        $vlvResultado[1]="5";
        $vlvResultado[0]="El usuario no existe";
        $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    }
    else
    {
        header('Content-type: application/json; charset=utf-8');
        $vlvResultado[1]="6";
        $vlvResultado[0]="Es el primer ingreso, debe cambiar contraseña";
        $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    }
    echo json_encode($_jSonArray);

}

function  fLogout()
{
    session_destroy();
    $vlvResultado[1]="1";
    $vlvResultado[0]="El usuario o contraseña son correctos";
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
}
function  fValidaPasswdAnt($_pUser, $_pPasswd)
{
    $_operaciones_usuario=new operaciones_usuario();
    $_operacionesbd=new operacionesbd();
    /*CONSULTA PARAMETROS DE SEGURIDAD*/
    $p_SQL="SELECT a.id_parametros_seguridad, a.complejidad_password, a.longitud_max_password, a.edad_min_password
            FROM tbl_parametros_seguridad a
            where a.estado IS NULL";
    $_rs=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $_pDiasCambioClaves=0;
    foreach ($_rs as $_data)
    {
        $_pEdadMinPasswd=$_data["edad_min_password"];
        //$_pLongitudMaxPasswd=$_data["longitud_max_password"];
    }
    /*CONSULTA DIAS DESDE EL CAMBIO DE CLAVES*/
    $p_SQL1="select CURRENT_DATE-a.fecha_cambio_password as dias_cambio_clave,a.usuario_nuevo
            from tbl_usuarios a
            where a.usuario='".$_pUser."' and a.estado is null";
    $_rs1=$_operacionesbd->f_EjecutaQuery($p_SQL1);

    foreach ($_rs1 as $_data1)
    {
        $_pDiasCambioClaves=$_data1["dias_cambio_clave"];
        $_pUsuarioNuevo=$_data1["usuario_nuevo"];
    }

    if($_pUsuarioNuevo=='1')//VERIFICA SI EL USUARIO ES NUEVO, SI ES ANTIGUO APLICA VALIDACION DE FECHA MINIMA
    {
        if($_pDiasCambioClaves>$_pEdadMinPasswd)
        {
            $_pValidaPasswd=$_operaciones_usuario->f_valida_password($_pUser, $_pPasswd);
            if($_pValidaPasswd)
            {
                $vlvResultado[1]="1";
                $vlvResultado[0]="ok";
                //$vlvResultado[0]=password_hash($_pPasswd, PASSWORD_DEFAULT, ['cost' => 12]);;
                $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
            }
            else
            {
                $vlvMensaje="<span>La contrase&ntilde;a no coincide con la guardada, favor ingresarla nuevamente</span>";
                $vlvResultado[1]="2";
                $vlvResultado[0]=$vlvMensaje;
                //$vlvResultado[0]=password_hash($_pPasswd, PASSWORD_DEFAULT, ['cost' => 12]);;
                $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
            }
        }
        else
        {
            $vlvMensaje="<span>La edad m&iacute;nima de una contrase&ntilde;a es de <b>". $_pEdadMinPasswd." d&iacute;a (s).</b><br>La edad de su contrase&ntilde;a es de:<b>". $_pDiasCambioClaves." d&iacute;a (s).</b></span> ";
            $vlvResultado[1]="3";
            $vlvResultado[0]=$vlvMensaje;
                //$vlvResultado[0]=password_hash($_pPasswd, PASSWORD_DEFAULT, ['cost' => 12]);;
            $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
        }
    }
    else
    {
        $_pValidaPasswd=$_operaciones_usuario->f_valida_password($_pUser, $_pPasswd);
        if($_pValidaPasswd)
        {
            $vlvResultado[1]="1";
            $vlvResultado[0]="ok";
            //$vlvResultado[0]=password_hash($_pPasswd, PASSWORD_DEFAULT, ['cost' => 12]);;
            $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
        }
        else
        {
            $vlvMensaje="<span>La contrase&ntilde;a no coincide con la guardada, favor ingresarla nuevamente</span>";
            $vlvResultado[1]="2";
            $vlvResultado[0]=$vlvMensaje;
            //$vlvResultado[0]=password_hash($_pPasswd, PASSWORD_DEFAULT, ['cost' => 12]);;
            $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
        }
    }


    echo json_encode($_jSonArray);
}
function  fValidaPasswdNew($_pUser, $_pPasswd, $_pIdCodigo)
{
    $_operaciones_usuario=new operaciones_usuario();

    $_pValidaHistoriaPasswd=$_operaciones_usuario->f_valida_historia_passwd($_pPasswd, $_pIdCodigo);

    if($_pValidaHistoriaPasswd)
    {
        $vlvMensaje="<span><b>La clave ya ha sido utilizada</b></span> ";
        $vlvResultado[1]="2";
        $vlvResultado[0]=$vlvMensaje;
        //$vlvResultado[0]=password_hash($_pPasswd, PASSWORD_DEFAULT, ['cost' => 12]);;
        $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    }
    else
    {
        $vlvMensaje="ok";
        $vlvResultado[1]="1";
        $vlvResultado[0]=$vlvMensaje;
        //$vlvResultado[0]=password_hash($_pPasswd, PASSWORD_DEFAULT, ['cost' => 12]);;
        $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    }


    echo json_encode($_jSonArray);
}

function  fCambiarPasswd($_pUser, $_pPasswd, $_pIdCodigo)
{
    //$_operacionesbd=new operacionesbd();
    $_operaciones_usuario=new operaciones_usuario();
    $_pCambioPasswd=$_operaciones_usuario->f_cambiarPasswd($_pPasswd,$_pUser,$_pIdCodigo);

    if($_pCambioPasswd=='1')
    {
        header('Content-type: application/json; charset=utf-8');
        $vlvMensaje="<span><b>Su contrase&ntilde;a se cambio con exito</b></span> ";
        $vlvResultado[1]="1";
        $vlvResultado[0]=$vlvMensaje;
        $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    }
    else
    {
        header('Content-type: application/json; charset=utf-8');
        $vlvMensaje="<span><b>Se presento un error y no se cambio su contrase&ntilde;a</b></span> ";
        $vlvResultado[1]="2";
        $vlvResultado[0]=$vlvMensaje;
        $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    }
    echo json_encode($_jSonArray);
}  
?>