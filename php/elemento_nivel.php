<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pAccion =$_POST["pAccion"];
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$pNombreTabla="tbl_elemento_nivel";
$pNombreDataTable="ver_elemento_nivel";
$pNombreModal="ModModificarElementoNivel";
$pNombreHeader="Elementos del nivel No";
$pPlaceHolder="Ingrese la descripci&oacute;n";
$pPlaceHolder1="Ingrese el Largo";
$pPlaceHolder2="Ingrese el Alto";
$pPlaceHolder3="Ingrese el Ancho";
$pPlaceHolder4="Ingrese el Espesor";

$p_funcion="SP_GUARDAR_ELEMENTO_NIVEL";
$p_funcionAnulaActiva="SP_ANULAR_ACTIVAR_ELEMENTO_NIVEL";

/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $pIdEdificio    =$_POST["pIdEdificio"];
        $_pIdNivel    =$_POST["pIdNivel"];
        fCargarFormulario($pIdEdificio,$_pIdNivel);
    break;
    case 'GUARDAR':
        $_pIdTipoElemento  =$_POST['cbTipoElemento'];
        $_pIdNivel  =$_POST['hddIdNivel'];
        $_pAlto     =$_POST['txtAlto'];
        $_pLargo    =$_POST['txtLargo'];
        $_pNombreElemento   =$_POST['txtNombreElemento'];
        $_pAncho            =$_POST['txtAncho'];
        $_pEspesor          =$_POST['txtEspesor'];
        $_pArmadura         =$_POST['txArmadura'];
        $_pTipoMaterial     =$_POST['cbTipoMaterial'];
        $_pResistencia      =$_POST['txResistencia'];
        $_pAceroFrecuencia  =$_POST['txAceroFrecuencia'];
        fGuardar($_pIdTipoElemento, $_pIdNivel, $_pAlto, $_pLargo, $_pNombreElemento, $_pAncho, $_pEspesor, $_pArmadura, $_pTipoMaterial, $_pResistencia, $_pAceroFrecuencia);
    break;
    case 'ACTUALIZAR':
        $_pIdTipoElemento  =$_POST['cbTipoElemento'];
        $_pIdNivel  =$_POST['hddIdNivel'];
        $_pId       =$_POST['hddId'];
        $_pAlto     =$_POST['txtAlto'];
        $_pLargo    =$_POST['txtLargo'];
        $_pNombreElemento   =$_POST['txtNombreElemento'];
        $_pAncho            =$_POST['txtAncho'];
        $_pEspesor          =$_POST['txtEspesor'];
        $_pArmadura         =$_POST['txArmadura'];
        $_pTipoMaterial     =$_POST['cbTipoMaterial'];
        $_pResistencia      =$_POST['txResistencia'];
        $_pAceroFrecuencia  =$_POST['txAceroFrecuencia'];
        fActualizar($_pId, $_pIdTipoElemento, $_pIdNivel, $_pAlto, $_pLargo, $_pNombreElemento, $_pAncho, $_pEspesor, $_pArmadura, $_pTipoMaterial, $_pResistencia, $_pAceroFrecuencia);
    break;
    case 'VER_PANTALLA_MODIFICAR':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $_pId               =$_POST['pId'];
        $_pIdEdificio               =$_POST['pIdEdificio'];
        fVerPantallaModificar($_pId,$_pIdEdificio);
    break;
    case 'ANULA_ACTIVA':
        $_pIdOpcion         =$_POST['pIdCodigo'];
        fAnularActivar($_pIdOpcion);
    break;
}

function fGuardar($_pIdTipoElemento, $_pIdNivel, $_pAlto, $_pLargo, $_pNombreElemento, $_pAncho, $_pEspesor, $_pArmadura, $_pTipoMaterial, $_pResistencia, $_pAceroFrecuencia)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $_pId="null";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdTipoElemento.",".$_pIdNivel.",".$_pAlto.",".$_pLargo.",'".$_pNombreElemento."',".$_pAncho.",".$_pEspesor.",'".$_pArmadura."',".$_pTipoMaterial.",'".$_pResistencia."','".$_pAceroFrecuencia."',".$_pId;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

function fActualizar($_pId, $_pIdTipoElemento, $_pIdNivel, $_pAlto, $_pLargo, $_pNombreElemento, $_pAncho, $_pEspesor, $_pArmadura, $_pTipoMaterial, $_pResistencia, $_pAceroFrecuencia)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdTipoElemento.",".$_pIdNivel.",".$_pAlto.",".$_pLargo.",'".$_pNombreElemento."',".$_pAncho.",".$_pEspesor.",'".$_pArmadura."',".$_pTipoMaterial.",'".$_pResistencia."','".$_pAceroFrecuencia."',".$_pId;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario($pIdEdificio,$pIdNivel)
{
    global $_pNombreArchivo, $pNombreTabla,$pNombreDataTable,$pNombreModal, $pNombreFrm,$pNombreHeader,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3, $pPlaceHolder4;
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $pNombreFrm="frmNuevo";
    $pNombreBtnGuardar="btnAreaNivel";

    /*CREAR COMBO TIPO AREA*/
    $pNombreCombo='cbTipoElemento';
    $pCamposId='a.id_tipo_elemento';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_elemento a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbTipoElemento=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    /*CREAR COMBO TIPO MATERIAL*/
    $pNombreCombo='cbTipoMaterial';
    $pCamposId='a.id_tipo_material';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_material a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbTipoMaterial=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Edificio No. '.$pIdEdificio.' - '.$pNombreHeader.' '.$pIdNivel.'
                        </div>
                        <div class="panel-body">
                            <form role="form" id="'.$pNombreFrm.'" >
                            <input type="hidden" id="hddIdNivel" name="hddIdNivel" value="'.$pIdNivel.'">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nombre Elemento</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtNombreElemento" name="txtNombreElemento" placeholder="'.$pPlaceHolder.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Tipo de elemento</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        '.$cbTipoElemento.'
                                        <p class="help-block"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            Geometr&iacute;a
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>Ancho (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                        <input class="form-control required number" id="txtAncho" name="txtAncho" placeholder="'.$pPlaceHolder3.'">
                                                        <p class="help-block with-errors"></p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>Largo (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                        <input class="form-control required number" id="txtLargo" name="txtLargo" placeholder="'.$pPlaceHolder1.'">
                                                        <p class="help-block with-errors"></p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>Altura entre niveles (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                        <input class="form-control required number" id="txtAlto" name="txtAlto" placeholder="'.$pPlaceHolder2.'">
                                                        <p class="help-block with-errors"></p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>Espesor (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                        <input class="form-control required number" id="txtEspesor" name="txtEspesor" placeholder="'.$pPlaceHolder4.'">
                                                        <p class="help-block with-errors"></p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Armadura</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                        <textarea class="form-control required" rows="1" id="txArmadura" name="txArmadura" placeholder="'.$pPlaceHolder.'"></textarea>
                                                        <p class="help-block with-errors"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">
                                            Materiales
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Tipo de material</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                        '.$cbTipoMaterial.'
                                                        <p class="help-block"></p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Resistencia</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                        <textarea class="form-control required" rows="1" id="txResistencia" name="txResistencia" placeholder="'.$pPlaceHolder.'"></textarea>
                                                        <p class="help-block with-errors"></p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Acero/Frecuencia</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                        <textarea class="form-control required" rows="1" id="txAceroFrecuencia" name="txAceroFrecuencia" placeholder="'.$pPlaceHolder.'"></textarea>
                                                        <p class="help-block with-errors"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="'.$pNombreBtnGuardar.'" onClick="jsGuardar3(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\',\''.$pIdEdificio.'\',\''.$pIdNivel.'\');">Guardar</button>
                           <button type="button" class="btn btn-primary" onClick="jsRefrescarForm3(\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\',\''.$pIdNivel.'\');">Limpiar</button>
                           <button type="button" class="btn btn-primary" onClick="jsAbrirNivel(\'nivel_edificio.\',\'NUEVO\',\''.$pIdEdificio.'\');">Regresar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_elemento_nivel, a.nombre_elemento, a.id_nivel, a.id_tipo_elemento, b.detalle as tipo_elemento, a.alto, a.largo
            FROM ".$pNombreTabla." a
            inner join tbl_tipo_elemento b on a.id_tipo_elemento=b.id_tipo_elemento
            where a.id_nivel=".$pIdNivel;

    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

     $_form.='
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="'.$pNombreDataTable.'">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Id. Registro</th>
                                    <th>Nombre Elemento</th>
                                    <th>Tipo Elemento</th>
                                    <th>Alto</th>
                                    <th>Largo</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>';

   foreach ($_vlv_Resultado as $_data)
    {
        $_form.='           <tr class="odd gradeA">
                                <td class="center">'.$_data["estado"].'</td>
                                <td class="center">'.$_data["id_elemento_nivel"].'</td>
                                <td class="center">'.$_data["nombre_elemento"].'</td>
                                <td class="center">'.$_data["tipo_elemento"].'</td>
                                <td class="center">'.$_data["alto"].'</td>
                                <td class="center">'.$_data["largo"].'</td>
                            ';
        if($_data["estado"]=='A')
            {
                $_form.='       <td align="center">&nbsp;</td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Registro" onClick="jsAnularActivar3('.$_data["id_elemento_nivel"].',\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\',\''.$pIdNivel.'\')"><i class="fa fa-check"></i></button></td>';

            }
            else
            {
                $_form.='       <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#'.$pNombreModal.'" onClick="jsVerPantallaModificar2('.$_data["id_elemento_nivel"].',\''.$_pNombreArchivo.'\',\''.$pNombreModal.'\',\''.$pIdEdificio.'\')">Modificar</button></td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Registro" onClick="jsAnularActivar3('.$_data["id_elemento_nivel"].',\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\',\''.$pIdNivel.'\')"><i class="fa fa-times"></i></button></td>';
            }
        $_form.='           </tr>';
    }
    $_form.='               </tbody>
                        </table>
                    </div> <!-- /.panel-body -->
                </div>  <!-- /.panel -->
            </div> <!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <script>
        $(document).ready(function() {
            $("#'.$pNombreDataTable.'").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                },
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 0, 1 ]
                } ]
            });
        });
        </script>
        ';
    $_form.='<div class="modal fade" id="'.$pNombreModal.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             </div>';
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}

function fVerPantallaModificar($_pIdOpcion,$pIdEdificio)
{
    global $_pNombreArchivo, $pNombreTabla,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3;
    $pNombreFrm="frmActualizar";
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $jsondata = array();
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_elemento_nivel, a.id_tipo_elemento, a.id_nivel, a.alto, a.largo, a.nombre_elemento, a.ancho, a.espesor, a.armadura,
    a.id_tipo_material, a.resistencia, a.acero_frecuencia
            FROM ".$pNombreTabla." a
            where a.id_elemento_nivel=".$_pIdOpcion;
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    /*CREAR COMBO TIPO ESTRUCTURA*/
    $pNombreCombo='cbTipoElemento';
    $pCamposId='a.id_tipo_elemento';
    $pCamposDetalle='a.detalle';
    $pTabla='tbl_tipo_elemento a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder='';
    $pGroupBy='';
    $pRequerido='required';

    /*CREAR COMBO TIPO MATERIAL*/
    $pNombreCombo1='cbTipoMaterial';
    $pCamposId1='a.id_tipo_material';
    $pCamposDetalle1='a.detalle';
    $pTabla1='tbl_tipo_material a';
    $pInner1='';
    $pWhere1=' where a.estado is null';
    $pOrder1='';
    $pGroupBy1='';
    $pRequerido1='required';

    foreach ($_vlv_Resultado as $_data)
    {
        $pSelected=$_data["id_tipo_elemento"];
        $cbTipoElemento=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

        $pSelected1=$_data["id_tipo_material"];
        $cbTipoMaterial=$_appcomponentes->f_crear_combo($pNombreCombo1,$pCamposId1,$pCamposDetalle1,$pTabla1,$pInner1,$pWhere1,$pOrder1,$pGroupBy1,$pSelected1,$pRequerido1);
        $_form='
        <!-- Modal -->
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarForm3(\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\',\''.$_data["id_nivel"].'\');">&times;</button>
                        <div id="divMensajeModal"></div>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Modificar Registro - ID: '.$_data["id_elemento_nivel"].'
                                </div>
                                <div class="panel-body">
                                <form role="form" id="'.$pNombreFrm.'" >
                                <input type="hidden" id="hddId" name="hddId" value="'.$_data["id_elemento_nivel"].'">
                                <input type="hidden" id="hddIdNivel" name="hddIdNivel" value="'.$_data["id_nivel"].'">
                                <input type="hidden" id="hddIdEdificio" name="hddIdEdificio" value="'.$pIdEdificio.'">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nombre Elemento</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            <input class="form-control required" id="txtNombreElemento" name="txtNombreElemento" value="'.$_data["nombre_elemento"].'">
                                            <p class="help-block with-errors"></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Tipo de elemento</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbTipoElemento.'
                                            <p class="help-block"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                Geometr&iacute;a
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>Ancho (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                            <input class="form-control required number" id="txtAncho" name="txtAncho" placeholder="'.$pPlaceHolder1.'"
                                                            value="'.$_data["ancho"].'">
                                                            <p class="help-block with-errors"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>Largo (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                            <input class="form-control required number" id="txtLargo" name="txtLargo" placeholder="'.$pPlaceHolder1.'"
                                                            value="'.$_data["largo"].'">
                                                            <p class="help-block with-errors"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>Altura niveles (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                            <input class="form-control required number" id="txtAlto" name="txtAlto" placeholder="'.$pPlaceHolder2.'"
                                                            value="'.$_data["alto"].'">
                                                            <p class="help-block with-errors"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>Espesor (Valor en m)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                            <input class="form-control required number" id="txtEspesor" name="txtEspesor" placeholder="'.$pPlaceHolder2.'"
                                                            value="'.$_data["espesor"].'">
                                                            <p class="help-block with-errors"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label>Armadura</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                            <textarea class="form-control required" rows="1" id="txArmadura" name="txArmadura">'.$_data["armadura"].'</textarea>
                                                            <p class="help-block with-errors"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-warning">
                                            <div class="panel-heading">
                                                Materiales
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label>Tipo de material</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                            '.$cbTipoMaterial.'
                                                            <p class="help-block"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label>Resistencia</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                            <textarea class="form-control required" rows="1" id="txResistencia" name="txResistencia">'.$_data["resistencia"].'</textarea>
                                                            <p class="help-block with-errors"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label>Acero/Frecuencia</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                            <textarea class="form-control required" rows="1" id="txAceroFrecuencia" name="txAceroFrecuencia">'.$_data["acero_frecuencia"].'</textarea>
                                                            <p class="help-block with-errors"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <!-- /.row (nested) -->
                                </form>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div><!-- /.modal-body -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onClick="jsActualizar(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarForm3(\''.$_pNombreArchivo.'\',\''.$pIdEdificio.'\',\''.$_data["id_nivel"].'\');">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        <!-- /.modal -->';
    }

    $array = array(0 => $_form);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}
function fAnularActivar($_pIdOpcion)
{
    global $_pUsuario, $_pIp, $p_funcionAnulaActiva;
    $_operacionesbd=new operacionesbd();

    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdOpcion;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcionAnulaActiva,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>