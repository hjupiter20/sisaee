<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$_pAccion               =$_POST["pAccion"];
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        fCargarFormulario();
    break;
    case 'GUARDAR':
        $_pNombreOpcion    =$_POST['txtNombreOpcion'];
        $_pAliasOpcion    =$_POST['txtAliasOpcion'];
        $_pNombreArchivo    =$_POST['txtNombreArchivo'];
        $_pIdAccion    =$_POST['cbAcciones'];

        fGuardarOpcionSistema($_pNombreOpcion, $_pAliasOpcion, $_pNombreArchivo,$_pIdAccion);
    break;
    case 'ACTUALIZAR':
        $_pNombreOpcion    =$_POST['txtNombreOpcion'];
        $_pAliasOpcion    =$_POST['txtAliasOpcion'];
        $_pIdOpciones     =$_POST['hddIdOpciones'];
        $_pNombreArchivo    =$_POST['txtNombreArchivo'];
        $_pIdAccion    =$_POST['cbAcciones'];
        fActualizarOpcionSistema($_pNombreOpcion,$_pAliasOpcion,$_pIdOpciones, $_pNombreArchivo,$_pIdAccion);
    break;
    case 'LISTA':
        $_pTipoLista    =$_POST['pTipoLista'];
        fGeneraLista($_pTipoLista);
    break;
    case 'CONSULTA':
        $_pIdOpciones     =$_POST['pIdOpciones'];
        fConsultarOpcion($_pIdOpciones);
    break;
    case 'MODIFICA':
        $_pIdOpciones     =$_POST['pIdOpciones'];
        fModificarOpcion($_pIdOpciones);
    break;
    case 'ANULA_ACTIVA':
        $_pIdOpcion           =$_POST['pIdCodigo'];
        fAnularActivarOpcion($_pIdOpcion);
    break;
}

function fGuardarOpcionSistema($_pNombreOpcion, $_pAliasOpcion, $_pNombreArchivo,$_pIdAccion)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    $_pIdOpcionSistema="null";
    $p_funcion="SP_GUARDAR_OPCION_SISTEMA";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."','".$_pNombreOpcion."','".$_pAliasOpcion."',".$_pIdOpcionSistema.",'".$_pNombreArchivo."',".$_pIdAccion;
//echo p_parametros;  
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

function fActualizarOpcionSistema($_pNombreOpcion, $_pAliasOpcion, $_pIdOpcionSistema, $_pNombreArchivo,$_pIdAccion)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    //$_pIdOpcionSistema="null";
    $p_funcion="SP_GUARDAR_OPCION_SISTEMA";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."','".$_pNombreOpcion."','".$_pAliasOpcion."',".$_pIdOpcionSistema.",'".$_pNombreArchivo."',".$_pIdAccion;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    $_appcomponentes=new appcomponentes();
    /*CREAR COMBO PERFILES*/
    $pNombreCombo='cbAcciones';
    $pCamposId='a.id_acciones';
    $pCamposDetalle='a.accion';
    $pTabla='tbl_acciones a';
    $pInner='';
    $pWhere='';
    $pOrder='';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbAcciones=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);
    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Nuevo Acceso al Sistema
                        </div>
                        <div class="panel-body">
                         <form role="form" data-toggle="validator" id="frmOpcionesSistema" >
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Nombre de Opci&oacute;n</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtNombreOpcion" name="txtNombreOpcion">
                                        <p class="help-block with-errors">Ingrese el nombre de la nueva opci&oacute;n.</p>
                                    </div>
                                     <div class="form-group">
                                        <label>Alias de Opci&oacute;n</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtAliasOpcion" name="txtAliasOpcion">
                                        <p class="help-block with-errors">Ingrese el Alias con el que se reconocera a la opci&oacute;n.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">                                   
                                    <div class="form-group">
                                        <label>Nombre Archivo</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtNombreArchivo" name="txtNombreArchivo">
                                        <p class="help-block with-errors">Ingrese el nombre del archivo.</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Acciones</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        '.$cbAcciones.'
                                    <p class="help-block">Seleccione la unidad organizativa.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="btnGuardarOpcionSistema" onClick="jsGuardarOpcionSistema();">Guardar</button>
                           <button type="button" class="btn btn-primary">Limpiar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    //$_form="ENTRO";
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}
function fGeneraLista($pTipo) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_opcion_sistema, a.nombre_opcion, a.alias_opcion, a.nombre_archivo, b.accion
                FROM tbl_opcion_sistema a
                inner join tbl_acciones b on a.id_acciones=b.id_acciones";
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">';
        if($pTipo=='C')//CONSULTAR
            $_form.='       Consulta Opciones del Sistema';
        else
            $_form.='       Modificar Opciones del Sistema';

        $_form.='       </div>
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="ver_opciones_sistema">
                                <thead>
                                    <tr>
                                        <th>Estado</th>
                                        <th>Id. Opci&oacute;n</th>
                                        <th>Nombre Opci&oacute;n</th>
                                        <th>Alias Opci&oacute;n</th>
                                        <th>Nombre Archivo</th>
                                        <th>Acci&oacute;n</th>
                                        <th>&nbsp;</th>';
        if($pTipo=='M')//CONSULTAR
            $_form.='                   <th>&nbsp;</th>';

        $_form.='                   </tr>
                                </thead>
                                <tbody>';
        foreach ($_vlv_Resultado as $_data)
        {
            $_form.='                   <tr class="odd gradeA">
                                            <td class="center">'.$_data["estado"].'</td>
                                            <td class="center">'.$_data["id_opcion_sistema"].'</td>
                                            <td class="center">'.$_data["nombre_opcion"].'</td>
                                            <td class="center">'.$_data["alias_opcion"].'</td>
                                            <td class="center">'.$_data["nombre_archivo"].'</td>
                                            <td class="center">'.$_data["accion"].'</td>
                                            ';
            if($pTipo=='C')//CONSULTAR
                {
                    if($_data["estado"]=='A')
                        $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-danger" data-toggle="modal" data-target="#ModConsultaOpciones" onClick="jsConsultarOpciones('.$_data["id_opcion_sistema"].')">Ver</button></td>';
                    else
                        $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModConsultaOpciones" onClick="jsConsultarOpciones('.$_data["id_opcion_sistema"].')">Ver</button></td>';
                }
                else//MODIFICAR
                {
                    if($_data["estado"]=='A')
                    {
                        $_form.='           <td align="center">&nbsp;</td>';
                        $_form.='           <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Perfil de Usuario" onClick="jsAnularActivar('.$_data["id_opcion_sistema"].',\'opciones_sistema.php\')"><i class="fa fa-check"></i></button></td>';

                    }
                    else
                    {
                        $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModModificarOpciones" onClick="jsModificarOpciones('.$_data["id_opcion_sistema"].')">Modificar</button></td>';
                        $_form.='           <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Perfil de Usuario" onClick="jsAnularActivar('.$_data["id_opcion_sistema"].',\'opciones_sistema.php\')"><i class="fa fa-times"></i></button></td>';
                    }
                }
            $_form.='
                                        </tr>';
        }
        $_form.='
                                </tbody>

                            </table>
                        </div>
                        <!-- /.panel-body -->

                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <script>
            $(document).ready(function() {
                $("#ver_opciones_sistema").DataTable({
                    responsive: true,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                        "zeroRecords": "No se encontraron registros",
                        "info": "P&aacutegina _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros",
                        "sSearch":         "Buscar:",                        
                        "oPaginate": {
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    },
                    "order": [[ 1, "asc" ]]
                });
            });
            </script>
            ';
        if($pTipo=='C')//CONSULTAR
        {
            $_form.='<div class="modal fade" id="ModConsultaOpciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';
        }
        else
        {
            $_form.='<div class="modal fade" id="ModModificarOpciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            </div>';
        }
        $array = array(0 => $_form);

        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fConsultarOpcion($_pIdOpcion) {
        $_operacionesbd=new operacionesbd();
        $jsondata = array();
        $vlnNum=0;
        $p_SQL="SELECT a.estado, a.id_opcion_sistema, a.nombre_opcion, a.alias_opcion, a.nombre_archivo, b.accion
                FROM tbl_opcion_sistema a
                inner join tbl_acciones b on a.id_acciones=b.id_acciones
                where a.id_opcion_sistema=".$_pIdOpcion;
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        foreach ($_vlv_Resultado as $_data)
        {
            $_form='<!-- Modal -->
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-lg-12">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        Consulta Opci&oacute;n - ID: '.$_data["id_opcion_sistema"].'
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label>Nombre de Opci&oacute;n</label> 
                                                                        <input class="form-control" id="txtNombreOpcion" name="txtNombreOpcion" onBlur="js_solo_mayusculas(this.value, this.id);" required
                                                                        value="'.$_data["nombre_opcion"].'">
                                                                        
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Nombre Archivo</label> 
                                                                        <input class="form-control" id="txtNombreArchivo" name="txtNombreArchivo" value="'.$_data["nombre_archivo"].'">                                                                    
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label>Alias de Opci&oacute;n</label> 
                                                                        <input class="form-control" id="txtAliasOpcion" name="txtAliasOpcion" value="'.$_data["alias_opcion"].'">                                                                       
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Acci&oacute;n</label> 
                                                                        <input class="form-control" id="cbAcciones" name="cbAcciones" value="'.$_data["accion"].'">                                                                       
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        <!-- /.row (nested) -->
                                                    </div>
                                                </div>
                                                <!-- /.panel -->
                                            </div>
                                        </div><!-- /.modal-body -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->

                            <!-- /.modal -->';
        }

        $array = array(0 => $_form);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fModificarOpcion($_pIdOpcion) {
        $_operacionesbd=new operacionesbd();
        $_appcomponentes=new appcomponentes();
        $jsondata = array();
        $vlnNum=0;
        //COMBO ACCIONES
        $pNombreCombo='cbAcciones';
        $pCamposId='a.id_acciones';
        $pCamposDetalle='a.accion';
        $pTabla='tbl_acciones a';
        $pInner='';
        $pWhere='';
        $pOrder='';
        $pGroupBy='';
        $pRequerido='required';

        $p_SQL="SELECT a.estado, a.id_opcion_sistema, a.nombre_opcion, a.alias_opcion, a.nombre_archivo, a.id_acciones
                FROM tbl_opcion_sistema a
                inner join tbl_acciones b on a.id_acciones=b.id_acciones
                where a.id_opcion_sistema=".$_pIdOpcion;
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
        $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
        $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

        foreach ($_vlv_Resultado as $_data)
        {
            $pSelected=$_data["id_acciones"];
            $cbAcciones=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);
            $_form='<!-- Modal -->
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarLista(\'opciones_sistema.php\');">&times;</button>
                                            <div id="divMensajeModal"></div>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-lg-12">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        Consulta Opci&oacute;n - ID: '.$_data["id_opcion_sistema"].'
                                                    </div>
                                                    <div class="panel-body">
                                                    <form role="form" data-toggle="validator" id="frmOpcionesSistema" >
                                                    <input type="hidden" id="hddIdOpciones" name="hddIdOpciones" value="'.$_data["id_opcion_sistema"].'">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label>Nombre de Opci&oacute;n</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                                        <input class="form-control required" id="txtNombreOpcion" name="txtNombreOpcion"
                                                                        value="'.$_data["nombre_opcion"].'">

                                                                        <p class="help-block with-errors">Ingrese el nombre de la nueva opci&oacute;n.</p>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Nombre de Archivo</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                                        <input class="form-control required" id="txtNombreArchivo" name="txtNombreArchivo"
                                                                        value="'.$_data["nombre_archivo"].'">

                                                                        <p class="help-block with-errors">Ingrese el nombre del Archivo</p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label>Alias de Opci&oacute;n</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                                        <input class="form-control required" id="txtAliasOpcion" name="txtAliasOpcion" value="'.$_data["alias_opcion"].'">
                                                                        <p class="help-block with-errors">Ingrese el Alias con el que se reconocera a la opci&oacute;n.</p>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Perfil de Usuario</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                                        '.$cbAcciones.'
                                                                        <p class="help-block">Seleccione un perfil  de usuario.</p>
                                                                    </div>                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.row (nested) -->
                                                    </form>
                                                    </div>
                                                </div>
                                                <!-- /.panel -->
                                            </div>
                                        </div><!-- /.modal-body -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" onClick="jsActualizarOpciones();">Guardar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarLista(\'opciones_sistema.php\');">Cerrar</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->

                            <!-- /.modal -->';
        }

        $array = array(0 => $_form);
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($array);
}
function fAnularActivarOpcion($_pIdOpcion)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    $p_funcion="SP_ANULAR_ACTIVAR_OPCIONES";
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdOpcion;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>