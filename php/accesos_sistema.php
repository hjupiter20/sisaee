<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$_pAccion               =$_POST["pAccion"];
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/



/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        fCargarFormulario();
    break;
    case 'GUARDAR':
        $_pPerfilUsuario    =$_POST['cbPerfil1'];
        $_pOpcion    =$_POST['cbOpciones'];
        $_pMenuSistema    =$_POST['cbMenuSistema'];
        fGuardarAccesoSistema($_pPerfilUsuario, $_pOpcion, $_pMenuSistema);
    break;
    case 'ACTUALIZAR':
        $_pPerfilUsuario    =$_POST['cbPerfil1'];
        $_pOpcion    =$_POST['cbOpciones'];
        $_pMenuSistema    =$_POST['cbMenuSistema'];
        $_pIdAccesoSistema    =$_POST['hddIdAccesoSistema'];

        fActualizarAccesoSistema($_pPerfilUsuario, $_pOpcion, $_pMenuSistema, $_pIdAccesoSistema);
    break;
    case 'LISTA':
        $_pTipoLista    =$_POST['pTipoLista'];
        fGeneraLista($_pTipoLista);
    break;
    case 'MODIFICA':
        $_pIdAcceso     =$_POST['pIdAcceso'];
        fModificarAccesoSistema($_pIdAcceso);
    break;
    case 'ANULA_ACTIVA':
        $_pIdAcceso           =$_POST['pIdCodigo'];
        fAnularActivarAccesoSistema($_pIdAcceso);
    break;
}

function fGuardarAccesoSistema($_pPerfilUsuario, $_pOpcion, $_pMenuSistema)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    $_pIdAccesoSistema="null";
    $p_funcion="SP_GUARDAR_ACCESO_SISTEMA";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pPerfilUsuario.",".$_pOpcion.",".$_pMenuSistema.",".$_pIdAccesoSistema;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fActualizarAccesoSistema($_pPerfilUsuario, $_pOpcion, $_pMenuSistema, $_pIdAccesoSistema)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    //$_pIdAccesoSistema="null";
    $p_funcion="SP_GUARDAR_ACCESO_SISTEMA";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pPerfilUsuario.",".$_pOpcion.",".$_pMenuSistema.",".$_pIdAccesoSistema;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    $_appcomponentes=new appcomponentes();
    $_operacionesbd=new operacionesbd();

    /*CREAR COMBO PERFILES*/
    $pNombreCombo='cbPerfil1';
    $pCamposId='a.id_perfil_usuario';
    $pCamposDetalle='a.nombre_perfil';
    $pTabla='tbl_perfil_usuario a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder=' order by a.nombre_perfil';
    $pGroupBy='';
    $pSelected='NULL';
    $pRequerido='required';
    $cbPerfil=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    /*CREAR COMBO MENU*/
    $pNombreCombo='cbMenuSistema';
    $pCamposId='a.id_menu_sistema';
    $pCamposDetalle='a.nombre_menu';
    $pTabla='tbl_menu_sistema a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder=' order by a.nombre_menu';
    $pGroupBy='';
    $pSelected='NULL';
    $pRequerido='required';
    $cbMenuSistema=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    /*CREAR COMBO OPCIONES*/
    $pNombreCombo='cbOpciones';
    $pCamposId='a.id_opcion_sistema';
    $pCamposDetalle='a.nombre_opcion';
    $pTabla='tbl_opcion_sistema a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder=' order by a.nombre_opcion';
    $pGroupBy='';
    $pSelected='NULL';
    $pRequerido='required';
    $cbOpciones=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    $p_SQL="select a.estado, a.id_acceso_sistema,
            a.id_perfil_usuario, b.nombre_perfil,
            a.id_menu_sistema, c.nombre_menu,
            a.id_opcion_sistema, d.nombre_opcion
            from tbl_acceso_sistema a
            inner join tbl_perfil_usuario b on a.id_perfil_usuario=b.id_perfil_usuario
            inner join tbl_menu_sistema c on a.id_menu_sistema=c.id_menu_sistema
            inner join tbl_opcion_sistema d on a.id_opcion_sistema=d.id_opcion_sistema
            order by b.nombre_perfil,c.nombre_menu,d.nombre_opcion ";
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Asignar Opciones a Perfiles
                        </div>
                        <div class="panel-body">
                            <form role="form" data-toggle="validator" id="frmAccesoSistema" >
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Perfil de Usuario</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbPerfil.'
                                            <p class="help-block">Seleccione un perfil  de usuario.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Men&uacute; del Sistema</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbMenuSistema.'
                                            <p class="help-block">Seleccione un Men&uacute;.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Opci&oacute;n del Sistema</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbOpciones.'
                                            <p class="help-block">Seleccione una opci&oacute;n para el perfil.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </form>
                            </div>
                                <!-- /.panel-body -->
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary" id="btnGuardarOpcionSistema" onClick="jsGuardarAccesoSistema();">Guardar</button>
                                    <button type="button" class="btn btn-primary">Limpiar</button>
                                </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="ver_accesos">
                                <thead>
                                    <tr>
                                        <th>Estado</th>
                                        <th>Perfil</th>
                                        <th>Men&uacute;</th>
                                        <th>Opci&oacute;n</th>
                                    </tr>
                                </thead>
                                <tbody>';
                        foreach ($_vlv_Resultado as $_data)
                        {
                            $_form.='<tr class="odd gradeA">
                                        <td class="center">'.$_data["estado"].'</td>
                                        <td class="center">'.$_data["nombre_perfil"].'</td>
                                        <td class="center">'.$_data["nombre_menu"].'</td>
                                        <td class="center">'.$_data["nombre_opcion"].'</td>
                                        ';
                            $_form.='</tr>';
                        }

    $_form.='                    </tbody>
                            </table>
                    </div>
                </div>
                </div>
            </div>
            <script>
            $(document).ready(function() {
                $("#ver_accesos").DataTable({
                    responsive: true,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                        "zeroRecords": "No se encontraron registros",
                        "info": "P&aacutegina _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros",
                        "sSearch":         "Buscar:",
                        "oPaginate": {
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    }
                });
            });
            </script>';
    //$_form="ENTRO";
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}
function fGeneraLista($pTipo) {
    $_operacionesbd=new operacionesbd();
    $jsondata = array();
    $vlnNum=0;
    $p_SQL="select a.estado, a.id_acceso_sistema,
        a.id_perfil_usuario, b.nombre_perfil,
        a.id_menu_sistema, c.nombre_menu,
        a.id_opcion_sistema, d.nombre_opcion
        from tbl_acceso_sistema a
        inner join tbl_perfil_usuario b on a.id_perfil_usuario=b.id_perfil_usuario
        inner join tbl_menu_sistema c on a.id_menu_sistema=c.id_menu_sistema
        inner join tbl_opcion_sistema d on a.id_opcion_sistema=d.id_opcion_sistema
        order by b.nombre_perfil,c.nombre_menu,d.nombre_opcion";
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    $_form='<div class="row">
            <div class="col-lg-12">
                <div id="divMensaje">&nbsp;</div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">';
    if($pTipo=='C')//CONSULTAR
        $_form.='       Consulta Pefiles';
    else
        $_form.='       Modificar Pefiles';

    $_form.='       </div>
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="ver_accesos_opciones">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Perfil</th>
                                    <th>Men&uacute;</th>
                                    <th>Opci&oacute;n</th>
                                    <th>&nbsp;</th>';
    if($pTipo=='M')//CONSULTAR
        $_form.='                   <th>&nbsp;</th>';

    $_form.='                   </tr>
                            </thead>
                            <tbody>';
    foreach ($_vlv_Resultado as $_data)
    {
    $_form.='                   <tr class="odd gradeA">
                                    <td class="center">'.$_data["estado"].'</td>
                                    <td class="center">'.$_data["nombre_perfil"].'</td>
                                    <td class="center">'.$_data["nombre_menu"].'</td>
                                    <td class="center">'.$_data["nombre_opcion"].'</td>
                                    ';
    if($pTipo=='C')//CONSULTAR
        {
            if($_data["estado"]=='A')
                $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-danger" data-toggle="modal" data-target="#ModConsultaAccesos" onClick="jsConsultarPerfilUsuario('.$_data["id_acceso_sistema"].')">Ver</button></td>';
            else
                $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModConsultaAccesos" onClick="jsConsultarPerfilUsuario('.$_data["id_acceso_sistema"].')">Ver</button></td>';
        }
        else//MODIFICAR
        {
            if($_data["estado"]=='A')
            {
                $_form.='           <td align="center">&nbsp;</td>';
                $_form.='           <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Perfil de Usuario" onClick="jsAnularActivar('.$_data["id_acceso_sistema"].',\'accesos_sistema.php\')"><i class="fa fa-check"></i></button></td>';

            }
            else
            {
                $_form.='           <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#ModModificarAccesos" onClick="jsModificarAccesoSistema('.$_data["id_acceso_sistema"].')">Modificar</button></td>';
                $_form.='           <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Perfil de Usuario" onClick="jsAnularActivar('.$_data["id_acceso_sistema"].',\'accesos_sistema.php\')"><i class="fa fa-times"></i></button></td>';
            }
        }
    $_form.='
                                </tr>';
    }
    $_form.='
                            </tbody>
                        </table>
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <script>
        $(document).ready(function() {
            $("#ver_accesos_opciones").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
        });
        </script>
        ';
    if($pTipo=='C')//CONSULTAR
    {
        $_form.='<div class="modal fade" id="ModConsultaAccesos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        </div>';
    }
    else
    {
        $_form.='<div class="modal fade" id="ModModificarAccesos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        </div>';
    }
    $array = array(0 => $_form);

    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}
function fModificarAccesoSistema($_pIdAccesos) {
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $jsondata = array();
    $vlnNum=0;

    $p_SQL="select a.estado, a.id_acceso_sistema,
            a.id_perfil_usuario, b.nombre_perfil,
            a.id_menu_sistema, c.nombre_menu,
            a.id_opcion_sistema, d.nombre_opcion
            from tbl_acceso_sistema a
            inner join tbl_perfil_usuario b on a.id_perfil_usuario=b.id_perfil_usuario
            inner join tbl_menu_sistema c on a.id_menu_sistema=c.id_menu_sistema
            inner join tbl_opcion_sistema d on a.id_opcion_sistema=d.id_opcion_sistema
            where a.id_acceso_sistema=".$_pIdAccesos."
            order by b.nombre_perfil,c.nombre_menu,d.nombre_opcion";
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    foreach ($_vlv_Resultado as $_data)
    {
        /*CREAR COMBO PERFILES*/
        $pNombreCombo='cbPerfil1';
        $pCamposId='a.id_perfil_usuario';
        $pCamposDetalle='a.nombre_perfil';
        $pTabla='tbl_perfil_usuario a';
        $pInner='';
        $pWhere=' where a.estado is null';
        $pOrder=' order by a.nombre_perfil';
        $pGroupBy='';
        $pSelected='NULL';
        $pRequerido='required';
        $cbPerfil=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$_data["id_perfil_usuario"],$pRequerido);

        /*CREAR COMBO MENU*/
        $pNombreCombo='cbMenuSistema';
        $pCamposId='a.id_menu_sistema';
        $pCamposDetalle='a.nombre_menu';
        $pTabla='tbl_menu_sistema a';
        $pInner='';
        $pWhere=' where a.estado is null';
        $pOrder=' order by a.nombre_menu';
        $pGroupBy='';
        $pSelected='NULL';
        $pRequerido='required';
        $cbMenuSistema=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$_data["id_menu_sistema"], $pRequerido);

        /*CREAR COMBO OPCIONES*/
        $pNombreCombo='cbOpciones';
        $pCamposId='a.id_opcion_sistema';
        $pCamposDetalle='a.nombre_opcion';
        $pTabla='tbl_opcion_sistema a';
        $pInner='';
        $pWhere=' where a.estado is null';
        $pOrder=' order by a.nombre_opcion';
        $pGroupBy='';
        $pSelected='NULL';
        $pRequerido='required';
        $cbOpciones=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$_data["id_opcion_sistema"],$pRequerido);
        $_form='<!-- Modal -->
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarLista(\'accesos_sistema.php\');">&times;</button>
                                        <div id="divMensajeModal"></div>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-lg-12">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    Modificar Acceso de Perfil - ID: '.$_data["id_acceso_sistema"].'
                                                </div>
                                                <div class="panel-body">
                                                 <form role="form" data-toggle="validator" id="frmAccesoSistema" >
                                                 <input type="hidden" id="hddIdAccesoSistema" name="hddIdAccesoSistema" value="'.$_data["id_acceso_sistema"].'">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label>Nombre de Perfil</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                                '.$cbPerfil.'
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label>Nombre de Menu</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                                '.$cbMenuSistema.'
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label>Nombre de Opcion</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                                '.$cbOpciones.'
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.row (nested) -->
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- /.panel -->
                                        </div>
                                    </div><!-- /.modal-body -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" id="btnGuardarPerfil" onClick="jsActualizarAccesoSistema();">Guardar</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarLista(\'accesos_sistema.php\');">Cerrar</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->

                        <!-- /.modal -->';
    }

    $array = array(0 => $_form);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}
function fAnularActivarAccesoSistema($_pIdPerfil)
{
    global $_pUsuario, $_pIp;
    $_operacionesbd=new operacionesbd();
    $p_funcion="SP_ANULAR_ACTIVAR_ACCESO_SISTEMA";
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdPerfil;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>