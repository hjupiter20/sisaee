<?php
require_once('../php/include/init.php');
/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIdUnidadOrganizativa=$_SESSION["vgnIdUnidadOrganizativa"];
$_pIp=Recuperaip();
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

$_dboperations=new dboperations();
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_FILES["fileToUpload"]["type"]))
{
    //DIFINICION DE NOMBRE DEL ARCHIVO
    $_pAnio=$_COOKIE["pAnio"];
    $_pCarrera=$_COOKIE["pCarrera"];
    $_pUnidadOrganizativa=$_COOKIE["pUnidadOrganizativa"];
    $_pPeriodo=$_COOKIE["pPeriodo"];
    $_pMes=$_COOKIE["pMes"];
    $_pResumenDoc=$_COOKIE["pResumenDoc"];
    $_pTipoDoc=$_COOKIE["pTipoDoc"];
    $_pSecuencia="";
    $p_SQL="SELECT case when max(a.secuencial) is  null then 1 else max(a.secuencial)+1 end as secuencia
            FROM tbl_gestion_documentos a
            where a.anio='".$_pAnio."'
            and a.mes='".$_pMes."'
            and a.id_carrera=".$_pCarrera."
            and a.prefijo='".$_pUnidadOrganizativa."'
            and a.id_periodo_academico=".$_pPeriodo."
            and a.prefijo_doc='".$_pTipoDoc."'";
    $_vlv_Resultado=$_dboperations->f_EjecutaQuery($p_SQL);
    $vlnNum=$_dboperations->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_dboperations->f_EjecutaQuery($p_SQL);

    foreach ($_vlv_Resultado as $_data)
    {
        $_pSecuencia=$_data["secuencia"];
    }

    $target_dir = '../file_storage/'.$_pAnio."/";
    $vlvNombreArchivo=$_pAnio.$_pMes.$_pCarrera.$_pTipoDoc.$_pUnidadOrganizativa.$_pPeriodo.str_pad($_pSecuencia,8,'0',STR_PAD_LEFT);

    //echo $target_dir;
    $carpeta=$target_dir;
    if (!file_exists($carpeta)) {
        mkdir($carpeta, 0777, true);
    }

    //$target_file = $carpeta . basename($_FILES["fileToUpload"]["name"]);//SIN CAMBIAR NOMBRE DEL ARCHIVO
    $target_file = $carpeta . basename($vlvNombreArchivo.'.pdf');
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            $errors[]= "El archivo es un pdf - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            $errors[]= "El archivo no es un pdf.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        $errors[]="Lo sentimos, archivo ya existe.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 20971520) {
        $errors[]= "Lo sentimos, el archivo es demasiado grande.  Tamaño máximo admitido: 20 MB";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "pdf") {
        $errors[]= "Lo sentimos, sólo archivos PDF son permitidos.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $errors[]= "Lo sentimos, tu archivo no fue subido.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
        {

            $_pIdDocumento="null";
            $p_funcion="SP_GUARDAR_DOCUMENTO";
            //$vlvResultado=array();
            $p_parametros="'".$_pUsuario."','".$_pIp."','".$_pAnio."','".$_pMes."',".$_pCarrera.",'".$_pUnidadOrganizativa."',".$_pPeriodo.",".$_pSecuencia.",'".$_pResumenDoc."','".$vlvNombreArchivo."','".$_pTipoDoc."',".$_pIdDocumento.",".$_pIdUnidadOrganizativa;
            $vlvResultado=explode("|",$_dboperations->f_EjecutaFuncion($p_funcion,$p_parametros));
            $messages[]= "El Archivo ha sido subido correctamente.";

        } else {
           $errors[]= "Lo sentimos, hubo un error subiendo el archivo.";
        }
    }

    if (isset($errors)){
        ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Error!</strong>
          <?php
          foreach ($errors as $error){
              echo"<p>$error</p>";
          }
          ?>
        </div>
        <?php
    }

    if (isset($messages)){
        ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Aviso!</strong>
          <?php
          foreach ($messages as $message){
              echo"<p>$message</p>";
          }
          ?>
        </div>
        <?php
    }
}
?>