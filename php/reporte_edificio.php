<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$_pAccion               =$_POST["pAccion"];
/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/



/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'COMBO':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $pSelected           =$_POST['pSelected'];
        $pId           =$_POST['pId'];
        fComboNivel($pSelected, $pId);
    break;
    case 'NUEVO':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        fCargarFormulario();
    break;
    case 'VER_REPORTE':
        $_pIdEdificio    =$_POST['pIdEdificio'];
        $_pIdNivel    =$_POST['pIdNivel'];
        fVerReporte($_pIdEdificio, $_pIdNivel);
    break;
}
function fComboNivel($pSelected, $pId)
{
    $_appcomponentes=new appcomponentes();
    if($pId)
    {
        $pNombreCombo='cbNiveles';
        $pCamposId='a.id_nivel';
        $pCamposDetalle="b.detalle";
        $pTabla='tbl_nivel a';
        $pInner=' inner join tbl_tipo_nivel b on a.id_tipo_nivel=b.id_tipo_nivel';
        $pWhere=' where a.estado is null and a.id_edificio='.$pId;
        $pOrder=' order by b.detalle';
        $pGroupBy='';
        $pSelected=$pSelected;
        $pRequerido='required';
        $cbEdificio=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);
    }
    else
        $cbEdificio='<select class="form-control">
                        <option value="">Seleccione</option>
                    </select>';

    $vlvResultado=explode("|",$cbEdificio);
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    global $_pNombreArchivo;
    $_appcomponentes=new appcomponentes();
    $_operacionesbd=new operacionesbd();

    /*CREAR COMBO EDIFICIO*/
    $pNombreCombo='cbEdificio';
    $pCamposId='a.id_edificio';
    $pCamposDetalle='a.nombre_edificio';
    $pTabla='tbl_edificio a';
    $pInner='';
    $pWhere=' where a.estado is null';
    $pOrder=' order by a.nombre_edificio';
    $pGroupBy='';
    $pSelected='NULL';
    $pRequerido='required';
    $pOnChange="jsComboNiveles('".$_pNombreArchivo."','NULL', this.value)";
    $cbEdificio=$_appcomponentes->f_crear_combo_change($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido,$pOnChange);

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Reporte de Edificio
                        </div>
                        <div class="panel-body">
                            <form role="form" data-toggle="validator" id="frmAccesoSistema" >
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Edificio</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbEdificio.'
                                            <p class="help-block">Seleccione edificio.</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Niveles</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            <div id="divCbNiveles">
                                                <select class="form-control">
                                                <option value="">Seleccione</option>
                                                </select>
                                            </div>
                                            <p class="help-block">Seleccione nivel.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </form>
                            </div>
                                <!-- /.panel-body -->
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary" id="btnGuardarOpcionSistema" onClick="jsVerReporte(\''.$_pNombreArchivo.'\');">Ver Reporte</button>
                                    <button type="button" class="btn btn-success" onClick="jsExportarExcel();">Exportar</button>
                                </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div id="divReporte"></div>
                    </div>
                </div>
                </div>
            </div>
            <script>
            $(document).ready(function() {
                $("#ver_accesos").DataTable({
                    responsive: true,
                    "language": {
                        "sProcessing":     "Procesando...",
                        "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                        "zeroRecords": "No se encontraron registros",
                        "info": "P&aacutegina _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros",
                        "sSearch":         "Buscar:",
                        "oPaginate": {
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    }
                });
            });
            </script>';
    //$_form="ENTRO";
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}
function fVerReporte($_pIdEdificio, $_pIdNivel) {
    $_operacionesbd=new operacionesbd();
    $jsondata = array();
    $vlnNum=0;
    if($_pIdEdificio<>'')
    {
        if($_pIdNivel<>'')
            $where="and a.id_edificio=".$_pIdEdificio." and b.id_nivel=".$_pIdNivel;
        else
            $where="and a.id_edificio=".$_pIdEdificio;

    }
    else
        $where="";

    $p_SQL="SELECT
            a.id_edificio, a.nombre_edificio, a.direccion, a.propietario, a.predio
            , a.ancho, a.alto, a.fondo, a.codigo_obra, a.valor_edificio, a.contratista
            , a.id_tipo_edificio, d.detalle as tipo_edificio
            , b.id_nivel, c.detalle as tipo_nivel, b.alto as altura_niveles
            , b.ancho as ancho_nivel, b.fondo as largo_nivel, e.detalle as tipo_estructura
            , f.id_area_nivel, g.detalle as tipo_area, f.alto as altura_niveles_area
            , f.fondo as ancho_area, f.largo as largo_area, h.id_nivel_area_elemento
            , j.detalle as tipo_elemento_edificio
            , k.nombre_elemento, l.detalle as tipo_elemento_nivel, k.ancho as ancho_elementos
            , k.largo as largo_elementos
            , k.alto as altura_niveles_elemento, k.espesor, k.armadura
            , m.detalle as tipo_material_elemento, k.resistencia, k.acero_frecuencia
            FROM tbl_edificio a
            left OUTER join tbl_nivel b on a.id_edificio=b.id_edificio and b.estado is NULL
            left OUTER join tbl_tipo_nivel c on b.id_tipo_nivel=c.id_tipo_nivel
            left OUTER join tbl_tipo_edificio d on a.id_tipo_edificio=d.id_tipo_edificio
            left OUTER join tbl_tipo_estructura e on b.id_tipo_estructura=e.id_tipo_estructura
            left OUTER join tbl_area_nivel f on b.id_nivel=f.id_nivel and f.estado is null
            left OUTER join tbl_tipo_area g on f.id_tipo_area=g.id_tipo_area
            left outer join tbl_nivel_area_elemento h on f.id_area_nivel=h.id_area_nivel and h.estado is NULL
            left OUTER join  tbl_elemento_nivel i on h.id_elemento_nivel=i.id_elemento_nivel
            left OUTER join tbl_tipo_elemento j on i.id_tipo_elemento=j.id_tipo_elemento
            left outer join tbl_elemento_nivel k on b.id_nivel=k.id_nivel and k.estado is null
            left outer join tbl_tipo_elemento l on k.id_tipo_elemento=l.id_tipo_elemento and l.estado is null
            left outer join tbl_tipo_material m on m.id_tipo_material=k.id_tipo_material
            WHERE a.estado is null ".$where;
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    $_form='<div class="row">
            <div class="col-lg-12">
                <div id="divMensaje">&nbsp;</div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">';

        $_form.='      Reporte Edificios';

    $_form.='       </div>
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="ver_reportes">
                            <thead>
                                <tr>
                                    <th colspan="12" align="center">Edificio</th>
                                    <th colspan="5" align="center">Nivel</th>
                                    <th colspan="5" align="center">&Aacute;rea</th>
                                    <th colspan="1" align="center">&nbsp;</th>
                                    <th colspan="10" align="center">Elemento</th>
                                </tr>
                                <tr>
                                    <th>Id Edificio</th>
                                    <th>Edificio</th>
                                    <th>Direcci&oacute;n</th>
                                    <th>Propietario</th>
                                    <th>Predio</th>
                                    <th>Ancho</th>
                                    <th>Alto</th>
                                    <th>Fondo</th>
                                    <th>Cod. Obra</th>
                                    <th>Valor</th>
                                    <th>Contratista</th>
                                    <th>Tipo Edificio</th>
                                    <th>Id Nivel</th>
                                    <th>Tipo Nivel</th>
                                    <th>Altura Nivel</th>
                                    <th>Ancho Nivel</th>
                                    <th>Largo Nivel</th>
                                    <th>Tipo Estructura</th>
                                    <th>Tipo &Aacute;rea</th>
                                    <th>Altura &Aacute;rea</th>
                                    <th>Ancho &Aacute;rea</th>
                                    <th>Largo &Aacute;rea</th>
                                    <th>Elemento Edificio</th>
                                    <th>Nombre Elemento</th>
                                    <th>Tipo Elemento</th>
                                    <th>Ancho Elemento</th>
                                    <th>Largo Elemento</th>
                                    <th>Altura Elemento</th>
                                    <th>Espesor</th>
                                    <th>Armadura</th>
                                    <th>Tipo Material</th>
                                    <th>Resistencia</th>
                                    <th>Acero/Frecuencia</th>
                                </tr>

                                    ';

    $_form.='
                            </thead>
                            <tbody>';
    foreach ($_vlv_Resultado as $_data)
    {
    $_form.='                   <tr class="odd gradeA">
                                    <td class="center">'.$_data["id_edificio"].'</td>
                                    <td class="center">'.$_data["nombre_edificio"].'</td>
                                    <td class="center">'.$_data["direccion"].'</td>
                                    <td class="center">'.$_data["propietario"].'</td>
                                    <td class="center">'.$_data["predio"].'</td>
                                    <td class="center">'.$_data["ancho"].'</td>
                                    <td class="center">'.$_data["alto"].'</td>
                                    <td class="center">'.$_data["fondo"].'</td>
                                    <td class="center">'.$_data["codigo_obra"].'</td>
                                    <td class="center">'.$_data["valor_edificio"].'</td>
                                    <td class="center">'.$_data["contratista"].'</td>
                                    <td class="center">'.$_data["tipo_edificio"].'</td>
                                    <td class="center">'.$_data["id_nivel"].'</td>
                                    <td class="center">'.$_data["tipo_nivel"].'</td>
                                    <td class="center">'.$_data["altura_niveles"].'</td>
                                    <td class="center">'.$_data["ancho_nivel"].'</td>
                                    <td class="center">'.$_data["largo_nivel"].'</td>
                                    <td class="center">'.$_data["tipo_estructura"].'</td>
                                    <td class="center">'.$_data["tipo_area"].'</td>
                                    <td class="center">'.$_data["altura_niveles_area"].'</td>
                                    <td class="center">'.$_data["ancho_area"].'</td>
                                    <td class="center">'.$_data["largo_area"].'</td>
                                    <td class="center">'.$_data["tipo_elemento_edificio"].'</td>
                                    <td class="center">'.$_data["nombre_elemento"].'</td>
                                    <td class="center">'.$_data["tipo_elemento_nivel"].'</td>
                                    <td class="center">'.$_data["ancho_elementos"].'</td>
                                    <td class="center">'.$_data["largo_elementos"].'</td>
                                    <td class="center">'.$_data["altura_niveles_elemento"].'</td>
                                    <td class="center">'.$_data["espesor"].'</td>
                                    <td class="center">'.$_data["armadura"].'</td>
                                    <td class="center">'.$_data["tipo_material_elemento"].'</td>
                                    <td class="center">'.$_data["resistencia"].'</td>
                                    <td class="center">'.$_data["acero_frecuencia"].'</td>
                                    ';
    $_form.='
                                </tr>';
    }
    $_form.='
                            </tbody>
                        </table>
                    </div>
                    <!-- /.panel-body -->

                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <script>
        $(document).ready(function() {
            $("#ver_reportes").DataTable({
                responsive: false,
                "scrollX": true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
        });
        </script>
        ';

    $array = array(0 => $_form);

    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}


?>