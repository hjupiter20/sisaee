<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pAccion               =$_POST["pAccion"];
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$pNombreTabla="tbl_nivel_area_elemento";
$pNombreDataTable="ver_nivel";
$pNombreModal="ModModificarNivel";
$pNombreHeader="Asignaci&oacute;n de Elementos a un &Aacute;reas, ";
$pPlaceHolder="Ingrese la descripci&oacute;n";
$pPlaceHolder1="Ingrese el Largo";
$pPlaceHolder2="Ingrese el Alto";
$pPlaceHolder3="Ingrese el Fondo";

$p_funcion="SP_GUARDAR_NIVEL_AREA_ELEMENTO";
$p_funcionAnulaActiva="SP_ANULAR_ACTIVAR_NIVEL_AREA_ELEMENTO";

/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        $_pNombreArchivo        =$_POST["pNombreArchivo"];
        $_pIdEdificio           =$_POST["pIdEdificio"];
        $_pIdNivel              =$_POST["pIdNivel"];
        $_pIdAreaNivel          =$_POST["pIdAreaNivel"];
        //echo $_pId;
        fCargarFormulario($_pIdEdificio, $_pIdNivel, $_pIdAreaNivel);
    break;
    case 'CONSULTAR':
        $_pNombreArchivo        =$_POST["pNombreArchivo"];
        $_pId                   =$_POST["pId"];
        //echo $_pId;
        fConsultarNiveles($_pId);
    break;

    case 'GUARDAR':
        //$_pIdEdificio       =$_POST['hddIdEdificio'];
        $_pIdNivel              =$_POST['hddIdNivel'];
        $_pIdAreaNivel          =$_POST['hddIdAreaNivel'];
        $_pIdElementoNivel      =$_POST['cbTipoElemento'];
        fGuardar($_pIdNivel, $_pIdAreaNivel, $_pIdElementoNivel);
    break;
    case 'ACTUALIZAR':
        $_pId                   =$_POST['hddId'];
        $_pIdNivel              =$_POST['hddIdNivel'];
        $_pIdAreaNivel          =$_POST["hddIdAreaNivel"];
        $_pIdElementoNivel      =$_POST['cbTipoElemento'];
        fActualizar($_pId, $_pIdNivel, $_pIdAreaNivel, $_pIdElementoNivel);
    break;
    case 'LISTA':
        $_pNombreArchivo        =$_POST["pNombreArchivo"];
        fGeneraLista();
    break;
    case 'VER_PANTALLA_MODIFICAR':
        $_pNombreArchivo        =$_POST["pNombreArchivo"];
        $_pId                   =$_POST['pId'];
        $_pIdEdificio           =$_POST['pIdEdificio'];
        $_pIdNivel              =$_POST['pIdNivel'];
        $_pIdAreaNivel          =$_POST["pIdAreaNivel"];
        fVerPantallaModificar($_pId, $_pIdEdificio, $_pIdNivel, $_pIdAreaNivel);
    break;
    case 'ANULA_ACTIVA':
        $_pIdOpcion             =$_POST['pIdCodigo'];
        fAnularActivar($_pIdOpcion);
    break;
}

function fGuardar($_pIdNivel, $_pIdAreaNivel, $_pIdElementoNivel)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $_pId="null";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdNivel.",".$_pIdAreaNivel.",".$_pIdElementoNivel.",".$_pId;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

function fActualizar($_pId, $_pIdNivel, $_pIdAreaNivel, $_pIdElementoNivel)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdNivel.",".$_pIdAreaNivel.",".$_pIdElementoNivel.",".$_pId;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario($_pIdEdificio,$_pIdNivel, $_pIdAreaNivel)
{
    global $_pNombreArchivo, $pNombreTabla,$pNombreDataTable,$pNombreModal, $pNombreFrm,$pNombreHeader,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3;
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $pNombreFrm="frmNuevo";
    $pNombreBtnGuardar="btnNivelAreaElemento";

    /*CREAR COMBO TIPO ELEMENTO*/
    $pNombreCombo='cbTipoElemento';
    $pCamposId='a.id_elemento_nivel';
    $pCamposDetalle='b.detalle';
    $pTabla='tbl_elemento_nivel a';
    $pInner=' inner join tbl_tipo_elemento b on a.id_tipo_elemento=b.id_tipo_elemento';
    $pWhere=' where a.estado is null and a.id_nivel='.$_pIdNivel;
    $pOrder=' order by b.detalle';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';
    $cbTipoElemento=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            '.$pNombreHeader.' Edificio No '.$_pIdEdificio.', Nivel No.'.$_pIdNivel.', &Aacute;rea No. '.$_pIdAreaNivel.'
                        </div>
                        <div class="panel-body">
                            <form role="form" id="'.$pNombreFrm.'" >
                            <input type="hidden" id="hddIdEdificio" name="hddIdEdificio" value="'.$_pIdEdificio.'">
                            <input type="hidden" id="hddIdNivel" name="hddIdNivel" value="'.$_pIdNivel.'">
                            <input type="hidden" id="hddIdAreaNivel" name="hddIdAreaNivel" value="'.$_pIdAreaNivel.'">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Tipo de elemento</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        '.$cbTipoElemento.'
                                        <p class="help-block"></p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="'.$pNombreBtnGuardar.'"
                           onClick="jsGuardar4(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\',\''.$_pIdEdificio.'\',\''.$_pIdNivel.'\','.$_pIdAreaNivel.');">Guardar</button>
                           <button type="button" class="btn btn-primary" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\','.$_pIdEdificio.');">Limpiar</button>
                           <button type="button" class="btn btn-primary" onClick="jsAbrirAreaNivel(\'area_nivel.php\',\'NUEVO\',\''.$_pIdEdificio.'\','.$_pIdNivel.');">Regresar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
            //$_pIdEdificio, $_pIdNivel, $_pIdAreaEdificio, $_pIdElemento
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_nivel_area_elemento, c.detalle as tipo_area, e.detalle as tipo_elemento, a.id_nivel
            FROM ".$pNombreTabla." a
            inner join tbl_area_nivel b on a.id_area_nivel=b.id_area_nivel
            inner join tbl_tipo_area c on b.id_tipo_area=c.id_tipo_area
            inner join tbl_elemento_nivel d on a.id_elemento_nivel=d.id_elemento_nivel
            inner join tbl_tipo_elemento e on d.id_tipo_elemento=e.id_tipo_elemento
            where a.id_area_nivel=".$_pIdAreaNivel;

    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    $_form.='
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="'.$pNombreDataTable.'">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Id.</th>
                                    <th>&Aacute;rea Nivel</th>
                                    <th>Tipo Elemento</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>';

   foreach ($_vlv_Resultado as $_data)
    {
        $_form.='           <tr class="odd gradeA">
                                <td class="center">'.$_data["estado"].'</td>
                                <td class="center">'.$_data["id_nivel_area_elemento"].'</td>
                                <td class="center">'.$_data["tipo_area"].'</td>
                                <td class="center">'.$_data["tipo_elemento"].'</td>
                            ';
        if($_data["estado"]=='A')
            {
                $_form.='       <td align="center">&nbsp;</td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Registro" onClick="jsAnularActivar4('.$_data["id_nivel_area_elemento"].',\''.$_pNombreArchivo.'\',\''.$_pIdEdificio.'\',\''.$_data["id_nivel"].'\','.$_pIdAreaNivel.')"><i class="fa fa-check"></i></button></td>';

            }
            else
            {
                $_form.='       <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#'.$pNombreModal.'" onClick="jsVerPantallaModificar3('.$_data["id_nivel_area_elemento"].',\''.$_pNombreArchivo.'\',\''.$pNombreModal.'\',\''.$_pIdEdificio.'\',\''.$_data["id_nivel"].'\','.$_pIdAreaNivel.')">Modificar</button></td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Registro" onClick="jsAnularActivar4('.$_data["id_nivel_area_elemento"].',\''.$_pNombreArchivo.'\',\''.$_pIdEdificio.'\',\''.$_data["id_nivel"].'\','.$_pIdAreaNivel.')"><i class="fa fa-times"></i></button></td>';
            }
        $_form.='           </tr>';
    }
    $_form.='               </tbody>
                        </table>
                    </div> <!-- /.panel-body -->
                </div>  <!-- /.panel -->
            </div> <!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <script>
        $(document).ready(function() {
            $("#'.$pNombreDataTable.'").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                },
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 0, 1 ]
                } ]
            });
        });
        </script>
        ';
    $_form.='<div class="modal fade" id="'.$pNombreModal.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             </div>';
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}

function fVerPantallaModificar($_pIdOpcion, $pIdEdificio, $pIdNivel, $_pIdAreaEdificio)
{
    global $_pNombreArchivo, $pNombreTabla,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3, $pNombreBtnGuardar, $pNombreModal;
    $pNombreFrm="frmActualizar";
    $_operacionesbd=new operacionesbd();
    $_appcomponentes=new appcomponentes();
    $jsondata = array();
    $vlnNum=0;

    $p_SQL="SELECT a.estado, a.id_nivel_area_elemento, a.id_nivel, a.id_elemento_nivel
            FROM ".$pNombreTabla." a
            where a.id_nivel_area_elemento=".$_pIdOpcion;
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    /*CREAR COMBO TIPO ELEMENTO*/
    $pNombreCombo='cbTipoElemento';
    $pCamposId='a.id_elemento_nivel';
    $pCamposDetalle='b.detalle';
    $pTabla='tbl_elemento_nivel a';
    $pInner=' inner join tbl_tipo_elemento b on a.id_tipo_elemento=b.id_tipo_elemento';
    $pWhere=' where a.estado is null and a.id_nivel='.$pIdNivel;
    $pOrder=' order by b.detalle';
    $pGroupBy='';
    $pSelected='null';
    $pRequerido='required';

    foreach ($_vlv_Resultado as $_data)
    {
        $pSelected=$_data["id_elemento_nivel"];
        //$cbTipoNivel=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);
        $cbTipoElemento=$_appcomponentes->f_crear_combo($pNombreCombo,$pCamposId,$pCamposDetalle,$pTabla,$pInner,$pWhere,$pOrder,$pGroupBy,$pSelected,$pRequerido);

        $_form='
        <!-- Modal -->
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarForm4(\''.$_pNombreArchivo.'\','.$pIdEdificio.',\''.$pIdNivel.'\','.$_pIdAreaEdificio.');">&times;</button>
                        <div id="divMensajeModal"></div>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Modificar Registro - ID Edificio: '.$pIdEdificio.', Nivel No.'.$pIdNivel.' / ID &Aacute;rea - Elemento: '.$_pIdOpcion.'
                                </div>
                                <div class="panel-body">
                                <form role="form" id="'.$pNombreFrm.'" >
                                    <input type="hidden" id="hddId" name="hddId" value="'.$_pIdOpcion.'">
                                    <input type="hidden" id="hddIdEdificio" name="hddIdEdificio" value="'.$pIdEdificio.'">
                                    <input type="hidden" id="hddIdNivel" name="hddIdNivel" value="'.$pIdNivel.'">
                                    <input type="hidden" id="hddIdAreaNivel" name="hddIdAreaNivel" value="'.$_pIdAreaEdificio.'">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label>Tipo de elemento</label> <i class="fa fa-asterisk" style="color:red"></i>
                                            '.$cbTipoElemento.'
                                            <p class="help-block"></p>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </form>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div><!-- /.modal-body -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="'.$pNombreBtnGuardar.'" onClick="jsActualizar2(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onClick="jsRefrescarForm4(\''.$_pNombreArchivo.'\','.$pIdEdificio.',\''.$pIdNivel.'\','.$_pIdAreaEdificio.');">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        <!-- /.modal -->';
    }

    $array = array(0 => $_form);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}

function fAnularActivar($_pIdOpcion)
{
    global $_pUsuario, $_pIp, $p_funcionAnulaActiva;
    $_operacionesbd=new operacionesbd();

    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdOpcion;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcionAnulaActiva,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>