<?php
/*INICIO CARGAR LIBRERIAS*/
require_once('../php/include/init.php');
/*FIN CARGAR LIBRERIAS*/

/*INICIO CARGAR VARIABLES DE SESION Y GLOBALES*/
$_pAccion               =$_POST["pAccion"];
$_pUsuario=$_SESSION["vgvUsuario"];
$_pIp=Recuperaip();
$pNombreTabla="tbl_area_edificio";
$pNombreDataTable="ver_area_edificio";
$pNombreModal="ModModificarTipoLocacion";
$pNombreHeader="Tipo &Aacute;rea Edificio";
$pPlaceHolder="Ingrese la descripci&oacute;n";
$pPlaceHolder1="Ingrese el Largo";
$pPlaceHolder2="Ingrese el Alto";
$pPlaceHolder3="Ingrese el Fondo";

$p_funcion="SP_GUARDAR_AREA_EDIFICIO";
$p_funcionAnulaActiva="SP_ANULAR_ACTIVAR_AREA_EDIFICIO";

/*FIN CARGAR VARIABLES DE SESION Y GLOBALES*/

switch($_pAccion)
{
    case 'NUEVO':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        fCargarFormulario();
    break;
    case 'GUARDAR':
        $_pDetalle  =$_POST['txtDetalle'];
        $_pAlto     =$_POST['txtAlto'];
        $_pLargo    =$_POST['txtLargo'];
        $_pFondo    =$_POST['txtFondo'];
        fGuardar($_pDetalle, $_pAlto, $_pLargo, $_pFondo);
    break;
    case 'ACTUALIZAR':
        $_pDetalle  =$_POST['txtDetalle'];
        $_pId       =$_POST['hddId'];
        $_pAlto     =$_POST['txtAlto'];
        $_pLargo    =$_POST['txtLargo'];
        $_pFondo    =$_POST['txtFondo'];
        fActualizar($_pId, $_pDetalle, $_pAlto, $_pLargo, $_pFondo);
    break;
    case 'VER_PANTALLA_MODIFICAR':
        $_pNombreArchivo    =$_POST["pNombreArchivo"];
        $_pId               =$_POST['pId'];
        fVerPantallaModificar($_pId);
    break;
    case 'ANULA_ACTIVA':
        $_pIdOpcion         =$_POST['pIdCodigo'];
        fAnularActivar($_pIdOpcion);
    break;
}

function fGuardar($_pDetalle, $_pAlto, $_pLargo, $_pFondo)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $_pId="null";
    //$vlvResultado=array();
    $p_parametros="'".$_pUsuario."','".$_pIp."','".strtoupper($_pDetalle)."',".$_pId.",".$_pAlto.",".$_pLargo.",".$_pFondo;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

function fActualizar($_pId, $_pDetalle, $_pAlto, $_pLargo, $_pFondo)
{
    global $_pUsuario, $_pIp, $p_funcion;
    $_operacionesbd=new operacionesbd();
    $p_parametros="'".$_pUsuario."','".$_pIp."','".strtoupper($_pDetalle)."',".$_pId.",".$_pAlto.",".$_pLargo.",".$_pFondo;

    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcion,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}
function fCargarFormulario()
{
    global $_pNombreArchivo, $pNombreTabla,$pNombreDataTable,$pNombreModal, $pNombreFrm,$pNombreHeader,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3;
    $_operacionesbd=new operacionesbd();
    $pNombreFrm="frmNuevo";
    $pNombreBtnGuardar="btnTipoLocacion";

    $_form='<div class="row">
                <div class="col-lg-12">
                    <div id="divMensaje">&nbsp;</div>
                </div>
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            '.$pNombreHeader.'
                        </div>
                        <div class="panel-body">
                            <form role="form" id="'.$pNombreFrm.'" >
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Descripci&oacute;n</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required" id="txtDetalle" name="txtDetalle" placeholder="'.$pPlaceHolder.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Largo (Valor en m2)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required number" id="txtLargo" name="txtLargo" placeholder="'.$pPlaceHolder1.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Alto (Valor en m2)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required number" id="txtAlto" name="txtAlto" placeholder="'.$pPlaceHolder2.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Fondo (Valor en m2)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                        <input class="form-control required number" id="txtFondo" name="txtFondo" placeholder="'.$pPlaceHolder3.'">
                                        <p class="help-block with-errors"></p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                        <div class="panel-footer">
                           <button type="button" class="btn btn-primary" id="'.$pNombreBtnGuardar.'" onClick="jsGuardar(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                           <button type="button" class="btn btn-primary" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">Limpiar</button>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>';
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_area_edificio, a.detalle, a.alto, a.largo, a.fondo
            FROM ".$pNombreTabla." a";
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

     $_form.='
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="'.$pNombreDataTable.'">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Id. Registro</th>
                                    <th>Descripci&oacute;n</th>
                                    <th>Alto</th>
                                    <th>Largo</th>
                                    <th>Fondo</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>';

   foreach ($_vlv_Resultado as $_data)
    {
        $_form.='           <tr class="odd gradeA">
                                <td class="center">'.$_data["estado"].'</td>
                                <td class="center">'.$_data["id_area_edificio"].'</td>
                                <td class="center">'.$_data["detalle"].'</td>
                                <td class="center">'.$_data["alto"].'</td>
                                <td class="center">'.$_data["largo"].'</td>
                                <td class="center">'.$_data["fondo"].'</td>
                            ';
        if($_data["estado"]=='A')
            {
                $_form.='       <td align="center">&nbsp;</td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Activar Registro" onClick="jsAnularActivar('.$_data["id_area_edificio"].',\''.$_pNombreArchivo.'\')"><i class="fa fa-check"></i></button></td>';

            }
            else
            {
                $_form.='       <td align="center"><button type="button" class="btn btn-outline btn-success" data-toggle="modal" data-target="#'.$pNombreModal.'" onClick="jsVerPantallaModificar('.$_data["id_area_edificio"].',\''.$_pNombreArchivo.'\',\''.$pNombreModal.'\')">Modificar</button></td>';
                $_form.='       <td align="center"><button type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="top" title="Anular Registro" onClick="jsAnularActivar('.$_data["id_area_edificio"].',\''.$_pNombreArchivo.'\')"><i class="fa fa-times"></i></button></td>';
            }
        $_form.='           </tr>';
    }
    $_form.='               </tbody>
                        </table>
                    </div> <!-- /.panel-body -->
                </div>  <!-- /.panel -->
            </div> <!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <script>
        $(document).ready(function() {
            $("#'.$pNombreDataTable.'").DataTable({
                responsive: true,
                "language": {
                    "sProcessing":     "Procesando...",
                    "lengthMenu": "Mostrar _MENU_ registros por p&aacutegina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "P&aacutegina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "sSearch":         "Buscar:",
                    "oPaginate": {
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                },
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 0, 1 ]
                } ]
            });
        });
        </script>
        ';
    $_form.='<div class="modal fade" id="'.$pNombreModal.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             </div>';
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $_form);
    echo json_encode($_jSonArray);
}

function fVerPantallaModificar($_pIdOpcion)
{
    global $_pNombreArchivo, $pNombreTabla,$pPlaceHolder, $pPlaceHolder1,$pPlaceHolder2,$pPlaceHolder3;
    $pNombreFrm="frmActualizar";
    $_operacionesbd=new operacionesbd();
    $jsondata = array();
    $vlnNum=0;
    $p_SQL="SELECT a.estado, a.id_area_edificio, a.detalle, a.alto, a.largo, a.fondo
            FROM ".$pNombreTabla." a
            where a.id_area_edificio=".$_pIdOpcion;
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);
    $vlnNum=$_operacionesbd->db_numrows($_vlv_Resultado);
    $_vlv_Resultado=$_operacionesbd->f_EjecutaQuery($p_SQL);

    foreach ($_vlv_Resultado as $_data)
    {
        $_form='
        <!-- Modal -->
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">&times;</button>
                        <div id="divMensajeModal"></div>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Modificar Registro - ID: '.$_data["id_area_edificio"].'
                                </div>
                                <div class="panel-body">
                                <form role="form" id="'.$pNombreFrm.'" >
                                <input type="hidden" id="hddId" name="hddId" value="'.$_data["id_area_edificio"].'">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Descripci&oacute;n</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required" id="txtDetalle" name="txtDetalle"
                                                value="'.$_data["detalle"].'">
                                                <p class="help-block with-errors">'.$pPlaceHolder.'</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Largo (Valor en m2)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" id="txtLargo" name="txtLargo"
                                                value="'.$_data["largo"].'">
                                                <p class="help-block with-errors">'.$pPlaceHolder1.'</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Alto (Valor en m2)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" id="txtAlto" name="txtAlto"
                                                value="'.$_data["alto"].'">
                                                <p class="help-block with-errors">'.$pPlaceHolder2.'</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Fondo (Valor en m2)</label> <i class="fa fa-asterisk" style="color:red"></i>
                                                <input class="form-control required number" id="txtFondo" name="txtFondo"
                                                value="'.$_data["fondo"].'">
                                                <p class="help-block with-errors">'.$pPlaceHolder3.'</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row (nested) -->
                                </form>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div><!-- /.modal-body -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onClick="jsActualizar(\''.$_pNombreArchivo.'\',\''.$pNombreFrm.'\');">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onClick="jsRefrescarForm(\''.$_pNombreArchivo.'\');">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        <!-- /.modal -->';
    }

    $array = array(0 => $_form);
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($array);
}
function fAnularActivar($_pIdOpcion)
{
    global $_pUsuario, $_pIp, $p_funcionAnulaActiva;
    $_operacionesbd=new operacionesbd();

    $p_parametros="'".$_pUsuario."','".$_pIp."',".$_pIdOpcion;
    $vlvResultado=explode("|",$_operacionesbd->f_EjecutaFuncion($p_funcionAnulaActiva,$p_parametros));
    header('Content-type: application/json; charset=utf-8');
    $_jSonArray = array(0 => $vlvResultado[0],1 =>$vlvResultado[1]);
    echo json_encode($_jSonArray);
}

?>